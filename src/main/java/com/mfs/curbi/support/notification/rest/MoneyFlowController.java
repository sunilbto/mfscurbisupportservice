package com.mfs.curbi.support.notification.rest;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.curbi.support.dto.MoneyFlowDto;
import com.mfs.curbi.support.dto.MoneyFlowResponseDto;
import com.mfs.curbi.support.service.MoneyFlowService;
import com.mfs.curbi.support.util.Constant;

/**
 * 
 * @author : Beauto Systems
 * @email : amit.yadav@beautosys.com
 * @functionality : This controller is for money flow details
 */
@RestController
public class MoneyFlowController {

	private static final Logger log = LoggerFactory.getLogger(MoneyFlowController.class);

	@Autowired
	MoneyFlowService moneyFlowService;

	/*
	 * This api returns Money Flow details.
	 */
	@RequestMapping(value = "/getMoneyFlows", method = RequestMethod.POST)
	public MoneyFlowResponseDto getMoneyFlowResponse(@RequestBody MoneyFlowDto moneyFlowDto) {
		MoneyFlowResponseDto moneyFlowResponseDto = null;
		try {
			moneyFlowResponseDto = moneyFlowService.getMoneyFlows(moneyFlowDto);

			moneyFlowResponseDto.setMessage("MoneyFlows Info");
			moneyFlowResponseDto.setErrorCode(Constant.SUCCESS_CODE);

		} catch (HibernateException e) {
			log.error("Exception While Getting DashBoard Info " + e);

			moneyFlowResponseDto = new MoneyFlowResponseDto();
			moneyFlowResponseDto.setErrorCode(Constant.ERROR_CODE);

		} catch (Exception e) {
			moneyFlowResponseDto = new MoneyFlowResponseDto();
			moneyFlowResponseDto.setErrorCode(Constant.ERROR_CODE);

		}
		return moneyFlowResponseDto;
	}
	
}
