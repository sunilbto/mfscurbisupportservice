package com.mfs.curbi.support.notification.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.curbi.support.dto.ComplianceFinalResponseDto;
import com.mfs.curbi.support.dto.ComplianceRequestDto;
import com.mfs.curbi.support.dto.FinalStatisticTransactionResponseDto;
import com.mfs.curbi.support.dto.MoneyFilterResponseDto;
import com.mfs.curbi.support.dto.PricingFinalResponseDto;
import com.mfs.curbi.support.dto.PricingRequestDto;
import com.mfs.curbi.support.dto.StaticsticFilterResponseDto;
import com.mfs.curbi.support.dto.StatisticCounsumeRequestDto;
import com.mfs.curbi.support.dto.StatisticCounsumeResponseDto;
import com.mfs.curbi.support.dto.StatisticTransactionRequestDto;
import com.mfs.curbi.support.service.ComplianceDetailsService;
import com.mfs.curbi.support.service.MoneyFilterService;
import com.mfs.curbi.support.service.PricingDetailService;
import com.mfs.curbi.support.service.StaticsticFilterService;
import com.mfs.curbi.support.service.StatisticDetailService;
import com.mfs.curbi.support.util.Constant;

/**
 * 
 * @author : Beauto Systems
 * @email : ashwini.yadav@beautosys.com
 * @functionality : This controller returns pricing details,moneyflow filter,
 *                compliance details,staticstic details and staticstic filter.
 */
@RestController
public class RegulatorController {

	@Autowired
	PricingDetailService pricingDetailService;

	@Autowired
	MoneyFilterService moneyFilterService;
	@Autowired
	ComplianceDetailsService complianceDetailsService;
	@Autowired
	StatisticDetailService statisticDetailService;
	@Autowired
	StaticsticFilterService staticsticFilterService;

	/*
	 * This api returns pricing details.
	 */
	@RequestMapping(value = "/getPricingDetails", method = RequestMethod.POST)
	public PricingFinalResponseDto getPricingDetails(@RequestBody PricingRequestDto request) {
		PricingFinalResponseDto pricingResponse = null;

		pricingResponse = pricingDetailService.getPricingDetails(request);

		return pricingResponse;
	}

	/*
	 * This api returns money flow filters
	 */
	@RequestMapping(value = "/getMoneyFilter")
	public MoneyFilterResponseDto getMoneyFilter() {
		MoneyFilterResponseDto filterResponse = null;
		filterResponse = moneyFilterService.moneyFilterService();

		return filterResponse;
	}

	/*
	 * This api returns compliance details
	 */
	@RequestMapping(value = "/getCompliance", method = RequestMethod.POST)
	public ComplianceFinalResponseDto getComplianceDetails(@RequestBody ComplianceRequestDto request) {
		ComplianceFinalResponseDto complianceresponse = null;
		try {
			complianceresponse = complianceDetailsService.getComplianceDetailsService(request.getCountryName(),
					request.getStartDate(), request.getEndDate());

			if (complianceresponse.receivingFromCountryResponse == null
					&& complianceresponse.sendingFromCountryRespone == null
					&& complianceresponse.sendingToCountryRespone == null) {
				complianceresponse.setResponseCode(Constant.ERROR_CODE);
				complianceresponse.setResponseMessage("No record found");
			} else {
				complianceresponse.setResponseCode(Constant.SUCCESS_CODE);
				complianceresponse.setResponseMessage("Compliance details");
			}

		} catch (Exception e) {

			complianceresponse = new ComplianceFinalResponseDto();
			complianceresponse.setResponseCode(Constant.ERROR_CODE);
			complianceresponse.setResponseMessage("Exception getting Compliance record");

		}

		return complianceresponse;

	}

	/*
	 * This api returns statistic counsumer details
	 */
	@RequestMapping(value = "/getStatisticCounsume", method = RequestMethod.POST)
	public StatisticCounsumeResponseDto getStatisticCounsumerDetails(@RequestBody StatisticCounsumeRequestDto request) {
		StatisticCounsumeResponseDto statisticResponse = null;
		statisticResponse = statisticDetailService.getStatisticCounsmerDetailsService(request.getCountryName(),
				request.getStartDate(), request.getEndDate(), request.getProduct(), request.getDestinationType());

		if (statisticResponse != null) {
			statisticResponse.setResponseCode(Constant.SUCCESS_CODE);
			statisticResponse.setResponseMessage("Statistic Counsume Details ");
		} else {
			// log.info("No record found");
			statisticResponse = new StatisticCounsumeResponseDto();
			statisticResponse.setResponseCode(Constant.INFO_CODE);
			statisticResponse.setResponseMessage("No record found");
		}

		return statisticResponse;
	}

	/*
	 * This api returns statistic transaction details
	 */
	@RequestMapping(value = "/getStatisticTransaction", method = RequestMethod.POST)
	public FinalStatisticTransactionResponseDto getStatisticTransactionDetails(
			@RequestBody StatisticTransactionRequestDto request) {
		FinalStatisticTransactionResponseDto finalResponse = null;
		finalResponse = statisticDetailService.getStatisticTransactionDetailsService(request.getCountryName(),
				request.getStartDate(), request.getEndDate(), request.getProduct(), request.getDestinationType());

		return finalResponse;
	}

	/*
	 * This api returns staticstic filter
	 */
	@RequestMapping(value = "/getStaticsticFilter")
	public StaticsticFilterResponseDto getStaticsticFilter() {
		StaticsticFilterResponseDto staticsticFilterResponse = null;
		staticsticFilterResponse = staticsticFilterService.getStaticsticFilterService();

		return staticsticFilterResponse;
	}

}