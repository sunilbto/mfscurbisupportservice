package com.mfs.curbi.support.notification.rest;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.curbi.support.dto.AllBalanceByIdRequestDto;
import com.mfs.curbi.support.dto.AllBalanceRequestDto;
import com.mfs.curbi.support.dto.AllDashBoardRequestDto;
import com.mfs.curbi.support.dto.AllDashBoardResponse;
import com.mfs.curbi.support.dto.AllTransactionByIdRequestDto;
import com.mfs.curbi.support.dto.AllTransactionRequestDto;
import com.mfs.curbi.support.dto.BalanceExcelDto;
import com.mfs.curbi.support.dto.BalanceResponseDto;
import com.mfs.curbi.support.dto.GetOrganizationResponseDto;
import com.mfs.curbi.support.dto.MdmCountryResponseDto;
import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.dto.MdmDirectionResonseDto;
import com.mfs.curbi.support.dto.MdmProductResponseDto;
import com.mfs.curbi.support.dto.ReportExcelDto;
import com.mfs.curbi.support.dto.TransactionAllResponseDto;
import com.mfs.curbi.support.dto.TransactionByIdResponseDto;
import com.mfs.curbi.support.dto.TransactionDetailResponseDto;
import com.mfs.curbi.support.dto.TransactionExcelDto;
import com.mfs.curbi.support.dto.TransactionStatusRsponseDto;
import com.mfs.curbi.support.service.BalanceService;
import com.mfs.curbi.support.service.Countryservice;
import com.mfs.curbi.support.service.DashboardService;
import com.mfs.curbi.support.service.DestinationTypeService;
import com.mfs.curbi.support.service.DirectionService;
import com.mfs.curbi.support.service.ExportService;
import com.mfs.curbi.support.service.GetOrganizationService;
import com.mfs.curbi.support.service.ProductService;
import com.mfs.curbi.support.service.TransactionDetailService;
import com.mfs.curbi.support.service.TransactionStatusService;
import com.mfs.curbi.support.util.Constant;

/**
 * 
 * @author : Beauto Systems
 * @email : shubham.bhople@beautosys.com
 * @functionality : This controller is for transaction details,balanace record
 *                and report overview.
 */
@RestController
public class CubriController {
	@Autowired
	TransactionDetailService transService;
	@Autowired
	ProductService productService;
	@Autowired
	DirectionService directionService;
	@Autowired
	Countryservice countryService;
	@Autowired
	DestinationTypeService destinationType;
	@Autowired
	BalanceService balanceService;
	@Autowired
	TransactionStatusService transactionStatusService;
	@Autowired
	GetOrganizationService getOrganizationService;
	@Autowired
	DashboardService dashboardService;
	@Autowired
	ExportService exportService;

	private static final Logger log = LoggerFactory.getLogger(CubriController.class);

	/*
	 * This api return report overview for admin and organisation.
	 */
	@RequestMapping(value = "/getDashBoard", method = RequestMethod.POST)
	public AllDashBoardResponse getDashBoardInfo(@RequestBody AllDashBoardRequestDto request) {

		AllDashBoardResponse response = new AllDashBoardResponse();

		try {
			response = dashboardService.getDashBoardInfo(request.getPartnerCode(), request.getStartDate(),
					request.getEndDate(), request.getStatus(), request.getProduct(), request.getDirection(),
					request.getDestinationType(), request.getReceiveCountry());

		} catch (HibernateException e) {
			log.error("Exception While Getting DashBoard Info " + e);

			response = new AllDashBoardResponse();
			response.setResponseCode(Constant.ERROR_CODE);

		} catch (Exception e) {
			log.error("Exception In getDashBoardInfo() " + e);
			response = new AllDashBoardResponse();
			response.setResponseCode(Constant.ERROR_CODE);

		}

		return response;
	}

	/*
	 * This api returns all transaction details for that partner.
	 */
	@RequestMapping(value = "/getAllTransaction", method = RequestMethod.POST)
	public TransactionAllResponseDto getAllTransactions(@RequestBody AllTransactionRequestDto request) {

		TransactionAllResponseDto response = null;
		try {
			response = transService.getAllTransaction(request.getPartnerCode(), request.getStartDate(),
					request.getEndDate(), request.getStatus(), request.getProduct(), request.getDirection(),
					request.getDestinationType(), request.getReceiveCountry(), request.getFlag());

			if (response == null) {
				// log.info("No record found");
				response = new TransactionAllResponseDto();
				response.setResponseCode(Constant.INFO_CODE);
				response.setResponseMessage("No record found");
			}

		}

		catch (HibernateException e) {
			log.error("Exception In getAllTransactions() while Getting Transaction" + e.getMessage());
			response = new TransactionAllResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		} catch (Exception e) {
			log.error("Exception In getAllTransactions() " + e.getMessage());
			response = new TransactionAllResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		}

		return response;
	}

	/*
	 * this api returns transaction details against transaction id.
	 */
	@RequestMapping(value = "/getTransactionById", method = RequestMethod.POST)
	public TransactionByIdResponseDto getTransactionById(@RequestBody AllTransactionByIdRequestDto request) {

		TransactionByIdResponseDto response = null;
		try {
			response = transService.getTransactionById(request.getTransactionId());

			if (response != null) {
				response.setResponseCode(Constant.SUCCESS_CODE);
				response.setResponseMessage("All transaction by id");
			} else {
				// log.info("No record found");
				response = new TransactionByIdResponseDto();
				response.setResponseCode(Constant.INFO_CODE);
				response.setResponseMessage("No record found");

			}
		} catch (HibernateException e) {
			log.error("Exception In getTransactionById() While Getting TransactionById Info " + e);
			response = new TransactionByIdResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		} catch (Exception e) {
			log.error("Exception In getTransactionById() Info " + e);
			response = new TransactionByIdResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		}
		return response;
	}

	/*
	 * This api returns all balance record for that partner.
	 */
	@RequestMapping(value = "/getAllBalance", method = RequestMethod.POST)
	public BalanceResponseDto getAllBalance(@RequestBody AllBalanceRequestDto request) {

		BalanceResponseDto response = null;
		try {
			response = balanceService.getAllBalance(request.getPartnerCode(), request.getStartDate(),
					request.getEndDate(), request.getStatus(), request.getProduct(), request.getDirection(),
					request.getDestinationType(), request.getReceiveCountry(), request.getFlag());

			if (response == null) {

				// log.info("No record found");
				response = new BalanceResponseDto();
				response.setResponseCode(Constant.INFO_CODE);
				response.setResponseMessage("No record found");
			}

		} catch (HibernateException e) {
			log.error("Exception In getAllBalance() While Getting AllBalance Info " + e);
			response = new BalanceResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		} catch (Exception e) {
			log.error("Exception In getAllBalance() Info " + e.getMessage());
			response = new BalanceResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		}

		return response;
	}

	/*
	 * this api returns balance details against transaction id.
	 */
	@RequestMapping(value = "/getAllBalanceById", method = RequestMethod.POST)
	public TransactionByIdResponseDto getAllBalanceById(@RequestBody AllBalanceByIdRequestDto request) {

		TransactionByIdResponseDto response = null;
		try {
			response = balanceService.getAllBalanceById(request.getTransactionId());

			if (response != null) {
				response.setResponseCode(Constant.SUCCESS_CODE);
				response.setResponseMessage(" All Balance");
			} else {
				// log.info("No record found");
				response = new TransactionByIdResponseDto();
				response.setResponseCode(Constant.INFO_CODE);
				response.setResponseMessage("No record found");
			}

		} catch (HibernateException e) {
			log.error("Exception In getAllBalanceById() While Getting AllBalanceById Info " + e);
			response = new TransactionByIdResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		} catch (Exception e) {
			log.error("Exception In getAllBalanceById() Info " + e);
			response = new TransactionByIdResponseDto();
			response.setResponseCode(Constant.ERROR_CODE);

		}

		return response;
	}

	/*
	 * This api returns all organization.
	 */
	@RequestMapping(value = "/getOrganization")
	public List<GetOrganizationResponseDto> getOrganization() {

		List<GetOrganizationResponseDto> response = new ArrayList<GetOrganizationResponseDto>();
		GetOrganizationResponseDto res = new GetOrganizationResponseDto();
		try {
			response = getOrganizationService.getOrganization();

		} catch (HibernateException e) {
			log.error("Exception In getOrganization() While Getting AllBalanceById Info " + e);
			response = new ArrayList<GetOrganizationResponseDto>();
			res = new GetOrganizationResponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		} catch (Exception e) {
			log.error("Exception In getOrganization() Info " + e);
			response = new ArrayList<GetOrganizationResponseDto>();
			res = new GetOrganizationResponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		}

		return response;
	}

	/*
	 * This api returns transaction status for filter.
	 */
	@RequestMapping(value = "/getTransactionStatus")
	public List<TransactionStatusRsponseDto> getTransactionStatus() {

		List<TransactionStatusRsponseDto> response = new ArrayList<TransactionStatusRsponseDto>();
		TransactionStatusRsponseDto res = new TransactionStatusRsponseDto();
		try {
			response = transactionStatusService.getTransactionStatus();

		} catch (HibernateException e) {
			log.error("Exception In getTransactionStatus() While Getting TransactionStatus Info " + e);
			response = new ArrayList<TransactionStatusRsponseDto>();
			res = new TransactionStatusRsponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);

		} catch (Exception e) {
			log.error("Exception In getTransactionStatus() Info " + e);
			res = new TransactionStatusRsponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);

		}
		return response;
	}

	/*
	 * This api returns product for filter.
	 */
	@RequestMapping(value = "/getAllProduct")
	public List<MdmProductResponseDto> getAllProduct() {
		List<MdmProductResponseDto> response = new ArrayList<MdmProductResponseDto>();
		MdmProductResponseDto res = new MdmProductResponseDto();
		try {
			response = productService.getAllProduct();

		} catch (HibernateException e) {
			log.error("Exception In getAllProduct() While Getting AllProduct Info " + e);
			response = new ArrayList<MdmProductResponseDto>();
			res = new MdmProductResponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		} catch (Exception e) {
			log.error("Exception In getAllProduct() Info " + e);
			response = new ArrayList<MdmProductResponseDto>();
			res = new MdmProductResponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		}
		return response;
	}

	/*
	 * This api returns direction list for filter.
	 */
	@RequestMapping(value = "/getDirectionList")
	public List<MdmDirectionResonseDto> getDirectionList() {

		List<MdmDirectionResonseDto> response = new ArrayList<MdmDirectionResonseDto>();
		MdmDirectionResonseDto res = new MdmDirectionResonseDto();
		try {

			response = directionService.getDirectionList();

		} catch (HibernateException e) {
			log.error("Exception In getDirectionList() While Getting DirectionList Info " + e);
			response = new ArrayList<MdmDirectionResonseDto>();
			res = new MdmDirectionResonseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		} catch (Exception e) {
			log.error("Exception In getDirectionList() Info " + e);
			response = new ArrayList<MdmDirectionResonseDto>();
			res = new MdmDirectionResonseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		}
		return response;
	}

	/*
	 * This api returns destination type for filter.
	 */
	@RequestMapping(value = "/getDestinationTypeList")
	public List<MdmDestinationTypeResponseDto> getDestinationTypeList() {

		List<MdmDestinationTypeResponseDto> response = new ArrayList<MdmDestinationTypeResponseDto>();

		try {
			response = destinationType.getDestinationTypeList();

		} catch (HibernateException e) {
			log.error("Exception In getDestinationTypeList() While Getting DestinationTypeList Info " + e);

		} catch (Exception e) {
			log.error("Exception getDestinationTypeList() Info " + e);

		}
		return response;
	}

	/*
	 * This api return all countries list for filter.
	 */
	@RequestMapping(value = "/getAllCountries")
	public List<MdmCountryResponseDto> getAllCountries() {

		List<MdmCountryResponseDto> response = new ArrayList<MdmCountryResponseDto>();
		MdmCountryResponseDto res = new MdmCountryResponseDto();
		try {
			response = countryService.getAllCountries();

		} catch (HibernateException e) {
			log.error("Exception In getAllCountries() While Getting AllCountries Info " + e);
			response = new ArrayList<MdmCountryResponseDto>();
			res = new MdmCountryResponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		} catch (Exception e) {
			log.error("Exception In AllCountries Info " + e);
			response = new ArrayList<MdmCountryResponseDto>();
			res = new MdmCountryResponseDto();
			res.setResponseCode(Constant.ERROR_CODE);
			response.add(res);
		}
		return response;
	}

	//export transaction in excel
	@RequestMapping(value = "/genExcelForTransaction", method = RequestMethod.POST)
	public TransactionExcelDto generateExcelForTransaction(@RequestBody AllTransactionRequestDto request) {
		TransactionExcelDto response = new TransactionExcelDto();

		try {
			response = exportService.exportTransactionExcelService(request);
			response.setResponseCode(Constant.SUCCESS_CODE);

		} catch (Exception e) {
			log.error("Exception In generateExcelForTransaction() Info " + e);
			response = new TransactionExcelDto();
			response.setResponseCode(Constant.ERROR_CODE);

		}
		return response;
	}

	//export balance in excel
	@RequestMapping(value = "/genExcelForBalance", method = RequestMethod.POST)
	public BalanceExcelDto generateExcelForBalance(@RequestBody AllBalanceRequestDto request) {
		BalanceExcelDto response = new BalanceExcelDto();

		try {
			response = exportService.exportBalanceExcelService(request);
			response.setResponseCode(Constant.SUCCESS_CODE);

		} catch (Exception e) {
			log.error("Exception In generateExcelForBalance() Info " + e);
			response = new BalanceExcelDto();
			response.setResponseCode(Constant.ERROR_CODE);

		}
		return response;
	}

	//export report in excel
	@RequestMapping(value = "/generateExcelForReport", method = RequestMethod.POST)
	public ReportExcelDto generateExcelForReport(@RequestBody AllDashBoardRequestDto request) {
		ReportExcelDto response = new ReportExcelDto();
		try {
			response = exportService.exportReportExcelService(request);
		} catch (Exception e) {
			log.error("Exception In generateExcelForBalance() Info " + e);
			response = new ReportExcelDto();
			response.setResponseCode(Constant.ERROR_CODE);

		}
		return response;
	}

	// return transaction as per date
	@RequestMapping(value = "/getPartnerTransactionForDate", method = RequestMethod.POST)
	public TransactionDetailResponseDto getPartnerTransactionForDate(@RequestBody AllTransactionRequestDto request) {

		TransactionDetailResponseDto response = new TransactionDetailResponseDto();
		try {
			response = transService.getTransactionPerDatePerPartner(request.getPartnerCode(), request.getStartDate());

		} catch (HibernateException e) {
			log.error("Exception In getPartnerTransactionForDate() while Getting Transaction" + e);
			response = prepareErrorResponseData(e);
		} catch (Exception e) {
			log.error("Exception In getPartnerTransactionForDate() " + e);
			response = prepareErrorResponseData(e);
		}
		return response;
	}

	@SuppressWarnings("rawtypes")
	private TransactionDetailResponseDto prepareErrorResponseData(Exception e) {
		TransactionDetailResponseDto response = new TransactionDetailResponseDto();
		response.setResponseCode(Constant.ERROR_CODE);

		return response;
	}

}
