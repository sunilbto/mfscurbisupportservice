package com.mfs.curbi.support.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertDateToString {

	
	public static String convertDateToString(Date date) {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = dateFormat.format(date);

		return strDate;

	}
   
}
