package com.mfs.curbi.support.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * 
 * @author sandeep.jakkula This class will provide utility methods to check if
 *         String, List are empty or not.
 */

public class Format {

	/**
	 * log variable.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(Format.class);

	/** The err msg builder. */
	private StringBuilder errMsgBuilder;
	
	

	private StringBuilder getPreparedErrMsgBuffer() {
		if (errMsgBuilder == null) {
			errMsgBuilder = new StringBuilder(96);
		} else {
			errMsgBuilder.append("\n");
		}
		return errMsgBuilder;
	}

	

	public boolean isAnyError() {
		return errMsgBuilder != null;
	}

	public String getErrorMessages() {
		return errMsgBuilder != null ? errMsgBuilder.toString() : "";
	}

	public void appendErrorMessage(String userMessage) {
		getPreparedErrMsgBuffer().append(userMessage);
	}

	/**
	 * Method to check if String is not empty and not null.
	 * 
	 * @param arg
	 * @return Boolean
	 */
	public static Boolean isStringNotEmptyAndNotNull(String arg) {
		if (arg != null && !("").equals(arg) && !("").equals(arg.trim())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a List/Set is not null and not empty.
	 * 
	 * @param c
	 * @return Boolean
	 */
	public static Boolean isCollectionNotEmptyAndNotNull(Collection<?> arg) {
		return (arg != null && !arg.isEmpty());
	}

	/**
	 * Method to check if a Map is not null and not empty.
	 * 
	 * @param c
	 * @return Boolean
	 */
	public static Boolean isMapNotEmtyAndNotNull(Map<Object, Object> map) {
		if (map != null && !map.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a long is not 0 and not empty.
	 * 
	 * @param c
	 * @return Boolean
	 */
	public static Boolean isLongNotEmtyAndNotZero(long arg) {
		if (arg != 0 && !("").equals(arg)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a Date is not 0 and not empty.
	 * 
	 * @param c
	 * @return
	 */
	public static Boolean isDateNotEmtyAndNotNull(Date arg) {
		if (arg != null && !("").equals(arg)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if String is empty or null.
	 * 
	 * @param arg
	 * @return
	 */
	public static Boolean isStringEmptyOrNull(String arg) {
		if (("").equals(arg) || arg == null || ("").equals(arg.trim())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a List/Set is null or empty.
	 * 
	 * @param Collection
	 * @return true if list is null or empty
	 */
	public static Boolean isCollectionEmtyOrNull(Collection<?> arg) {
		if (arg == null || arg.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a long is not 0 and not empty.
	 * 
	 * @param Long
	 *            arg
	 * @return Boolean
	 */
	public static Boolean isLongNotEmtyAndNotZero(Long arg) {
		if (arg != null && arg != 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a Integer is not 0 and not empty.
	 * 
	 * @param Integer
	 *            arg
	 * @return Boolean
	 */
	public static Boolean isIntegerNotEmtyAndNotZero(Integer arg) {
		if (arg != null && arg != 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a String[] is null or empty.
	 * 
	 * @param String
	 *            []
	 * @return true if String[] is null or empty
	 */
	public static Boolean isStringArrayEmptyOrNull(String[] arg) {
		if (arg == null || arg.length == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a String[] is null or empty.
	 * 
	 * @param String
	 *            []
	 * @return true if String[] is null or empty
	 */
	public static Boolean isStringArrayNotEmptyAndNotNull(String[] arg) {
		if (arg == null || arg.length == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Method to check if an object is not null or zero.
	 * 
	 * @param arg
	 *            as Object
	 * @return true if object is not null or zero
	 */
	public static Boolean isObjectNotEmptyAndNotZero(Object arg) {
		if (arg != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to format date in dd/MM/yyyy hh:mm a.
	 * 
	 * @param date
	 *            as Date
	 * @return string date formatted
	 */
	public static String parseString(Date date) {
		SimpleDateFormat format = null;
		String dateStr = null;
		format = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		dateStr = format.format(date);
		return dateStr;
	}

	/**
	 * Method to format date in dd/MM/yyyy hh:mm a.
	 * 
	 * @param date
	 *            as Date
	 * @return string date formatted
	 */
	public static String parseStringForRvc(Date date) {
		SimpleDateFormat format = null;
		String dateStr = null;
		format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
		dateStr = format.format(date);
		return dateStr;
	}

	/**
	 * Method to format date in dd/MM/yyyy hh:mm a.
	 * 
	 * @param dateStr
	 *            as String
	 * @return formatted date
	 */
	public static Date parseDateNewFormat(String dateStr) {
		SimpleDateFormat format = null;
		Date date = null;
		try {
			format = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
			date = format.parse(dateStr);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
		} catch (Exception e) {
			LOGGER.error("Exception in " + e);
		}
		return date;
	}

	/**
	 * Method to format date in yyyy-MM-dd hh:mm a.
	 * 
	 * @param dateStr
	 *            as String
	 * @return formatted date
	 */
	public static Date parseDateYYYYMMDDFormat(String dateStr) {
		SimpleDateFormat format = null;
		Date date = null;
		try {
			format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
			date = format.parse(dateStr);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
		} catch (Exception e) {
			LOGGER.error("exception in " + e);
		}
		return date;
	}

	/**
	 * Method to format date in yyyy-MM-dd hh:mm a.
	 * 
	 * @param dateStr
	 *            as String
	 * @return formatted date
	 */
	public static Date parseDateFormatRVC(String dateStr) {
		SimpleDateFormat format = null;
		Date date = null;
		try {
			format = new SimpleDateFormat("dd-MM-yyyy hh:mm");
			date = format.parse(dateStr);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
		} catch (Exception e) {
			LOGGER.error("" + e);
		}

		return date;
	}

	public static void main(String[] args) {
		String dt = "16-05-2013 18:12 PM";
	}

	/**
	 * Method to format date in dd-MM-yyyy.
	 * 
	 * @param newDateStr
	 *            as String
	 * @return formatted date
	 */
	public static Date parseDateDDMMYYYYFormat(String newDateStr) {
		SimpleDateFormat format = null;
		Date date = null;
		try {
			format = new SimpleDateFormat("dd-MM-yyyy");
			date = format.parse(newDateStr);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
		} catch (Exception e) {
			LOGGER.error("" + e);
		}
		return date;
	}

	/**
	 * Method to check if an object is not empty or null.
	 * 
	 * @param arg
	 *            as Object
	 * @return true if object is not empty or null
	 */
	public static Boolean isObjectNotEmptyAndNotNull(Object arg) {
		if (arg != null && !arg.equals("")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if an object is not null.
	 * 
	 * @param arg
	 *            as Object
	 * @return true if object is not null
	 */
	public static Boolean isNotNull(Object obj) {
		if (obj != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author rajnikant.patel Method to check if an BigDecimal object is not null.
	 * @param arg
	 *            as BigDecimal object
	 * @return true if BigDecimal object is not null
	 */
	public static Boolean isNotNull(BigDecimal obj) {
		if (obj != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if an object is null.
	 * 
	 * @param arg
	 *            as Object
	 * @return true if object is null
	 */
	public static Boolean isNull(Object obj) {
		if (obj == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to format date in dd/MM/yyyy hh:mm a.
	 * 
	 * @param newDate
	 *            as Date
	 * @return formatted String date
	 */
	public static String formatDate(Date newDate) {
		SimpleDateFormat format = null;
		String date = null;
		format = new SimpleDateFormat("dd-MM-yyyy");
		date = format.format(newDate);
		return date;
	}

	/**
	 * Method to format date in dd.
	 * 
	 * @param newDate
	 *            as Date
	 * @return formatted String date
	 */
	public static String getDDFormatStr(Date newDate) {
		SimpleDateFormat format = null;
		String date = null;
		format = new SimpleDateFormat("dd");
		date = format.format(newDate);
		return date;
	}

	/**
	 * Method to format date in MM.
	 * 
	 * @param newDate
	 *            as Date
	 * @return formatted String date
	 */
	public static String getMMFormatStr(Date newDate) {
		SimpleDateFormat format = null;
		String date = null;
		format = new SimpleDateFormat("MM");
		date = format.format(newDate);
		return date;
	}

	/**
	 * Method to format date in yyyy.
	 * 
	 * @param newDate
	 *            as Date
	 * @return formatted String date
	 */
	public static String getYYYYFormatStr(Date newDate) {
		SimpleDateFormat format = null;
		String date = null;
		format = new SimpleDateFormat("yyyy");
		date = format.format(newDate);
		return date;
	}

	/**
	 * Method to check if an object is null or value null.
	 * 
	 * @param arg
	 *            as Object
	 * @return String
	 */
	public static String isNullToSpace(Object object) {
		if (object == null || "null".equalsIgnoreCase(object.toString())) {
			return "";
		} else {
			return object.toString();
		}
	}

	public static Long[] getIDFromList(List lists) {

		Long[] ids = new Long[lists.size()];

		for (int i = 0; i < lists.size(); i++) {
			Object genericObject = lists.get(i);
			//
			for (Field field : genericObject.getClass().getDeclaredFields()) {
				field.setAccessible(true);// Where is set back to false???????
				Object id = null;
				try {
					id = field.get(genericObject);
				} catch (Exception e) {
					LOGGER.error("" + e);
				}
				if (id != null) {
					ids[i] = Long.valueOf(String.valueOf(id));
				}

			}
		}
		return ids;
	}

	public static String convertToCommaDelimited(Long[] list) {
		StringBuffer ret = new StringBuffer("");
		for (int i = 0; list != null && i < list.length; i++) {
			if (list[i] != null && !"null".equalsIgnoreCase(list[i].toString())
					&& !"".equals(list[i].toString().trim())) {
				ret.append(list[i]);
				if (i < list.length - 1) {
					ret.append(',');
				}
			}

		}
		return ret.toString();
	}

	/**
	 * Method to check if an object is empty or null.
	 * 
	 * @param arg
	 *            as Object
	 * @return true if object is empty or null
	 */
	public static Boolean isObjectEmptyAndNull(Object arg) {
		return ("").equals(arg) ? true : (arg == null ? true : false);

		/*
		 * if (("").equals(arg) || arg == null) { return true; } else { return false; }
		 */
	}

	/**
	 * Method to check if date is empty or null
	 * 
	 * @param arg
	 * @return
	 */
	public static Boolean isDateEmptyOrNull(Object arg) {

		if (("").equals(arg) || arg == null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Method to check if a Float is not 0 and not empty.
	 * 
	 * @param Integer
	 *            arg
	 * @return Boolean
	 */
	public static Boolean isFloatNotEmtyAndNotZero(Float arg) {
		if (arg != null && arg != 0.0f) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isStringFieldContainingAllNumericValue(String fieldValue) {
		boolean result = true;
		if (result = isNotNull(fieldValue)) {
			Pattern p = Pattern.compile("^[-+]?[0-9]*\\.?[0-9]+$");
			if (p.matcher(fieldValue).matches()) {
				return true;
			} else {
				return false;
			}
		}
		return result;
	}

	public static Date convertStringFromdate(String inputdate )
	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		 Date date = new Date();
		 try {
			date = df.parse(inputdate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	public static Date convertStringTodate(String inputdate )
	{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		 Date date = new Date();
		 try {
			date = df.parse(inputdate);
			Calendar c = Calendar.getInstance(); 
			c.setTime(date); 
			c.add(Calendar.DATE, 1);
			date = c.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

}
