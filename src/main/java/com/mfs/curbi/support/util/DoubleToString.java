package com.mfs.curbi.support.util;

import java.text.NumberFormat;

public class DoubleToString {

	public static double takeToDigit(double value)
	   {
		  
		    
		   NumberFormat nf = NumberFormat.getInstance();
		   nf.setMaximumFractionDigits(2); 
		   String a=nf.format(value);
		   String amount=a.replaceAll("[/$,]", "");
			
		   double value1=Double.parseDouble(amount);  
		   	
		   return value1;   
	   }
}
