package com.mfs.curbi.support.util;

public class Constant {

	public static final String SUCCESS_CODE="200";
	
	public static final String INFO_CODE="E402";

	public static final String ERROR_CODE="E401";
	
	public static final String BASE_URl="base_uri";
	
	public static final String DATE_FORMAT="yyyy-MM-dd HH:mm:ss";

	public static final String DATE_FORMAT_YYYY_MM_DD ="yyyy-MM-dd";
	 
	public static final String DOWNLOAD_CODE="E403"; 
	
	public static final String CHANGE_DATE_RANGE_CODE="E404";

}
