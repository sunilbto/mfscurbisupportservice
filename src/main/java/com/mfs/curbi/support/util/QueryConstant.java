package com.mfs.curbi.support.util;

public class QueryConstant {

	//pricing query
	public static final String PRICING_QUERY="select mp.partner_name,count(*) as count, avg(fee_s_fx) as AverageSendFee,avg(fee_s_fx)+";
	public static final String PRICING_QUERY1="as AverageTotalCost,sum(s_amount_s_fx)/sum(fee_s_fx+";
	public static final String PRICING_QUERY2=") as AverageTotalCostPercentage ,partner_currency as partnerCurrency  from mdm_trans_partner_detail d join mdm_trans_amount t on d.transaction_detail_id=t.transaction_detail_id join mdm_partner mp on d.partner_code=mp.partner_code  where partner_direction='Send' and partner_country IN(";
	public static final String PRICING_QUERY3=") group by partner_name,partner_currency;";
	
	//compliance query
	public static final String COMPLIANCE_QUERY="select distinct(mp.partner_name),t.partner_country  from mdm_trans_partner_detail t join mdm_transaction_detail m on t.transaction_detail_id=m.id join mdm_partner mp on t.partner_code=mp.partner_code where t.partner_direction='Send' and t.partner_country IN( ";
	public static final String COMPLIANCE_QUERY1="  )and m.transaction_processed_date between '";
	public static final String COMPLIANCE_QUERY3="';";
	public static final String COMPLIANCE_QUERY4=" select distinct(mp.partner_name),t.partner_country from mdm_trans_partner_detail t join mdm_transaction_detail m on t.transaction_detail_id=m.id join mdm_partner mp on t.partner_code=mp.partner_code where t.partner_direction='Send' and m.id In(";
	public static final String COMPLIANCE_QUERY5=");";
	public static final String COMPLIANCE_QUERY6="select distinct(mp.partner_name),t.partner_country from mdm_trans_partner_detail t join mdm_transaction_detail m on t.transaction_detail_id=m.id join mdm_partner mp on t.partner_code=mp.partner_code where t.partner_direction='Receive' and m.id In(";
	public static final String COMPLIANCE_QUERY7="select transaction_detail_id from mdm_transaction_detail td Join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id  where tpd.partner_country IN(";
	
 //common query
	public static final String COMMON_QUERY3="' and '";
	public static final String COMMON_QUERY7=") AND tpd.partner_direction = 'Receive'  and td.transaction_processed_date between '";
	public static final String COMMON_QUERY2=") AND tpd.partner_direction = 'Send'  and td.transaction_processed_date between '";
	public static final String COMMON_QUERY14=")  AND td.transaction_processed_date between'";
	public static final String COMMON_QUERY="AND p.product_name='";
	public static final String COMMON_QUERY1=" AND til.destination_type='";
	public static final String COMMON_QUERY5=" join mdm_trans_instruct_log til on til.transaction_detail_id=td.id";
	public static final String COMMON_QUERY6=" join mdm_product p on til.product_id=p.id";
	public static final String COMMON_QUERY11=" where tpd.partner_country IN(";
	public static final String COMMON_QUERY4="' AND tpd.transaction_detail_id IN(";
	public static final String COMMON_QUERY9="select id,product_name,rate_profile,product_description from mdm_product";
	public static final String COMMON_QUERY8="select distinct(destination_type) from mdm_api_product_mapping";
	public static final String COMMON_QUERY10="select * from mdm_product";
	public static final String COMMON_QUERY12=") AND tpd.partner_direction =  '";
	public static final String COMMON_QUERY17="AND td.transaction_status='";
 	public static final String COMMON_QUERY18=" AND p.product_name='";
 	public static final String COMMON_QUERY19=" AND til.destination_type='";
 	public static final String COMMON_QUERY20=" AND tpd.partner_direction='";
 	public static final String COMMON_QUERY21=" AND tpd.partner_country='";
 	public static final String COMMON_QUERY22=" AND a.partner_direction='";
 	public static final String COMMON_QUERY23=" AND a.partner_country='";
	public static final String COMMON_QUERY13="' AND td.transaction_processed_date between '";
	public static final String COMMON_QUERY16="select  tpd.transaction_detail_id  from    mdm_transaction_detail td  Join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id ";
	public static final String COMMON_QUERY15="' AND tpd.partner_direction='";
	
	//statistic consume query
	public static final String STATISTIC_CONSUME_QUERY="select transaction_processed_date,sum(if(partner_direction = 'Send', Trans_count, 0 )) as send_count,sum(if(partner_direction = 'Receive', Trans_count, 0 )) as receive_count ";
	public static final String STATISTIC_CONSUME_QUERY1="from ( select count(distinct(partner_code)) as Trans_count,Date(transaction_processed_date) as transaction_processed_date, partner_direction ";
	public static final String STATISTIC_CONSUME_QUERY2="from mdm_trans_partner_detail tpd Join mdm_transaction_detail td on td.id = tpd.transaction_detail_id Join mdm_trans_instruct_log til ON til.transaction_detail_id=td.id   JOIN mdm_product p ON til.product_id = p.id  where  partner_country in (";
	public static final String STATISTIC_CONSUME_QUERY4=" group by date(transaction_processed_date), partner_direction) as T group by transaction_processed_date;";
	
	//statistic transaction query
	public static final String STATISTIC_TRANSACTION_QUERY="select Format(avg(ta.s_amount_spc),2) as avg_amount,";
	public static final String STATISTIC_TRANSACTION_QUERY1=" date(td.transaction_processed_date) as transactionDate ,date(td.transaction_processed_date) as processedDate";
	public static final String STATISTIC_TRANSACTION_QUERY2=" week(td.transaction_processed_date) as transactionDate ,week(td.transaction_processed_date) as processedDate ";
	public static final String STATISTIC_TRANSACTION_QUERY3=" month(td.transaction_processed_date) as transactionDate ,Month(td.transaction_processed_date) as processedDate ";
	public static final String STATISTIC_TRANSACTION_QUERY4=" from mdm_transaction_detail td join mdm_trans_partner_detail tpd on tpd.transaction_detail_id=td.id Join mdm_trans_amount ta on td.id = ta.transaction_detail_id ";
	public static final String STATISTIC_TRANSACTION_QUERY8="group by transactionDate ,processedDate ;";
	public static final String STATISTIC_TRANSACTION_QUERY5=" where tpd.partner_country in (";
	public static final String STATISTIC_TRANSACTION_QUERY6="select avg(ta.s_amount_spc) as totalAvg "; 
	//country query
	public static final String COUNTRY_QUERY="select id,country_name,country_code,numeric_code,phone_code from mdm_country";
	
	//direction query
	public static final String DIRECTION_QUERY="select id,direction_name,description from mdm_direction";
	
	//organization query
	public static final String ORGANIZATION_QUERY="select id,partner_name,partner_code from mdm_partner";
	
	//transaction query
	public static final String TRANSACTION_STATUS_QUERY="select distinct(transaction_status) from mdm_transaction_detail";
	
	//moneyflow query
	public static final String MONEYFLOW_QUERY="select   amount as value_currency,  s_amount_spc as value_spc,  tpd.partner_currency as currency from    mdm_transaction_detail td  Join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id   Join mdm_trans_amount ta ON td.id = ta.transaction_detail_id ";
	public static final String MONEYFLOW_QUERY1="select Partner_country,sum(amount) as value_currency,Sum(s_amount_spc) as value_spc,avg(amount) as totalAmount,count(*) as trans_count from  mdm_transaction_detail td Join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id Join mdm_trans_amount ta ON td.id = ta.transaction_detail_id where tpd.partner_direction = 'Receive' And tpd.transaction_detail_id IN (";
	public static final String MONEYFLOW_QUERY2=" group by  partner_country order by value_currency Desc ;";
	public static final String MONEYFLOW_QUERY3=" select tpd.partner_currency as currency,sum(amount) as Trans_amount, count(*) as Trans_count,Sum(s_amount_spc) as value_spc,Date(transaction_processed_date) as transactionDate from  mdm_transaction_detail td  join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id Join mdm_trans_amount ta ON td.id = ta.transaction_detail_id";
	public static final String MONEYFLOW_QUERY4="  group by  Date(transaction_processed_date),currency ;";
	public static final String MONEYFLOW_QUERY5="select partner_country,sum(amount) as Value_currency, Sum(s_amount_spc) as value_spc, avg(amount) as totalAmount, count(*) as trans_count from  mdm_transaction_detail td Join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id Join mdm_trans_amount ta ON td.id = ta.transaction_detail_id ";
 	public static final String MONEYFLOW_QUERY6="select Partner_country,sum(amount) as value_currency,Sum(s_amount_spc) as value_spc, avg(amount) as totalAmount, count(*) as trans_count from mdm_transaction_detail td Join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id Join mdm_trans_amount ta ON td.id = ta.transaction_detail_id where tpd.partner_direction = 'Send' And tpd.transaction_detail_id IN (";
 	public static final String MONEYFLOW_QUERY7=" where td.id IN (";
 	public static final String MONEYFLOW_QUERY8=" and tpd.partner_direction = 'Receive'";
 	public static final String MONEYFLOW_QUERY9=" and tpd.partner_direction = 'Send'";
 	
 	//dashborad query
 	public static final String DASHBORAD_QUERY="select a.partner_id,Date(transaction_processed_date) as date, count(*) as count, sum(amount) as amount , sum(commission) as commission , sum(fee_rev_share) as fee_rev_share , sum(forex_rev_share) as forex_rev_share from mdm_trans_partner_detail a join mdm_transaction_detail td join mdm_trans_instruct_log til ON til.transaction_detail_id=td.id join mdm_product p ON til.product_id = p.id join mdm_treasury_nsp_ledger c on a.transaction_detail_id = td.id and td.trans_mfs_id = c.mfs_id And a.partner_id=c.partner_id where  a.partner_code=";
 	public static final String DASHBORAD_QUERY1=" group by Date(transaction_processed_date),a.partner_id;";
 	public static final String DASHBORAD_QUERY2="SELECT  tnl.after_balance AS afterBalance, tnl.partner_settl_ccy AS partnerSettlCcy FROM mdm_trans_partner_detail tpd JOIN mdm_transaction_detail td ON tpd.transaction_detail_id = td.id JOIN mdm_treasury_nsp_ledger tnl ON td.trans_mfs_id = tnl.mfs_id  WHERE tpd.partner_code = '";
 	public static final String DASHBORAD_QUERY3="AND tnl.partner_id = tpd.partner_id order by tnl.id DESC limit 1;";
 	public static final String DASHBORAD_QUERY4=" AND td.transaction_processed_date between '";
 	
 	
 	//transaction query
 	public static final String TRANSACTION_QUERY="SELECT tpd.partner_direction AS partnerDirection, tpd.transaction_detail_id AS transaction_detail_id, tpd.partner_country AS receiveCountry,td.trans_mfs_id AS mfsId, td.transaction_status AS Status,td.transaction_processed_date AS valueDate,til.destination_type AS destinationType,til.product_id AS producId,p.product_name AS productName,if(partner_direction = 'Send', spTransactionId, rpTransactionId)  AS thirdPartyTransactionId FROM mdm_trans_partner_detail tpd JOIN mdm_transaction_detail td ON td.id=tpd.transaction_detail_id JOIN mdm_trans_instruct_log til ON til.transaction_detail_id=td.id JOIN mdm_product p ON til.product_id = p.id WHERE tpd.partner_code = ";
 	public static final String TRANSACTION_QUERY1=" AND transaction_processed_date between '";
 	public static final String TRANSACTION_QUERY2="select tpd.transaction_detail_id AS transaction_detail_id,tpd.partner_direction,tpd.partner_country,td.trans_mfs_id AS mfsId,td.transaction_status AS Status, td.transaction_processed_date AS ValueDate,p.product_name,til.destination_type,tnl.before_balance AS beforeBalance,tnl.principal_amount AS Principal, tnl.commission AS Commission,tnl.fee_rev_share AS FeeShare,tnl.forex_rev_share AS fxShare,tnl.after_balance AS afterBalance,tnl.movement_type AS Type,c.currency_code AS Ccy from mdm_transaction_detail td Join mdm_trans_partner_detail tpd on td.id = tpd.transaction_detail_id Join mdm_treasury_nsp_ledger tnl on td.trans_mfs_id = tnl.mfs_id Join mdm_trans_instruct_log til ON til.transaction_detail_id=td.id join mdm_currency c on tnl.currency_id = c.id JOIN mdm_product p ON til.product_id = p.id where tpd.partner_code = ";
 	public static final String TRANSACTION_QUERY3=" AND tnl.partner_id =tpd.partner_id  AND transaction_processed_date between '";
 	public static final String TRANSACTION_QUERY4="SELECT td.transaction_created_date AS creationDate,td.transaction_processed_date AS processedDate,td.transaction_status AS transactionStatus,td.trans_mfs_id,tpsd.partner_direction as sendPartnerDirection,tpsd.partner_name AS sendPartner,tprd.partner_name AS receiveParner,tpsd.partner_currency AS sendCurrency,tpsd.partner_country AS sendCountry,tpsd.amount AS sendAmount,tpsd.fee AS sendFee,tprd.partner_currency AS receiveCurrency,tprd.partner_country AS recieveCountry,tprd.amount AS receiveAmount,tprd.fee AS receiveFee,tprd.partner_direction,td.transaction_processed_date AS valueDate,tnl.before_balance AS balanceBefore,tnl.principal_amount AS principal,tnl.commission AS commission,tnl.fee_rev_share AS feeRevShare,tnl.forex_rev_share AS forexRevShare,tnl.after_balance AS balanceAfter,tnl.movement_type AS movementType,tnl.partner_settl_ccy AS settlmentCurrency,td.id as tdid,tpsd.id as tpsdid,tprd.id as tprdid,tnl.id as tnlid,tpsd.partner_id As sPartnerId,tprd.partner_id AS rPsrtnerId,til.product_id AS producId,til.destination_type AS destinationType,p.product_name AS productName,tfr.fx_rate As fxRate FROM mdm_transaction_detail td JOIN mdm_trans_partner_detail tpsd ON tpsd.transaction_detail_id=td.id JOIN mdm_trans_partner_detail tprd ON tprd.transaction_detail_id=td.id JOIN mdm_treasury_nsp_ledger tnl ON tnl.mfs_id=td.trans_mfs_id and tnl.partner_id = tpsd.partner_id JOIN mdm_trans_instruct_log til ON til.transaction_detail_id=td.id JOIN mdm_product p ON til.product_id = p.id JOIN mdm_trans_fx_rate tfr On tfr.transaction_detail_id=td.id WHERE tpsd.transaction_detail_id = tprd.transaction_detail_id and tpsd.transaction_detail_id = ";
 	public static final String TRANSACTION_QUERY5=" and tpsd.partner_direction = 'Send'  and tprd.partner_direction = 'Receive' AND tnl.movement_type = 'Transaction'; ";
 	public static final String TRANSACTION_QUERY6="SELECT tnl.after_balance AS afterBalance,tnl.partner_settl_ccy AS partnerSettlCcy FROM mdm_trans_partner_detail tpd JOIN mdm_transaction_detail td ON tpd.transaction_detail_id = td.id JOIN mdm_treasury_nsp_ledger tnl ON td.trans_mfs_id = tnl.mfs_id WHERE tpd.partner_code = ";
 	public static final String TRANSACTION_QUERY7=" AND tnl.partner_id = tpd.partner_id order by tnl.id DESC limit 1;";
 	public static final String TRANSACTION_QUERY8="SELECT td.transaction_created_date AS creationDate, td.transaction_processed_date AS processedDate,td.transaction_status AS transactionStatus, td.trans_mfs_id,tpsd.partner_direction, tpsd.partner_name AS sendPartner,tprd.partner_name AS receiveParner, tpsd.partner_currency AS sendCurrency,tpsd.partner_country AS sendCountry, tpsd.amount AS sendAmount,tpsd.fee AS sendFee, tprd.partner_currency AS receiveCurrency,tprd.partner_country AS recieveCountry, tprd.amount AS receiveAmount, tprd.fee AS receiveFee, tprd.partner_direction,td.transaction_processed_date AS valueDate, tnl.before_balance AS balanceBefore,tnl.principal_amount AS principal, tnl.commission AS commission,tnl.fee_rev_share AS feeRevShare, tnl.forex_rev_share AS forexRevShare,tnl.after_balance AS balanceAfter, tnl.movement_type AS movementType,tnl.partner_settl_ccy AS settlmentCurrency, til.product_id AS producId,til.destination_type AS destinationType, p.product_name AS productName, tfr.fx_rate As fxRate FROM mdm_transaction_detail td JOIN mdm_trans_partner_detail tpsd ON tpsd.transaction_detail_id=td.id JOIN mdm_trans_partner_detail tprd ON tprd.transaction_detail_id=td.id JOIN mdm_treasury_nsp_ledger tnl ON tnl.mfs_id=td.trans_mfs_id and tnl.partner_id = tpsd.partner_id JOIN mdm_trans_instruct_log til ON til.transaction_detail_id=td.id JOIN mdm_product p ON til.product_id = p.id JOIN mdm_trans_fx_rate tfr On tfr.transaction_detail_id=td.id WHERE tpsd.transaction_detail_id = tprd.transaction_detail_id AND (tpsd.partner_code =  '";
 	public static final String TRANSACTION_QUERY9="' OR tprd.partner_code = '";
 	public static final String TRANSACTION_QUERY10="') AND tpsd.partner_direction = 'Send' AND tprd.partner_direction = 'Receive' AND DATE(td.transaction_processed_date) = '";
 	

}
