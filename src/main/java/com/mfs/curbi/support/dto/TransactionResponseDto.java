package com.mfs.curbi.support.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionResponseDto {

	private double currentBalance;
	private List<AllTransactionDetailsResponseDto> transaction;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public List<AllTransactionDetailsResponseDto> getTransaction() {
		return transaction;
	}

	public void setTransaction(List<AllTransactionDetailsResponseDto> transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		return "TransactionResponseDto [CurrentBalance=" + currentBalance + ", transaction=" + transaction
				+ ", responseCode=" + responseCode + ", responseMessage=" + responseMessage + ", exception=" + exception
				+ "]";
	}

}
