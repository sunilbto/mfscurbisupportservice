package com.mfs.curbi.support.dto;

import java.util.Date;
import java.util.List;

public class AllDashBoardResponse {

	private double finalTransactionCount;

	private List<Date> processedDateList;

	private List<Double> transactionCountList;

	private double finalAmount;

	private List<Double> amountList;

	private double currentBalance;

	private double commission;

	private double feeshare;

	private double fxShare;

	private String ccy;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public double getFinalTransactionCount() {
		return finalTransactionCount;
	}

	public void setFinalTransactionCount(double finalTransactionCount) {
		this.finalTransactionCount = finalTransactionCount;
	}

	public List<Double> getTransactionCountList() {
		return transactionCountList;
	}

	public void setTransactionCountList(List<Double> transactionCountList) {
		this.transactionCountList = transactionCountList;
	}

	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public List<Double> getAmountList() {
		return amountList;
	}

	public void setAmountList(List<Double> amountList) {
		this.amountList = amountList;
	}

	public List<Date> getProcessedDateList() {
		return processedDateList;
	}

	public void setProcessedDateList(List<Date> processedDateList) {
		this.processedDateList = processedDateList;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getFeeshare() {
		return feeshare;
	}

	public void setFeeshare(double feeshare) {
		this.feeshare = feeshare;
	}

	public double getFxShare() {
		return fxShare;
	}

	public void setFxShare(double fxShare) {
		this.fxShare = fxShare;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "AllDashBoardResponse [finalTransactionCount=" + finalTransactionCount + ", processedDateList="
				+ processedDateList + ", transactionCountList=" + transactionCountList + ", finalAmount=" + finalAmount
				+ ", amountList=" + amountList + ", currentBalance=" + currentBalance + ", commission=" + commission
				+ ", feeshare=" + feeshare + ", fxShare=" + fxShare + ", ccy=" + ccy + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

}
