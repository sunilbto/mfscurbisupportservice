package com.mfs.curbi.support.dto;

public class PricingResponseRowDto {

	private String partnerName;
	private double averageSendFee;
	private double averageTotalCost;
	private double averageTotalCostPer;
	private String partnerCurrency;
	private double totalCount;
	public double getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(double totalCount) {
		this.totalCount = totalCount;
	}
	public String getPartnerCurrency() {
		return partnerCurrency;
	}
	public void setPartnerCurrency(String partnerCurrency) {
		this.partnerCurrency = partnerCurrency;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public double getAverageSendFee() {
		return averageSendFee;
	}
	public void setAverageSendFee(double averageSendFee) {
		this.averageSendFee = averageSendFee;
	}
	public double getAverageTotalCost() {
		return averageTotalCost;
	}
	public void setAverageTotalCost(double averageTotalCost) {
		this.averageTotalCost = averageTotalCost;
	}
	public double getAverageTotalCostPer() {
		return averageTotalCostPer;
	}
	public void setAverageTotalCostPer(double averageTotalCostPer) {
		this.averageTotalCostPer = averageTotalCostPer;
	}
	@Override
	public String toString() {
		return "PricingResponseRowDto [partnerName=" + partnerName + ", averageSendFee=" + averageSendFee
				+ ", averageTotalCost=" + averageTotalCost + ", averageTotalCostPer=" + averageTotalCostPer
				+ ", partnerCurrency=" + partnerCurrency + ", totalCount=" + totalCount + "]";
	}

}
