package com.mfs.curbi.support.dto;

import java.util.Date;

public class AllTransactionResponseDto {

	private String direction;

	private int transaction_detail_id;

	private String mfsId;

	private String status;

	private Date valueDate;

	private int productId;

	private String product;

	private String thirdPartyTransactionId;
	
	private String destinationType;
	
	private String receiveCountry;

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getTransaction_detail_id() {
		return transaction_detail_id;
	}

	public void setTransaction_detail_id(int transaction_detail_id) {
		this.transaction_detail_id = transaction_detail_id;
	}

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getThirdPartyTransactionId() {
		return thirdPartyTransactionId;
	}

	public void setThirdPartyTransactionId(String thirdPartyTransactionId) {
		this.thirdPartyTransactionId = thirdPartyTransactionId;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	@Override
	public String toString() {
		return "AllTransactionResponseDto [direction=" + direction + ", transaction_detail_id=" + transaction_detail_id
				+ ", mfsId=" + mfsId + ", status=" + status + ", valueDate=" + valueDate + ", productId=" + productId
				+ ", product=" + product + ", thirdPartyTransactionId=" + thirdPartyTransactionId + ", destinationType="
				+ destinationType + ", receiveCountry=" + receiveCountry + "]";
	}


	

}
