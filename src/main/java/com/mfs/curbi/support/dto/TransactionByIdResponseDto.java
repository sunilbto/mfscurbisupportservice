package com.mfs.curbi.support.dto;

import java.util.List;

public class TransactionByIdResponseDto {

	private List<AllTransactionByIdResponseDto> transactionDetail;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public List<AllTransactionByIdResponseDto> getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(List<AllTransactionByIdResponseDto> transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "TransactionByIdResponseDto [transactionDetail=" + transactionDetail + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

	

}
