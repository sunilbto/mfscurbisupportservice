package com.mfs.curbi.support.dto;

import java.util.Date;

public class TmpNspLedBalanceResponseDto {

	private int tmpNspLedBalanceId;

	private int partnerId;

	private double beforeBalance;

	private double afterBalance;

	private String partnerSettlCcy;

	private Date valueDate;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public int getTmpNspLedBalanceId() {
		return tmpNspLedBalanceId;
	}

	public void setTmpNspLedBalanceId(int tmpNspLedBalanceId) {
		this.tmpNspLedBalanceId = tmpNspLedBalanceId;
	}

	public int getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public double getBeforeBalance() {
		return beforeBalance;
	}

	public void setBeforeBalance(double beforeBalance) {
		this.beforeBalance = beforeBalance;
	}

	public double getAfterBalance() {
		return afterBalance;
	}

	public void setAfterBalance(double afterBalance) {
		this.afterBalance = afterBalance;
	}

	public String getPartnerSettlCcy() {
		return partnerSettlCcy;
	}

	public void setPartnerSettlCcy(String partnerSettlCcy) {
		this.partnerSettlCcy = partnerSettlCcy;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "TmpNspLedBalanceResponseDto [tmpNspLedBalanceId=" + tmpNspLedBalanceId + ", partnerId=" + partnerId
				+ ", beforeBalance=" + beforeBalance + ", afterBalance=" + afterBalance + ", partnerSettlCcy="
				+ partnerSettlCcy + ", valueDate=" + valueDate + ", responseCode=" + responseCode + ", responseMessage="
				+ responseMessage + ", exception=" + exception + "]";
	}

}
