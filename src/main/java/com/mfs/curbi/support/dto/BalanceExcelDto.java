package com.mfs.curbi.support.dto;

public class BalanceExcelDto {
	
	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "BalanceExcelDto [responseCode=" + responseCode + ", responseMessage=" + responseMessage + ", exception="
				+ exception + "]";
	}
	
	

}
