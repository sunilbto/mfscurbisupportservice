package com.mfs.curbi.support.dto;

import java.util.List;

public class TransactionAllResponseDto {


	private List<AllTransactionResponseDto> allTransactionResponse;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public List<AllTransactionResponseDto> getAllTransactionResponse() {
		return allTransactionResponse;
	}

	public void setAllTransactionResponse(List<AllTransactionResponseDto> allTransactionResponse) {
		this.allTransactionResponse = allTransactionResponse;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "TransactionAllResponseDto [allTransactionResponse=" + allTransactionResponse + ", responseCode="
				+ responseCode + ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

}
