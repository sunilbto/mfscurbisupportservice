package com.mfs.curbi.support.dto;

public class ApiProductMappingResponseDto {

	private int apiProductMappingId;

	private String amountType;

	private String destionationType;

	private int apiId;

	private int productId;

	public int getApiProductMappingId() {
		return apiProductMappingId;
	}

	public void setApiProductMappingId(int apiProductMappingId) {
		this.apiProductMappingId = apiProductMappingId;
	}

	public String getAmountType() {
		return amountType;
	}

	public void setAmountType(String amountType) {
		this.amountType = amountType;
	}

	public String getDestionationType() {
		return destionationType;
	}

	public void setDestionationType(String destionationType) {
		this.destionationType = destionationType;
	}

	public int getApiId() {
		return apiId;
	}

	public void setApiId(int apiId) {
		this.apiId = apiId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "ApiProductMappingResponseDto [apiProductMappingId=" + apiProductMappingId + ", amountType=" + amountType
				+ ", destionationType=" + destionationType + ", apiId=" + apiId + ", productId=" + productId + "]";
	}

}
