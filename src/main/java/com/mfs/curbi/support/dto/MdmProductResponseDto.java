package com.mfs.curbi.support.dto;

public class MdmProductResponseDto {
	private int mdmProductId;

	private String productName;

	private int rateProfile;

	private String productDescription;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public int getMdmProductId() {
		return mdmProductId;
	}

	public void setMdmProductId(int mdmProductId) {
		this.mdmProductId = mdmProductId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getRateProfile() {
		return rateProfile;
	}

	public void setRateProfile(int rateProfile) {
		this.rateProfile = rateProfile;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "MdmProductResponseDto [mdmProductId=" + mdmProductId + ", productName=" + productName + ", rateProfile="
				+ rateProfile + ", productDescription=" + productDescription + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

}
