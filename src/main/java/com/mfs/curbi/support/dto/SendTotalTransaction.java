package com.mfs.curbi.support.dto;

public class SendTotalTransaction {

	private String partnerName;
	private double amount;
	private double spcAmount;
	private int txnCount;
	private String currency;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getSpcAmount() {
		return spcAmount;
	}

	public void setSpcAmount(double spcAmount) {
		this.spcAmount = spcAmount;
	}

	

	

	public int getTxnCount() {
		return txnCount;
	}

	public void setTxnCount(int txnCount) {
		this.txnCount = txnCount;
	}

	@Override
	public String toString() {
		return "SendTotalTransaction [partnerName=" + partnerName + ", amount=" + amount + ", spcAmount=" + spcAmount
				+ ", txnCount=" + txnCount + ", currency=" + currency + "]";
	}

	


}
