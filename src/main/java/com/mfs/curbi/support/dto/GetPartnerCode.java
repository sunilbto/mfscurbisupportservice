package com.mfs.curbi.support.dto;

public class GetPartnerCode {
	
	private String partnerCode;

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Override
	public String toString() {
		return "GetPartnerCode [partnerCode=" + partnerCode + "]";
	}
	
	

}
