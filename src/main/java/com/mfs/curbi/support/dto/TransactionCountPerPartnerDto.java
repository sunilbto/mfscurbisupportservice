package com.mfs.curbi.support.dto;

public class TransactionCountPerPartnerDto {

	private int transCount;
	private String parterName;
	private String transDate;

	public int getTransCount() {
		return transCount;
	}

	public void setTransCount(int transCount) {
		this.transCount = transCount;
	}

	public String getParterName() {
		return parterName;
	}

	public void setParterName(String parterName) {
		this.parterName = parterName;
	}

	
	
	public String getTransDate() {
		return transDate;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	@Override
	public String toString() {
		return "TransactionCountPerPartnerDto [transCount=" + transCount + ", parterName=" + parterName + ", transDate="
				+ transDate + "]";
	}

}
