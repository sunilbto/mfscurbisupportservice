package com.mfs.curbi.support.dto;

import java.util.List;

public class FinalStatisticTransactionResponseDto {
	
	List<StatisticTransactionSendToResponseDto> sendToCountry;
	
	List<StatisticTransactionSendFromResponseDto> sendFromCountry;
	
	double totalAverageSendFrom;
	double totalAverageSendTo;



	

	public double getTotalAverageSendFrom() {
		return totalAverageSendFrom;
	}

	public void setTotalAverageSendFrom(double totalAverageSendFrom) {
		this.totalAverageSendFrom = totalAverageSendFrom;
	}

	public double getTotalAverageSendTo() {
		return totalAverageSendTo;
	}

	public void setTotalAverageSendTo(double totalAverageSendTo) {
		this.totalAverageSendTo = totalAverageSendTo;
	}

	public List<StatisticTransactionSendToResponseDto> getSendToCountry() {
		return sendToCountry;
	}

	public void setSendToCountry(List<StatisticTransactionSendToResponseDto> sendToCountry) {
		this.sendToCountry = sendToCountry;
	}

	public List<StatisticTransactionSendFromResponseDto> getSendFromCountry() {
		return sendFromCountry;
	}

	public void setSendFromCountry(List<StatisticTransactionSendFromResponseDto> sendFromCountry) {
		this.sendFromCountry = sendFromCountry;
	}


	@Override
	public String toString() {
		return "FinalStatisticTransactionResponseDto [sendToCountry=" + sendToCountry + ", sendFromCountry="
				+ sendFromCountry + ", totalAverageSendFrom=" + totalAverageSendFrom + ", totalAverageSendTo="
				+ totalAverageSendTo + "]";
	}
	
	

}
