package com.mfs.curbi.support.dto;

import java.util.List;

public class PricingFinalResponseDto {
	
	private List<PricingResponseDto> pricingResponse;
	
	private double totalAverageCost;
	
	private double totalAverageCostPer;

	public List<PricingResponseDto> getPricingResponse() {
		return pricingResponse;
	}

	public void setPricingResponse(List<PricingResponseDto> pricingResponse) {
		this.pricingResponse = pricingResponse;
	}

	public double getTotalAverageCost() {
		return totalAverageCost;
	}

	public void setTotalAverageCost(double totalAverageCost) {
		this.totalAverageCost = totalAverageCost;
	}

	public double getTotalAverageCostPer() {
		return totalAverageCostPer;
	}

	public void setTotalAverageCostPer(double totalAverageCostPer) {
		this.totalAverageCostPer = totalAverageCostPer;
	}

	@Override
	public String toString() {
		return "PricingFinalResponseDto [pricingResponse=" + pricingResponse + ", totalAverageCost=" + totalAverageCost
				+ ", totalAverageCostPer=" + totalAverageCostPer + "]";
	}

	

	
	

}
