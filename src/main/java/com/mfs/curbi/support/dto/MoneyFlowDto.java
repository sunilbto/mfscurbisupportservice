package com.mfs.curbi.support.dto;

public class MoneyFlowDto {

	private String countryNames;
	private String type;
	private String startDate;
	private String endDate;
	private String product;
	private String destinationType;

	public String getCountryNames() {
		return countryNames;
	}

	public void setCountryNames(String countryNames) {
		this.countryNames = countryNames;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	@Override
	public String toString() {
		return "MoneyFlowDto [countryNames=" + countryNames + ", type=" + type + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", product=" + product + ", destinationType=" + destinationType + "]";
	}

}