package com.mfs.curbi.support.dto;

public class StatisticTransactionSendFromResponseDto {

	//private int count;

	private String processedDate;

	private double avgAmount;

	private String date;

	/*
	 * public int getCount() { return count; }
	 * 
	 * public void setCount(int count) { this.count = count; }
	 */

	public String getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}

	public double getAvgAmount() {
		return avgAmount;
	}

	public void setAvgAmount(double avgAmount) {
		this.avgAmount = avgAmount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "StatisticTransactionSendFromResponseDto [processedDate=" + processedDate + ", avgAmount=" + avgAmount
				+ ", date=" + date + "]";
	}

}
