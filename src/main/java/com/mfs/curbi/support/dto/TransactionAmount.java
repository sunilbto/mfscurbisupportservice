package com.mfs.curbi.support.dto;

public class TransactionAmount {
	
	private double transactionAmount;

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	@Override
	public String toString() {
		return "TransactionAmount [transactionAmount=" + transactionAmount + "]";
	}
	

}
