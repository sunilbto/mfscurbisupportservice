package com.mfs.curbi.support.dto;

public class AllTransactionByIdRequestDto {

	private int transactionId;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "AllTransactionRequestDto [transactionId=" + transactionId + "]";
	}

}
