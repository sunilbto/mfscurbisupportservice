package com.mfs.curbi.support.dto;

public class PricingRequestDto {
	
    private String countryName;
	
	private double forexMargin;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public double getForexMargin() {
		return forexMargin;
	}

	public void setForexMargin(double forexMargin) {
		this.forexMargin = forexMargin;
	}

	@Override
	public String toString() {
		return "PricingRequestDto [countryName=" + countryName + ", forexMargin=" + forexMargin + "]";
	}

   	


	
	
	
	

}
