package com.mfs.curbi.support.dto;

public class TrasnactionId {

	private String transactionId;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "\'" + transactionId + "\'";
	}

}
