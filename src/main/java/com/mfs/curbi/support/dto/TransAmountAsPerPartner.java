package com.mfs.curbi.support.dto;

public class TransAmountAsPerPartner {

	private double transAmount;
	private String transDate;
	private int transCount;
	private double spcAmount;
	private String currencyCode;

	public int getTransCount() {
		return transCount;
	}

	public void setTransCount(int transCount) {
		this.transCount = transCount;
	}

	public String getTransDate() {
		return transDate;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public double getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(double transAmount) {
		this.transAmount = transAmount;
	}

	public double getSpcAmount() {
		return spcAmount;
	}

	public void setSpcAmount(double spcAmount) {
		this.spcAmount = spcAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	@Override
	public String toString() {
		return "TransAmountAsPerPartner [transAmount=" + transAmount + ", transDate=" + transDate + ", transCount="
				+ transCount + "]";
	}

}
