package com.mfs.curbi.support.dto;

public class BalanceDetailResponseDto {

	private String valueDate;

	private double beforeBalance;

	private double principle;

	private double commission;

	private double feeshare;

	private double fxShare;

	private double afterBalance;

	private String ccy;

	private String type;

	private String mfsId;
	
	private int transactionId;

	public double getBeforeBalance() {
		return beforeBalance;
	}

	public void setBeforeBalance(double beforeBalance) {
		this.beforeBalance = beforeBalance;
	}

	public double getPrinciple() {
		return principle;
	}

	public void setPrinciple(double principle) {
		this.principle = principle;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getFeeshare() {
		return feeshare;
	}

	public void setFeeshare(double feeshare) {
		this.feeshare = feeshare;
	}

	public double getFxShare() {
		return fxShare;
	}

	public void setFxShare(double fxShare) {
		this.fxShare = fxShare;
	}

	public double getAfterBalance() {
		return afterBalance;
	}

	public void setAfterBalance(double afterBalance) {
		this.afterBalance = afterBalance;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "BalanceDetailResponseDto [valueDate=" + valueDate + ", beforeBalance=" + beforeBalance + ", principle="
				+ principle + ", commission=" + commission + ", feeshare=" + feeshare + ", fxShare=" + fxShare
				+ ", afterBalance=" + afterBalance + ", ccy=" + ccy + ", type=" + type + ", mfsId=" + mfsId
				+ ", transactionId=" + transactionId + "]";
	}

	

}
