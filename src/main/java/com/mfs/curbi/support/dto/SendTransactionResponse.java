package com.mfs.curbi.support.dto;

public class SendTransactionResponse {

	private double settlementCurrency;
	private String partnerCountry;

	public String getPartnerCountry() {
		return partnerCountry;
	}

	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}

	private double partnerCurrency;

	private double avgTxnValue;
	private int txnValue;

	public double getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(double settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public double getPartnerCurrency() {
		return partnerCurrency;
	}

	public void setPartnerCurrency(double partnerCurrency) {
		this.partnerCurrency = partnerCurrency;
	}

	public double getAvgTxnValue() {
		return avgTxnValue;
	}

	public void setAvgTxnValue(double avgTxnValue) {
		this.avgTxnValue = avgTxnValue;
	}

	public int getTxnValue() {
		return txnValue;
	}

	public void setTxnValue(int txnValue) {
		this.txnValue = txnValue;
	}

	@Override
	public String toString() {
		return "SendTransactionResponse [settlementCurrency=" + settlementCurrency + ", partnerCountry="
				+ partnerCountry + ", partnerCurrency=" + partnerCurrency + ", avgTxnValue=" + avgTxnValue
				+ ", txnValue=" + txnValue + "]";
	}

}
