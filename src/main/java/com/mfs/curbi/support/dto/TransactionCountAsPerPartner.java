package com.mfs.curbi.support.dto;

public class TransactionCountAsPerPartner {

	private String partnerName;
	private double totalTransCount;
	private String transaDate;
	
	

	public String getTransaDate() {
		return transaDate;
	}

	public void setTransaDate(String transaDate) {
		this.transaDate = transaDate;
	}

	public double getTotalTransCount() {
		return totalTransCount;
	}

	public void setTotalTransCount(double totalTransCount) {
		this.totalTransCount = totalTransCount;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	@Override
	public String toString() {
		return "TransactionCountAsPerPartner [partnerName=" + partnerName + ", totalTransCount=" + totalTransCount
				+ ", transaDate=" + transaDate + "]";
	}

}
