package com.mfs.curbi.support.dto;

public class MdmSendPartnerResponseDto {

	private String partnerName;

	private String partnerCode;

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	@Override
	public String toString() {
		return "MdmSendPartnerResponseDto [partnerName=" + partnerName + ", partnerCode=" + partnerCode + "]";
	}

}
