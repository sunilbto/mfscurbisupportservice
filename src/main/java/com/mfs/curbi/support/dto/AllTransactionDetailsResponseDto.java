package com.mfs.curbi.support.dto;

public class AllTransactionDetailsResponseDto {

	private String valueDate;

	private String mfsId;

	private String thirdPartyTransactionId;

	private String status;

	private String product;

	private String direction;

	private int transactionId;
	
	private String destinationType;
	
	private String receiveCountry;

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getThirdPartyTransactionId() {
		return thirdPartyTransactionId;
	}

	public void setThirdPartyTransactionId(String thirdPartyTransactionId) {
		this.thirdPartyTransactionId = thirdPartyTransactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "AllTransactionDetailsResponseDto [valueDate=" + valueDate + ", mfsId=" + mfsId
				+ ", thirdPartyTransactionId=" + thirdPartyTransactionId + ", status=" + status + ", product=" + product
				+ ", direction=" + direction + ", transactionId=" + transactionId + ", destinationType="
				+ destinationType + ", receiveCountry=" + receiveCountry + "]";
	}



}
