package com.mfs.curbi.support.dto;

public class RecieveCountry {

	private double amount;
	private double spcAmount;

	private double totalAmount;
	private int txnCount;
	private String partnerCountry;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getSpcAmount() {
		return spcAmount;
	}

	public void setSpcAmount(double spcAmount) {
		this.spcAmount = spcAmount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getTxnCount() {
		return txnCount;
	}

	public void setTxnCount(int txnCount) {
		this.txnCount = txnCount;
	}

	public String getPartnerCountry() {
		return partnerCountry;
	}

	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}

	@Override
	public String toString() {
		return "RecieveCountry [amount=" + amount + ", spcAmount=" + spcAmount + ", totalAmount=" + totalAmount
				+ ", txnCount=" + txnCount + ", partnerCountry=" + partnerCountry + "]";
	}
	

}
