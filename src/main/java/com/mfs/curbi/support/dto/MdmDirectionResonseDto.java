package com.mfs.curbi.support.dto;

public class MdmDirectionResonseDto {

	private int mdmDirctionId;

	private String directionName;

	private String description;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public int getMdmDirctionId() {
		return mdmDirctionId;
	}

	public void setMdmDirctionId(int mdmDirctionId) {
		this.mdmDirctionId = mdmDirctionId;
	}

	public String getDirectionName() {
		return directionName;
	}

	public void setDirectionName(String directionName) {
		this.directionName = directionName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "MdmDirectionResonseDto [mdmDirctionId=" + mdmDirctionId + ", directionName=" + directionName
				+ ", description=" + description + ", responseCode=" + responseCode + ", responseMessage="
				+ responseMessage + ", exception=" + exception + "]";
	}

}
