package com.mfs.curbi.support.dto;

public class MoneyFilterRequestDto {

	private String countryName;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public String toString() {
		return "MoneyFilterRequestDto [countryName=" + countryName + "]";
	}

}
