package com.mfs.curbi.support.dto;

public class ComplianceRequestDto {
	
	private String countryName;
	private String startDate;
	private String endDate;
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "ComplianceRequestDto [countryName=" + countryName + ", startDate=" + startDate + ", endDate=" + endDate
				+ "]";
	}
	

}
