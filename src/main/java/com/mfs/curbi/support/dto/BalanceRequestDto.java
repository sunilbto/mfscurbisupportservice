package com.mfs.curbi.support.dto;

public class BalanceRequestDto {

	private int transactionId;

	private String partnerId;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	@Override
	public String toString() {
		return "BalanceRequestDto [transactionId=" + transactionId + ", partnerId=" + partnerId + "]";
	}

}
