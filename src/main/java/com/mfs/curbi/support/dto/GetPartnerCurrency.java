package com.mfs.curbi.support.dto;

public class GetPartnerCurrency {
	
	private String partnerCurrency;

	public String getPartnerCurrency() {
		return partnerCurrency;
	}

	public void setPartnerCurrency(String partnerCurrency) {
		this.partnerCurrency = partnerCurrency;
	}

	@Override
	public String toString() {
		return "GetPartnerCurrency [partnerCurrency=" + partnerCurrency + "]";
	}
	
	
	

}
