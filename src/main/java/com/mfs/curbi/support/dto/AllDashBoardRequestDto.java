package com.mfs.curbi.support.dto;

public class AllDashBoardRequestDto {

	private long partnerCode;

	private String startDate;

	private String endDate;
	
	private String status;
	
	private String direction;
	
	private String receiveCountry;
	
	private String product;
	
	private String destinationType;

	

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	public long getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(long partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Override
	public String toString() {
		return "AllDashBoardRequestDto [partnerCode=" + partnerCode + ", startDate=" + startDate + ", endDate="
				+ endDate + ", status=" + status + ", direction=" + direction + ", receiveCountry=" + receiveCountry
				+ ", product=" + product + ", destinationType=" + destinationType + "]";
	}

	
	
	
	
}
