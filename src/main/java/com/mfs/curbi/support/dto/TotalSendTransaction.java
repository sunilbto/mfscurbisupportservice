package com.mfs.curbi.support.dto;

public class TotalSendTransaction {
	private String partnerName;
	private double settlementCurrency;
	private double partnerCurrency;
	
	private double avgTxnValue;
	private int txnValue;

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public double getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(double settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public double getPartnerCurrency() {
		return partnerCurrency;
	}

	public void setPartnerCurrency(double partnerCurrency) {
		this.partnerCurrency = partnerCurrency;
	}

	

	public double getAvgTxnValue() {
		return avgTxnValue;
	}

	public void setAvgTxnValue(double avgTxnValue) {
		this.avgTxnValue = avgTxnValue;
	}

	public int getTxnValue() {
		return txnValue;
	}

	public void setTxnValue(int txnValue) {
		this.txnValue = txnValue;
	}

	@Override
	public String toString() {
		return "TotalSendTransaction [partnerName=" + partnerName + ", settlementCurrency=" + settlementCurrency
				+ ", partnerCurrency=" + partnerCurrency + ", avgTxnValue=" + avgTxnValue + ", txnValue=" + txnValue
				+ "]";
	}

	

}
