package com.mfs.curbi.support.dto;

public class MdmDirectionResponseDto {
	
	private int mdmDirctionId;

	private String directionName;

	public int getMdmDirctionId() {
		return mdmDirctionId;
	}

	public void setMdmDirctionId(int mdmDirctionId) {
		this.mdmDirctionId = mdmDirctionId;
	}

	public String getDirectionName() {
		return directionName;
	}

	public void setDirectionName(String directionName) {
		this.directionName = directionName;
	}

	@Override
	public String toString() {
		return "MdmDirectionResponseDto [mdmDirctionId=" + mdmDirctionId + ", directionName=" + directionName + "]";
	}
	
	

}
