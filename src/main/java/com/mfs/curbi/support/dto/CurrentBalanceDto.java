package com.mfs.curbi.support.dto;

public class CurrentBalanceDto {

	private double afterBalance;

	private String partnerSettlCcy;

	

	public double getAfterBalance() {
		return afterBalance;
	}

	public void setAfterBalance(double afterBalance) {
		this.afterBalance = afterBalance;
	}

	public String getPartnerSettlCcy() {
		return partnerSettlCcy;
	}

	public void setPartnerSettlCcy(String partnerSettlCcy) {
		this.partnerSettlCcy = partnerSettlCcy;
	}

	@Override
	public String toString() {
		return "CurrentBalanceDto [afterBalance=" + afterBalance + ", partnerSettlCcy=" + partnerSettlCcy + "]";
	}

	
}
