package com.mfs.curbi.support.dto;

public class TotalAverageSendTo {

	private double totalAvgSendTo;

	public double getTotalAvgSendTo() {
		return totalAvgSendTo;
	}

	public void setTotalAvgSendTo(double totalAvgSendTo) {
		this.totalAvgSendTo = totalAvgSendTo;
	}

	@Override
	public String toString() {
		return "TotalAverageSendTo [totalAvgSendTo=" + totalAvgSendTo + "]";
	}
	
}
