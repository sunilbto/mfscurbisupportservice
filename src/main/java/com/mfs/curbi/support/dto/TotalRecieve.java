package com.mfs.curbi.support.dto;

public class TotalRecieve {
	private String partnerName;
	private double amount;
	private double spcAmount;
	private String marketShare;
	private double totalAmount;
	private int txnCount;
	private String partnerCountry;

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getSpcAmount() {
		return spcAmount;
	}

	public void setSpcAmount(double spcAmount) {
		this.spcAmount = spcAmount;
	}

	public String getMarketShare() {
		return marketShare;
	}

	public void setMarketShare(String marketShare) {
		this.marketShare = marketShare;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getTxnCount() {
		return txnCount;
	}

	public void setTxnCount(int txnCount) {
		this.txnCount = txnCount;
	}

	public String getPartnerCountry() {
		return partnerCountry;
	}

	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}

	@Override
	public String toString() {
		return "RecieveCountry [partnerName=" + partnerName + ", amount=" + amount + ", spcAmount=" + spcAmount
				+ ", marketShare=" + marketShare + ", totalAmount=" + totalAmount + ", txnCount=" + txnCount
				+ ", partnerCountry=" + partnerCountry + "]";
	}

}
