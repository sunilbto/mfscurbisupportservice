package com.mfs.curbi.support.dto;

import java.util.List;

public class MoneyFlowResponseDto {
	private int totalTransactionCount;
	private double totalTransactionAmount;
	private List<SendTransactionResponse> sendTransactionResponse;
	private List<RecieveTransactionResponse> recieveTransactionResponse;

	private List<TransAmountAsPerPartner> transAmountAsPerPartner;
	private String message;
	private String errorCode;
	private Exception exception;
	private String currecny;
	private List<TotalSendTransaction> totalSendTransactionList;
	private List<TotalRecieveTransaction> totalRecieveTransactionList;

	public List<TotalRecieveTransaction> getTotalRecieveTransactionList() {
		return totalRecieveTransactionList;
	}

	public void setTotalRecieveTransactionList(List<TotalRecieveTransaction> totalRecieveTransactionList) {
		this.totalRecieveTransactionList = totalRecieveTransactionList;
	}

	public List<TotalSendTransaction> getTotalSendTransactionList() {
		return totalSendTransactionList;
	}

	public void setTotalSendTransactionList(List<TotalSendTransaction> totalSendTransactionList) {
		this.totalSendTransactionList = totalSendTransactionList;
	}

	public String getCurrecny() {
		return currecny;
	}

	public void setCurrecny(String currecny) {
		this.currecny = currecny;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public List<TransAmountAsPerPartner> getTransAmountAsPerPartner() {
		return transAmountAsPerPartner;
	}

	public void setTransAmountAsPerPartner(List<TransAmountAsPerPartner> transAmountAsPerPartner) {
		this.transAmountAsPerPartner = transAmountAsPerPartner;
	}

	public int getTotalTransactionCount() {
		return totalTransactionCount;
	}

	public void setTotalTransactionCount(int totalTransactionCount) {
		this.totalTransactionCount = totalTransactionCount;
	}

	public double getTotalTransactionAmount() {
		return totalTransactionAmount;
	}

	public void setTotalTransactionAmount(double totalTransactionAmount) {
		this.totalTransactionAmount = totalTransactionAmount;
	}

	public List<SendTransactionResponse> getSendTransactionResponse() {
		return sendTransactionResponse;
	}

	public void setSendTransactionResponse(List<SendTransactionResponse> sendTransactionResponse) {
		this.sendTransactionResponse = sendTransactionResponse;
	}

	public List<RecieveTransactionResponse> getRecieveTransactionResponse() {
		return recieveTransactionResponse;
	}

	public void setRecieveTransactionResponse(List<RecieveTransactionResponse> recieveTransactionResponse) {
		this.recieveTransactionResponse = recieveTransactionResponse;
	}

	@Override
	public String toString() {
		return "MoneyFlowResponseDto [totalTransactionCount=" + totalTransactionCount + ", totalTransactionAmount="
				+ totalTransactionAmount + ", sendTransactionResponse=" + sendTransactionResponse
				+ ", recieveTransactionResponse=" + recieveTransactionResponse + ", transAmountAsPerPartner="
				+ transAmountAsPerPartner + ", message=" + message + ", errorCode=" + errorCode + ", exception="
				+ exception + ", currecny=" + currecny + ", totalSendTransactionList=" + totalSendTransactionList
				+ ", totalRecieveTransactionList=" + totalRecieveTransactionList + "]";
	}

}
