package com.mfs.curbi.support.dto;

public class AllBalanceRequestDto {

	private long partnerCode;
	
	private String startDate;
	
	private String endDate;
	
    private String status;
	
	private String product;
	
	private String direction;
	
	private String destinationType;
	
	private String receiveCountry;

    private String flag;
    


	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public long getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(long partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "AllBalanceRequestDto [partnerCode=" + partnerCode + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", status=" + status + ", product=" + product + ", direction=" + direction + ", destinationType="
				+ destinationType + ", receiveCountry=" + receiveCountry + ", flag=" + flag + "]";
	}

	
	
	
	

	

}
