package com.mfs.curbi.support.dto;

public class TransactionStatusRsponseDto {

	private String transactionStatus;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "TransactionStatusRsponseDto [transactionStatus=" + transactionStatus + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

}
