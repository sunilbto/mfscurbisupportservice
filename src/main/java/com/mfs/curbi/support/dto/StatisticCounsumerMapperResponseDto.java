package com.mfs.curbi.support.dto;

import java.sql.Timestamp;

public class StatisticCounsumerMapperResponseDto {
	
	private Timestamp transactionProcessedDate;
	
	private double sendCount;
	
	private double receiveCount;

	public Timestamp getTransactionProcessedDate() {
		return transactionProcessedDate;
	}

	public void setTransactionProcessedDate(Timestamp transactionProcessedDate) {
		this.transactionProcessedDate = transactionProcessedDate;
	}

	public double getSendCount() {
		return sendCount;
	}

	public void setSendCount(double sendCount) {
		this.sendCount = sendCount;
	}

	public double getReceiveCount() {
		return receiveCount;
	}

	public void setReceiveCount(double receiveCount) {
		this.receiveCount = receiveCount;
	}

	@Override
	public String toString() {
		return "StatisticCounsumerMapperResponseDto [transactionProcessedDate=" + transactionProcessedDate
				+ ", sendCount=" + sendCount + ", receiveCount=" + receiveCount + "]";
	}

	
	
	
	

}
