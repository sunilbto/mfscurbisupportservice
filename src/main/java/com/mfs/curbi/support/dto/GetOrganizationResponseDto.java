package com.mfs.curbi.support.dto;

public class GetOrganizationResponseDto {

	private int orgId;

	private String organizatioName;

	private String partnerCode;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public int getOrgId() {
		return orgId;
	}

	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}

	public String getOrganizatioName() {
		return organizatioName;
	}

	public void setOrganizatioName(String organizatioName) {
		this.organizatioName = organizatioName;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Override
	public String toString() {
		return "GetOrganizationResponseDto [orgId=" + orgId + ", organizatioName=" + organizatioName + ", partnerCode="
				+ partnerCode + ", responseCode=" + responseCode + ", responseMessage=" + responseMessage
				+ ", exception=" + exception + "]";
	}

}
