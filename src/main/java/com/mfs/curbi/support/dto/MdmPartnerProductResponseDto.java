package com.mfs.curbi.support.dto;

public class MdmPartnerProductResponseDto {

	private int partnerProductId;

	private int partnerId;

	private int productId;

	private int pivotCaseId;

	private boolean isEnabled;

	public int getPartnerProductId() {
		return partnerProductId;
	}

	public void setPartnerProductId(int partnerProductId) {
		this.partnerProductId = partnerProductId;
	}

	public int getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getPivotCaseId() {
		return pivotCaseId;
	}

	public void setPivotCaseId(int pivotCaseId) {
		this.pivotCaseId = pivotCaseId;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	@Override
	public String toString() {
		return "MdmPartnerProductResponseDto [partnerProductId=" + partnerProductId + ", partnerId=" + partnerId
				+ ", productId=" + productId + ", pivotCaseId=" + pivotCaseId + ", isEnabled=" + isEnabled + "]";
	}

}
