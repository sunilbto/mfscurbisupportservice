package com.mfs.curbi.support.dto;

public class TotalRecieveTransaction {
	private String partnerName;
	private double settlementCurrency;
	private double partnerCurrency;
	private double avgTxnValue;
	private int txnValue;
	private String country;

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public double getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(double settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public double getPartnerCurrency() {
		return partnerCurrency;
	}

	public void setPartnerCurrency(double partnerCurrency) {
		this.partnerCurrency = partnerCurrency;
	}

	public double getAvgTxnValue() {
		return avgTxnValue;
	}

	public void setAvgTxnValue(double avgTxnValue) {
		this.avgTxnValue = avgTxnValue;
	}

	public int getTxnValue() {
		return txnValue;
	}

	public void setTxnValue(int txnValue) {
		this.txnValue = txnValue;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "TotalRecieveTransaction [partnerName=" + partnerName + ", settlementCurrency=" + settlementCurrency
				+ ", partnerCurrency=" + partnerCurrency + ", avgTxnValue=" + avgTxnValue + ", txnValue=" + txnValue
				+ ", country=" + country + "]";
	}

}
