package com.mfs.curbi.support.dto;

public class CurrentBalanceResponseDto {

	private double currentBalance;

	private String currency;

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "CurrentBalanceResponseDto [currentBalance=" + currentBalance + ", currency=" + currency + "]";
	}

}
