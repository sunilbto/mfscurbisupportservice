package com.mfs.curbi.support.dto;

import java.util.List;

public class TransactionDetailResponseDto {

	private List<TransactionDetailByIdResponseDto> data;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public List<TransactionDetailByIdResponseDto> getData() {
		return data;
	}

	public void setData(List<TransactionDetailByIdResponseDto> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "TransactionDetailResponseDto [data=" + data + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

}
