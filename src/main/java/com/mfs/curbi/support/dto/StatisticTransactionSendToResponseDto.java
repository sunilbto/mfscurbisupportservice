package com.mfs.curbi.support.dto;

public class StatisticTransactionSendToResponseDto {

	
	private String processedDate;
	
	private double avgAmount;

	private String date;
	
	
	public String getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}

	public double getAvgAmount() {
		return avgAmount;
	}

	public void setAvgAmount(double avgAmount) {
		this.avgAmount = avgAmount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "StatisticTransactionSendToResponseDto [processedDate=" + processedDate + ", avgAmount=" + avgAmount
				+ ", date=" + date + "]";
	}
	
	
	
	

}
