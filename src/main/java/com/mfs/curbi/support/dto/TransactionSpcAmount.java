package com.mfs.curbi.support.dto;

public class TransactionSpcAmount {

	private double spcAmount;

	public double getSpcAmount() {
		return spcAmount;
	}

	public void setSpcAmount(double spcAmount) {
		this.spcAmount = spcAmount;
	}

	@Override
	public String toString() {
		return "TransactionSpcAmount [spcAmount=" + spcAmount + "]";
	}

}
