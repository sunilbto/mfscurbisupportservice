package com.mfs.curbi.support.dto;

import java.sql.Timestamp;

public class MdmTransactionDetailsResponseDTO {

	private int mdmTansactionId;

	private Timestamp transactionCreatedDate;

	private Timestamp transactionProcessedDate;

	private String transactionStatus;

	private String transactionRef;

	private String transMfsId;

	private String spTransactionId;

	private String rpTransactionId;

	private String statusCode;

	private String statusMessage;

	public int getMdmTansactionId() {
		return mdmTansactionId;
	}

	public void setMdmTansactionId(int mdmTansactionId) {
		this.mdmTansactionId = mdmTansactionId;
	}

	public Timestamp getTransactionCreatedDate() {
		return transactionCreatedDate;
	}

	public void setTransactionCreatedDate(Timestamp transactionCreatedDate) {
		this.transactionCreatedDate = transactionCreatedDate;
	}

	public Timestamp getTransactionProcessedDate() {
		return transactionProcessedDate;
	}

	public void setTransactionProcessedDate(Timestamp transactionProcessedDate) {
		this.transactionProcessedDate = transactionProcessedDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getTransactionRef() {
		return transactionRef;
	}

	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}

	public String getTransMfsId() {
		return transMfsId;
	}

	public void setTransMfsId(String transMfsId) {
		this.transMfsId = transMfsId;
	}

	public String getSpTransactionId() {
		return spTransactionId;
	}

	public void setSpTransactionId(String spTransactionId) {
		this.spTransactionId = spTransactionId;
	}

	public String getRpTransactionId() {
		return rpTransactionId;
	}

	public void setRpTransactionId(String rpTransactionId) {
		this.rpTransactionId = rpTransactionId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	@Override
	public String toString() {
		return "MdmTransactionDetailsResponseDTO [mdmTansactionId=" + mdmTansactionId + ", transactionCreatedDate="
				+ transactionCreatedDate + ", transactionProcessedDate=" + transactionProcessedDate
				+ ", transactionStatus=" + transactionStatus + ", transactionRef=" + transactionRef + ", transMfsId="
				+ transMfsId + ", spTransactionId=" + spTransactionId + ", rpTransactionId=" + rpTransactionId
				+ ", statusCode=" + statusCode + ", statusMessage=" + statusMessage + "]";
	}

}
