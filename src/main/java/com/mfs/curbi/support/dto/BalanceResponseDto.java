package com.mfs.curbi.support.dto;

import java.util.List;

public class BalanceResponseDto {

	List<AllBalanceResponseDto> balanceDetail;

	private double currentBalance;

	private String currency;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public List<AllBalanceResponseDto> getBalanceDetail() {
		return balanceDetail;
	}

	public void setBalanceDetail(List<AllBalanceResponseDto> balanceDetail) {
		this.balanceDetail = balanceDetail;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "BalanceResponseDto [balanceDetail=" + balanceDetail + ", currentBalance=" + currentBalance
				+ ", currency=" + currency + ", responseCode=" + responseCode + ", responseMessage=" + responseMessage
				+ ", exception=" + exception + "]";
	}


	
}
