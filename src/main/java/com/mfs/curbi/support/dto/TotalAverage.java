package com.mfs.curbi.support.dto;

public class TotalAverage {
	
	private double  totalAvgSendFrom;

	public double getTotalAvgSendFrom() {
		return totalAvgSendFrom;
	}

	public void setTotalAvgSendFrom(double totalAvgSendFrom) {
		this.totalAvgSendFrom = totalAvgSendFrom;
	}

	@Override
	public String toString() {
		return "TotalAverage [totalAvgSendFrom=" + totalAvgSendFrom + "]";
	}

	

}
