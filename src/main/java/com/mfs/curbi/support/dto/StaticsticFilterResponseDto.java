package com.mfs.curbi.support.dto;

import java.util.List;

public class StaticsticFilterResponseDto {
	
	private List<MdmProductResponseDto> productList;
	
	private List<MdmDestinationTypeResponseDto> destinationTypeList;

	public List<MdmProductResponseDto> getProductList() {
		return productList;
	}

	public void setProductList(List<MdmProductResponseDto> productList) {
		this.productList = productList;
	}

	public List<MdmDestinationTypeResponseDto> getDestinationTypeList() {
		return destinationTypeList;
	}

	public void setDestinationTypeList(List<MdmDestinationTypeResponseDto> destinationTypeList) {
		this.destinationTypeList = destinationTypeList;
	}

	@Override
	public String toString() {
		return "StaticsticFilterResponseDto [productList=" + productList + ", destinationTypeList="
				+ destinationTypeList + "]";
	}
	
	

}
