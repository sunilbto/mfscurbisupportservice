package com.mfs.curbi.support.dto;

import java.util.List;

public class StatisticCounsumeResponseDto {
 
	List<StatisticCounsumerMapperResponseDto> statisticConsumeResponse;
	
	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public List<StatisticCounsumerMapperResponseDto> getStatisticConsumeResponse() {
		return statisticConsumeResponse;
	}

	public void setStatisticConsumeResponse(List<StatisticCounsumerMapperResponseDto> statisticConsumeResponse) {
		this.statisticConsumeResponse = statisticConsumeResponse;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "StatisticCounsumeResponseDto [statisticConsumeResponse=" + statisticConsumeResponse + ", responseCode="
				+ responseCode + ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

	
	
	
	
	
	
}
