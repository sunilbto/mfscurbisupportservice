package com.mfs.curbi.support.dto;

public class AllBalanceByIdRequestDto {

	private int transactionId;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "AllBalanceRequestDto [transactionId=" + transactionId + "]";
	}

}
