package com.mfs.curbi.support.dto;

import java.util.List;

public class ComplianceFinalResponseDto {
	
	public List<ComplianceResponseDto> sendingFromCountryRespone;

	public List<ComplianceResponseDto>  sendingToCountryRespone;

	public List<ComplianceResponseDto> receivingFromCountryResponse;
	
	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public List<ComplianceResponseDto> getSendingFromCountryRespone() {
		return sendingFromCountryRespone;
	}

	public void setSendingFromCountryRespone(List<ComplianceResponseDto> sendingFromCountryRespone) {
		this.sendingFromCountryRespone = sendingFromCountryRespone;
	}

	public List<ComplianceResponseDto> getSendingToCountryRespone() {
		return sendingToCountryRespone;
	}

	public void setSendingToCountryRespone(List<ComplianceResponseDto> sendingToCountryRespone) {
		this.sendingToCountryRespone = sendingToCountryRespone;
	}

	public List<ComplianceResponseDto> getReceivingFromCountryResponse() {
		return receivingFromCountryResponse;
	}

	public void setReceivingFromCountryResponse(List<ComplianceResponseDto> receivingFromCountryResponse) {
		this.receivingFromCountryResponse = receivingFromCountryResponse;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "ComplianceFinalResponseDto [sendingFromCountryRespone=" + sendingFromCountryRespone
				+ ", sendingToCountryRespone=" + sendingToCountryRespone + ", receivingFromCountryResponse="
				+ receivingFromCountryResponse + ", responseCode=" + responseCode + ", responseMessage="
				+ responseMessage + ", exception=" + exception + "]";
	}

	

    
	
	
	

}
