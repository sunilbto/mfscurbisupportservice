package com.mfs.curbi.support.dto;

public class MdmCountryResponseDto {

	private int mdmCountryId;

	private String countryName;

	private String countryCode;

	private String numericCode;

	private int phoneCode;

	private String responseCode;

	private String responseMessage;

	private Exception exception;

	public int getMdmCountryId() {
		return mdmCountryId;
	}

	public void setMdmCountryId(int mdmCountryId) {
		this.mdmCountryId = mdmCountryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getNumericCode() {
		return numericCode;
	}

	public void setNumericCode(String numericCode) {
		this.numericCode = numericCode;
	}

	public int getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(int phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "MdmCountryResponseDto [mdmCountryId=" + mdmCountryId + ", countryName=" + countryName + ", countryCode="
				+ countryCode + ", numericCode=" + numericCode + ", phoneCode=" + phoneCode + ", responseCode="
				+ responseCode + ", responseMessage=" + responseMessage + ", exception=" + exception + "]";
	}

}
