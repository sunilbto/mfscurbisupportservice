package com.mfs.curbi.support.dto;

import java.util.Date;

public class DashResponseDto {

	private String partnerId;

	private Date processedDate;

	private long count;

	private double finalAmount;

	private double commission;

	private double feeRevShare;

	private double forexRevShare;
	
	
	
	public double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getFeeRevShare() {
		return feeRevShare;
	}

	public void setFeeRevShare(double feeRevShare) {
		this.feeRevShare = feeRevShare;
	}

	public double getForexRevShare() {
		return forexRevShare;
	}

	public void setForexRevShare(double forexRevShare) {
		this.forexRevShare = forexRevShare;
	}

	@Override
	public String toString() {
		return "DashResponseDto [partnerId=" + partnerId + ", processedDate=" + processedDate + ", count=" + count
				+ ", finalAmount=" + finalAmount + ", commission=" + commission + ", feeRevShare=" + feeRevShare
				+ ", forexRevShare=" + forexRevShare + "]";
	}

	

	

	

	

}
