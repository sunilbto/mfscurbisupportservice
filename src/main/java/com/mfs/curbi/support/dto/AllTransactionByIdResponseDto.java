package com.mfs.curbi.support.dto;

import java.util.Date;

public class AllTransactionByIdResponseDto {

	private double fxRate;

	private int productId;

	private String productName;

	private String destinationType;

	private Date creationDate;

	private Date processedDate;

	private String transactionStatus;

	private String mfsId;

	private String partnerDirection;

	private String sendPartner;

	private String receiverPartner;

	private String sendCurrency;

	private String sendCountry;

	private double sendAmount;

	private double sendFee;

	private String receiveCurrency;

	private String receiveCountry;

	private double receiveAmount;

	private double receiveFee;

	private String partnerDirectionR;

	private Date valueDate;

	private double BalanceBefore;

	private double principal;

	private double commission;

	private double feeRevShare;

	private double ForexRevShare;

	private double BalanceAfter;

	private String MovementType;

	private String settlmentCurrency;

	private int tdid;

	private int tpsdid;

	private int tprdid;

	private int tnlid;

	private int sPartner_id;

	private int rPartner_id;

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getSendPartner() {
		return sendPartner;
	}

	public void setSendPartner(String sendPartner) {
		this.sendPartner = sendPartner;
	}

	public String getReceiverPartner() {
		return receiverPartner;
	}

	public void setReceiverPartner(String receiverPartner) {
		this.receiverPartner = receiverPartner;
	}

	public String getSendCurrency() {
		return sendCurrency;
	}

	public void setSendCurrency(String sendCurrency) {
		this.sendCurrency = sendCurrency;
	}

	public String getSendCountry() {
		return sendCountry;
	}

	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}

	public double getReceiveFee() {
		return receiveFee;
	}

	public void setReceiveFee(double receiveFee) {
		this.receiveFee = receiveFee;
	}

	public String getPartnerDirectionR() {
		return partnerDirectionR;
	}

	public void setPartnerDirectionR(String partnerDirectionR) {
		this.partnerDirectionR = partnerDirectionR;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public double getBalanceBefore() {
		return BalanceBefore;
	}

	public void setBalanceBefore(double balanceBefore) {
		BalanceBefore = balanceBefore;
	}

	public double getPrincipal() {
		return principal;
	}

	public void setPrincipal(double principal) {
		this.principal = principal;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getFeeRevShare() {
		return feeRevShare;
	}

	public void setFeeRevShare(double feeRevShare) {
		this.feeRevShare = feeRevShare;
	}

	public double getForexRevShare() {
		return ForexRevShare;
	}

	public void setForexRevShare(double forexRevShare) {
		ForexRevShare = forexRevShare;
	}

	public double getBalanceAfter() {
		return BalanceAfter;
	}

	public void setBalanceAfter(double balanceAfter) {
		BalanceAfter = balanceAfter;
	}

	public String getMovementType() {
		return MovementType;
	}

	public void setMovementType(String movementType) {
		MovementType = movementType;
	}

	public String getSettlmentCurrency() {
		return settlmentCurrency;
	}

	public void setSettlmentCurrency(String settlmentCurrency) {
		this.settlmentCurrency = settlmentCurrency;
	}

	public int getTdid() {
		return tdid;
	}

	public void setTdid(int tdid) {
		this.tdid = tdid;
	}

	public int getTpsdid() {
		return tpsdid;
	}

	public void setTpsdid(int tpsdid) {
		this.tpsdid = tpsdid;
	}

	public int getTprdid() {
		return tprdid;
	}

	public void setTprdid(int tprdid) {
		this.tprdid = tprdid;
	}

	public int getTnlid() {
		return tnlid;
	}

	public void setTnlid(int tnlid) {
		this.tnlid = tnlid;
	}

	public int getsPartner_id() {
		return sPartner_id;
	}

	public void setsPartner_id(int sPartner_id) {
		this.sPartner_id = sPartner_id;
	}

	public int getrPartner_id() {
		return rPartner_id;
	}

	public void setrPartner_id(int rPartner_id) {
		this.rPartner_id = rPartner_id;
	}

	public double getSendAmount() {
		return sendAmount;
	}

	public void setSendAmount(double sendAmount) {
		this.sendAmount = sendAmount;
	}

	public double getSendFee() {
		return sendFee;
	}

	public void setSendFee(double sendFee) {
		this.sendFee = sendFee;
	}

	public String getPartnerDirection() {
		return partnerDirection;
	}

	public void setPartnerDirection(String partnerDirection) {
		this.partnerDirection = partnerDirection;
	}

	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public double getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	public double getFxRate() {
		return fxRate;
	}

	public void setFxRate(double fxRate) {
		this.fxRate = fxRate;
	}

	@Override
	public String toString() {
		return "AllTransactionByIdResponseDto [fxRate=" + fxRate + ", productId=" + productId + ", productName="
				+ productName + ", destinationType=" + destinationType + ", creationDate=" + creationDate
				+ ", processedDate=" + processedDate + ", transactionStatus=" + transactionStatus + ", mfsId=" + mfsId
				+ ", partnerDirection=" + partnerDirection + ", sendPartner=" + sendPartner + ", receiverPartner="
				+ receiverPartner + ", sendCurrency=" + sendCurrency + ", sendCountry=" + sendCountry + ", sendAmount="
				+ sendAmount + ", sendFee=" + sendFee + ", receiveCurrency=" + receiveCurrency + ", receiveCountry="
				+ receiveCountry + ", receiveAmount=" + receiveAmount + ", receiveFee=" + receiveFee
				+ ", partnerDirectionR=" + partnerDirectionR + ", valueDate=" + valueDate + ", BalanceBefore="
				+ BalanceBefore + ", principal=" + principal + ", commission=" + commission + ", feeRevShare="
				+ feeRevShare + ", ForexRevShare=" + ForexRevShare + ", BalanceAfter=" + BalanceAfter
				+ ", MovementType=" + MovementType + ", settlmentCurrency=" + settlmentCurrency + ", tdid=" + tdid
				+ ", tpsdid=" + tpsdid + ", tprdid=" + tprdid + ", tnlid=" + tnlid + ", sPartner_id=" + sPartner_id
				+ ", rPartner_id=" + rPartner_id + "]";
	}

}
