package com.mfs.curbi.support.dto;

public class SendingToCountryResponseDto {
	private String countryName;
	private String partnerName;
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	@Override
	public String toString() {
		return "SendingFromCountryDto [countryName=" + countryName + ", partnerName=" + partnerName + "]";
	}

}
