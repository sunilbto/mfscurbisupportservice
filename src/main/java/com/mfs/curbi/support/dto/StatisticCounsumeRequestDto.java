package com.mfs.curbi.support.dto;

public class StatisticCounsumeRequestDto {
	
	private String countryName;
	
	private String startDate;
	
	private String endDate;
	
	private String product;
	
	private String destinationType;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	@Override
	public String toString() {
		return "StatisticRequestDto [countryName=" + countryName + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", product=" + product + ", destinationType=" + destinationType + "]";
	}

	
	
	

}
