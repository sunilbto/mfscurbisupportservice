package com.mfs.curbi.support.dto;

public class MdmDestinationTypeResponseDto {
	
	private String destinationType;

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	@Override
	public String toString() {
		return "MdmDestinationTypeResponseDto [destinationType=" + destinationType + "]";
	}
	
	

}
