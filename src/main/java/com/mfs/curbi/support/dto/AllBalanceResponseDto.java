package com.mfs.curbi.support.dto;

import java.util.Date;

public class AllBalanceResponseDto {

	private int transactionDetailId;

	private String mfsId;

	private String status;

	private Date valueDate;

	private double beforeBalance;

	private double principle;

	private double commission;

	private double feeShare;

	private double fxShare;

	private double afterBalance;

	private String type;

	private String ccy;
	
	private String partnerDirection;
	
	private String partnerCountry;
	
	private String destinationType;
	
	private String productName;

	public int getTransactionDetailId() {
		return transactionDetailId;
	}

	public void setTransactionDetailId(int transactionDetailId) {
		this.transactionDetailId = transactionDetailId;
	}

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public double getBeforeBalance() {
		return beforeBalance;
	}

	public void setBeforeBalance(double beforeBalance) {
		this.beforeBalance = beforeBalance;
	}

	public double getPrinciple() {
		return principle;
	}

	public void setPrinciple(double principle) {
		this.principle = principle;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getFeeShare() {
		return feeShare;
	}

	public void setFeeShare(double feeShare) {
		this.feeShare = feeShare;
	}

	public double getFxShare() {
		return fxShare;
	}

	public void setFxShare(double fxShare) {
		this.fxShare = fxShare;
	}

	public double getAfterBalance() {
		return afterBalance;
	}

	public void setAfterBalance(double afterBalance) {
		this.afterBalance = afterBalance;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getPartnerDirection() {
		return partnerDirection;
	}

	public void setPartnerDirection(String partnerDirection) {
		this.partnerDirection = partnerDirection;
	}

	public String getPartnerCountry() {
		return partnerCountry;
	}

	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}

	public String getDestinationType() {
		return destinationType;
	}

	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Override
	public String toString() {
		return "AllBalanceResponseDto [transactionDetailId=" + transactionDetailId + ", mfsId=" + mfsId + ", status="
				+ status + ", valueDate=" + valueDate + ", beforeBalance=" + beforeBalance + ", principle=" + principle
				+ ", commission=" + commission + ", feeShare=" + feeShare + ", fxShare=" + fxShare + ", afterBalance="
				+ afterBalance + ", type=" + type + ", ccy=" + ccy + ", partnerDirection=" + partnerDirection
				+ ", partnerCountry=" + partnerCountry + ", destinationType=" + destinationType + ", productName="
				+ productName + "]";
	}


	
	

	
	

	
}
