package com.mfs.curbi.support.dto;

public class TransactionCountDto {
	private int totalTransactionCount;

	public int getTotalTransactionCount() {
		return totalTransactionCount;
	}

	public void setTotalTransactionCount(int totalTransactionCount) {
		this.totalTransactionCount = totalTransactionCount;
	}

	@Override
	public String toString() {
		return "TransactionCountDto [totalTransactionCount=" + totalTransactionCount + "]";
	}

}
