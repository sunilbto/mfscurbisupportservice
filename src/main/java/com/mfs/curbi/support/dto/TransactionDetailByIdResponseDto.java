package com.mfs.curbi.support.dto;

public class TransactionDetailByIdResponseDto {

	private String mfsId;

	private int thirdPartyTransactionId;

	private String status;

	private String creationDate;

	private String proccessDate;

	private String valueDate;

	private String productName;

	private String direction;

	private String destinationAccountType;

	private String receiveCountry;

	private String receivePartner;

	private double sendAmount;

	private double sendFee;

	private String sendCurrency;

	private double receiveAmount;

	private String receiveCurrency;

	private double fxRate;

	private double receiveFee;

	private double balanceBefore;

	private double principal;

	private double commission;

	private double feeRevShare;

	private double forexRevShare;

	private double balanceAfter;

	private String settlementCurrency;

	public String getMfsId() {
		return mfsId;
	}

	public void setMfsId(String mfsId) {
		this.mfsId = mfsId;
	}

	public int getThirdPartyTransactionId() {
		return thirdPartyTransactionId;
	}

	public void setThirdPartyTransactionId(int thirdPartyTransactionId) {
		this.thirdPartyTransactionId = thirdPartyTransactionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDestinationAccountType() {
		return destinationAccountType;
	}

	public void setDestinationAccountType(String destinationAccountType) {
		this.destinationAccountType = destinationAccountType;
	}

	public String getReceiveCountry() {
		return receiveCountry;
	}

	public void setReceiveCountry(String receiveCountry) {
		this.receiveCountry = receiveCountry;
	}

	public String getReceivePartner() {
		return receivePartner;
	}

	public void setReceivePartner(String receivePartner) {
		this.receivePartner = receivePartner;
	}

	public double getSendAmount() {
		return sendAmount;
	}

	public void setSendAmount(double sendAmount) {
		this.sendAmount = sendAmount;
	}

	public double getSendFee() {
		return sendFee;
	}

	public void setSendFee(double sendFee) {
		this.sendFee = sendFee;
	}

	public String getSendCurrency() {
		return sendCurrency;
	}

	public void setSendCurrency(String sendCurrency) {
		this.sendCurrency = sendCurrency;
	}

	public double getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(double receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public double getFxRate() {
		return fxRate;
	}

	public void setFxRate(double fxRate) {
		this.fxRate = fxRate;
	}

	public double getReceiveFee() {
		return receiveFee;
	}

	public void setReceiveFee(double receiveFee) {
		this.receiveFee = receiveFee;
	}

	public double getBalanceBefore() {
		return balanceBefore;
	}

	public void setBalanceBefore(double balanceBefore) {
		this.balanceBefore = balanceBefore;
	}

	public double getPrincipal() {
		return principal;
	}

	public void setPrincipal(double principal) {
		this.principal = principal;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getFeeRevShare() {
		return feeRevShare;
	}

	public void setFeeRevShare(double feeRevShare) {
		this.feeRevShare = feeRevShare;
	}

	public double getForexRevShare() {
		return forexRevShare;
	}

	public void setForexRevShare(double forexRevShare) {
		this.forexRevShare = forexRevShare;
	}

	public double getBalanceAfter() {
		return balanceAfter;
	}

	public void setBalanceAfter(double balanceAfter) {
		this.balanceAfter = balanceAfter;
	}

	public String getSettlementCurrency() {
		return settlementCurrency;
	}

	public void setSettlementCurrency(String settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getProccessDate() {
		return proccessDate;
	}

	public void setProccessDate(String proccessDate) {
		this.proccessDate = proccessDate;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	@Override
	public String toString() {
		return "TransactiondetailByIdResponseDto [mfsId=" + mfsId + ", thirdPartyTransactionId="
				+ thirdPartyTransactionId + ", status=" + status + ", creationDate=" + creationDate + ", proccessDate="
				+ proccessDate + ", valueDate=" + valueDate + ", productName=" + productName + ", direction="
				+ direction + ", destinationAccountType=" + destinationAccountType + ", receiveCountry="
				+ receiveCountry + ", receivePartner=" + receivePartner + ", sendAmount=" + sendAmount + ", sendFee="
				+ sendFee + ", sendCurrency=" + sendCurrency + ", receiveAmount=" + receiveAmount + ", receiveCurrency="
				+ receiveCurrency + ", fxRate=" + fxRate + ", receiveFee=" + receiveFee + ", balanceBefore="
				+ balanceBefore + ", principal=" + principal + ", commission=" + commission + ", feeRevShare="
				+ feeRevShare + ", forexRevShare=" + forexRevShare + ", balanceAfter=" + balanceAfter
				+ ", settlementCurrency=" + settlementCurrency + "]";
	}

}
