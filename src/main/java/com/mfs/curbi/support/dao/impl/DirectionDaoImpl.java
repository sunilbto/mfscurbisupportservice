package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.exception.CurbiHibernateCommonException;
import com.mfs.curbi.support.dao.DirectionDao;
import com.mfs.curbi.support.dto.MdmDirectionResonseDto;
import com.mfs.curbi.support.mapper.DirectionDetailsMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("DirectionDaoImpl")
public class DirectionDaoImpl implements DirectionDao {

	@Autowired
	private DataSource dataSource;
	private static final Logger log = LoggerFactory.getLogger(DirectionDaoImpl.class);

	/*
	 * Method-getDirectionList method return all direction list
	 * using string builder and prepared statement
	 */
	public List<MdmDirectionResonseDto> getDirectionList() {
		List<MdmDirectionResonseDto> directionList = null;
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			StringBuilder directionQuery = new StringBuilder();
			directionQuery.append(QueryConstant.DIRECTION_QUERY);
			directionList = jdbcTemplate.query(directionQuery.toString(), new PreparedStatementSetter() {

				public void setValues(PreparedStatement arg0) throws SQLException {

				}
			}, new DirectionDetailsMapper());

		} catch (HibernateException e) {
			log.error("In Exception DirectionDaoImpl : " + e);
			throw new CurbiHibernateCommonException(e.getMessage());
		}
		return directionList;
	}

}
