package com.mfs.curbi.support.dao;

import java.sql.Timestamp;
import java.util.List;

import com.mfs.curbi.support.dto.AllBalanceResponseDto;
import com.mfs.curbi.support.dto.AllTransactionByIdResponseDto;
import com.mfs.curbi.support.dto.AllTransactionResponseDto;
import com.mfs.curbi.support.dto.CurrentBalanceResponseDto;

public interface TransactionDetailsDao {

	public List<AllTransactionResponseDto> getAllTransactionDao(long partnerCode,Timestamp startDate, Timestamp endDate,String status,String product,String direction,String destinationType,String receiveCountry) throws Exception;

	public List<AllBalanceResponseDto> getAllBalanceDao(long partnerCode,Timestamp startDate, Timestamp endDate,String status,String product,String direction,String destinationType,String receiveCountry) throws Exception;

	public List<AllTransactionByIdResponseDto> getTransactionDetailsById(int transactionDetailId) throws Exception;

	public CurrentBalanceResponseDto getCurrentBalanace(long partnerCode) throws Exception;

	List<AllTransactionByIdResponseDto> getTransactionPerDate(long partnerCode, Timestamp startDate);

}
