package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.TransactionDetailsDao;
import com.mfs.curbi.support.dto.AllBalanceResponseDto;
import com.mfs.curbi.support.dto.AllTransactionByIdResponseDto;
import com.mfs.curbi.support.dto.AllTransactionResponseDto;
import com.mfs.curbi.support.dto.CurrentBalanceDto;
import com.mfs.curbi.support.dto.CurrentBalanceResponseDto;
import com.mfs.curbi.support.mapper.AllBalanceMapper;
import com.mfs.curbi.support.mapper.AllTransactionByIdMapper;
import com.mfs.curbi.support.mapper.AllTransactionMapper;
import com.mfs.curbi.support.mapper.CurrentBalanceMapper;
import com.mfs.curbi.support.mapper.TransactionDetailDataMapper;
import com.mfs.curbi.support.util.Format;
import com.mfs.curbi.support.util.QueryConstant;

@Repository
public class TransactionDetailsDaoImpl implements TransactionDetailsDao {

	@Autowired
	DataSource dataSource;

	/*
	 * Method-getAllTransactionDao method return all TransactionId
	 * using string builder and prepared statement
	 */
	@SuppressWarnings("unused")
	public List<AllTransactionResponseDto> getAllTransactionDao(long partnerCode, Timestamp startDate,
			Timestamp endDate, String status, String product, String direction, String destinationType,
			String receiveCountry) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		/*
		 * This query returns all transaction exist against partner.
		 */
		StringBuilder queryAllTransaction = new StringBuilder();
		queryAllTransaction.append(QueryConstant.TRANSACTION_QUERY);
		queryAllTransaction.append(partnerCode);
		queryAllTransaction.append(QueryConstant.TRANSACTION_QUERY1);
		queryAllTransaction.append(startDate);
		queryAllTransaction.append(QueryConstant.COMMON_QUERY3);
		queryAllTransaction.append(endDate);
		queryAllTransaction.append("'");

		//check selected filter
		queryAllTransaction = commonQuery(queryAllTransaction, status, product, direction, destinationType,
				receiveCountry);

		queryAllTransaction.append(";");

		List<AllTransactionResponseDto> alltransaction = jdbcTemplate.query(queryAllTransaction.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new AllTransactionMapper());

		return alltransaction;
	}

	/*
	 * Method-getAllBalanceDao method return all Balance
	 * using string builder and prepared statement
	 */
	@SuppressWarnings("unused")
	public List<AllBalanceResponseDto> getAllBalanceDao(long partnerCode, Timestamp startDate, Timestamp endDate,
			String status, String product, String direction, String destinationType, String receiveCountry) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		/*
		 * This query return balanace record exist against partner.
		 */
		StringBuilder queryForBalance = new StringBuilder();
		queryForBalance.append(QueryConstant.TRANSACTION_QUERY2);
		queryForBalance.append(partnerCode);
		queryForBalance.append(QueryConstant.TRANSACTION_QUERY3);
		queryForBalance.append(startDate);
		queryForBalance.append(QueryConstant.COMMON_QUERY3);
		queryForBalance.append(endDate);
		queryForBalance.append("'");
		
		//check selected filter
		queryForBalance = commonQuery(queryForBalance, status, product, direction, destinationType, receiveCountry);

		queryForBalance.append(";");

		List<AllBalanceResponseDto> allBalance = jdbcTemplate.query(queryForBalance.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new AllBalanceMapper());

		return allBalance;
	}

	/*
	 * Method-getTransactionDetailsById method return transaction by Id
	 * using string builder and prepared statement
	 */
	public List<AllTransactionByIdResponseDto> getTransactionDetailsById(int transactionDetailId) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		/*
		 * This query return transaction details against particular transaction id.
		 */
		StringBuilder queryforGetTransById = new StringBuilder();
		queryforGetTransById.append(QueryConstant.TRANSACTION_QUERY4);
		queryforGetTransById.append(transactionDetailId);
		queryforGetTransById.append(QueryConstant.TRANSACTION_QUERY5);

		List<AllTransactionByIdResponseDto> allTransactionById = jdbcTemplate.query(queryforGetTransById.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new AllTransactionByIdMapper());

		return allTransactionById;
	}

	/*
	 * Method-getCurrentBalanace method return current Balance
	 * using string builder and prepared statement
	 */
	public CurrentBalanceResponseDto getCurrentBalanace(long partnerCode) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		CurrentBalanceResponseDto response = null;
		/*
		 * This query return current balance of that partner.
		 */
		StringBuilder getCurrentbalance = new StringBuilder();
		getCurrentbalance.append(QueryConstant.TRANSACTION_QUERY6);
		getCurrentbalance.append(partnerCode);
		getCurrentbalance.append(QueryConstant.TRANSACTION_QUERY7);
		List<CurrentBalanceDto> currentBalanceList = jdbcTemplate.query(getCurrentbalance.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new CurrentBalanceMapper());

		if (!(currentBalanceList.isEmpty())) {
			response = new CurrentBalanceResponseDto();
			for (CurrentBalanceDto currentBalances : currentBalanceList) {

				response.setCurrency(currentBalances.getPartnerSettlCcy());
				response.setCurrentBalance(currentBalances.getAfterBalance());

			}
		}

		return response;

	}

	/*
	 * Method-getTransactionPerDate method return transaction per date
	 * using string builder and prepared statement
	 */
	@SuppressWarnings("unused")
	public List<AllTransactionByIdResponseDto> getTransactionPerDate(long partnerCode, Timestamp startDate) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		StringBuilder transactionPerDate = new StringBuilder();
		transactionPerDate.append(QueryConstant.TRANSACTION_QUERY8);
		transactionPerDate.append(partnerCode);
		transactionPerDate.append(QueryConstant.TRANSACTION_QUERY9);
		transactionPerDate.append(partnerCode);
		transactionPerDate.append(QueryConstant.TRANSACTION_QUERY10);
		transactionPerDate.append(startDate);
		transactionPerDate.append("';");

		List<AllTransactionByIdResponseDto> alltransaction = jdbcTemplate.query(transactionPerDate.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new TransactionDetailDataMapper());

		return alltransaction;
	}

	//check selected filter
	public StringBuilder commonQuery(StringBuilder query, String status, String product, String direction,
			String destinationType, String receiveCountry) {
		if (Format.isStringNotEmptyAndNotNull(status)) {
			query.append(QueryConstant.COMMON_QUERY17);
			query.append(status);
			query.append("'");

		}
		if (Format.isStringNotEmptyAndNotNull(product)) {
			query.append(QueryConstant.COMMON_QUERY18);
			query.append(product);
			query.append("'");

		}
		if (Format.isStringNotEmptyAndNotNull(direction)) {
			query.append(QueryConstant.COMMON_QUERY20);
			query.append(direction);
			query.append("'");

		}
		if (Format.isStringNotEmptyAndNotNull(destinationType)) {
			query.append(QueryConstant.COMMON_QUERY19);
			query.append(destinationType);
			query.append("'");

		}
		if (Format.isStringNotEmptyAndNotNull(receiveCountry)) {
			query.append(QueryConstant.COMMON_QUERY21);
			query.append(receiveCountry);
			query.append("'");

		}
		return query;
	}
}
