package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;

public interface DestinationTypeDao {
   public List<MdmDestinationTypeResponseDto> getDestinationTypeList();
}
