package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.DestinationTypeDao;
import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.mapper.DestinationTypeMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("DestinationTypeImpl")
public class DestinationTypeImpl implements DestinationTypeDao {

	@Autowired
	private DataSource dataSource;

	/*
	 * Method-getDestinationTypeList method return all destination type list
	 * using string builder and prepared statement
	 */
	public List<MdmDestinationTypeResponseDto> getDestinationTypeList() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder destinationTypeQuery = new StringBuilder();
		destinationTypeQuery.append(QueryConstant.COMMON_QUERY8);
		List<MdmDestinationTypeResponseDto> destinationTypeList = jdbcTemplate.query(destinationTypeQuery.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new DestinationTypeMapper());

		return destinationTypeList;
	}

}
