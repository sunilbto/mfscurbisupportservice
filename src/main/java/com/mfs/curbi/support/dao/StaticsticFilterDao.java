package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.dto.MdmProductResponseDto;

public interface StaticsticFilterDao {

	public List<MdmProductResponseDto> getProductList();
	
	public List<MdmDestinationTypeResponseDto> getDestinationTypeList();
}
