package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.StaticsticFilterDao;
import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.dto.MdmProductResponseDto;
import com.mfs.curbi.support.mapper.DestinationTypeMapper;
import com.mfs.curbi.support.mapper.ProductMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("StaticsticFilterDao")
public class StaticsticFilterDaoImpl implements StaticsticFilterDao {

	/*
	 * Method-getProductList method return all product
	 * using string builder and prepared statement
	 */

	@Autowired
	private DataSource dataSource;

	public List<MdmProductResponseDto> getProductList() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder productList = new StringBuilder();
		productList.append(QueryConstant.COMMON_QUERY9);

		List<MdmProductResponseDto> productLists = jdbcTemplate.query(productList.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new ProductMapper());

		return productLists;
	}

	/*
	 * Method-getDestinationTypeList method return all destination Type list
	 * using string builder and prepared statement
	 */

	public List<MdmDestinationTypeResponseDto> getDestinationTypeList() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder destinationTypeList = new StringBuilder();
		destinationTypeList.append(QueryConstant.COMMON_QUERY8);
		List<MdmDestinationTypeResponseDto> destinationTypeLists = jdbcTemplate.query(destinationTypeList.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new DestinationTypeMapper());

		return destinationTypeLists;
	}

}
