package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.exception.CurbiHibernateCommonException;
import com.mfs.curbi.support.dao.OrganizationDao;
import com.mfs.curbi.support.dto.GetOrganizationResponseDto;
import com.mfs.curbi.support.mapper.OrganizationMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("OrganizationDaoImpl")
public class OrganizationDaoImpl implements OrganizationDao {

	@Autowired
	private DataSource dataSource;
	private static final Logger log = LoggerFactory.getLogger(OrganizationDaoImpl.class);

	/*
	 * Method-getOrganization method return all organization
	 * using string builder and prepared statement
	 */

	public List<GetOrganizationResponseDto> getOrganization() {

		List<GetOrganizationResponseDto> organizationList = null;
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			StringBuilder organizationQuery = new StringBuilder();
			organizationQuery.append(QueryConstant.ORGANIZATION_QUERY);
			organizationList = jdbcTemplate.query(organizationQuery.toString(), new PreparedStatementSetter() {

				public void setValues(PreparedStatement arg0) throws SQLException {

				}
			}, new OrganizationMapper());

		} catch (HibernateException e) {
			log.error("In Exception OrganizationDaoImpl : " + e);
			throw new CurbiHibernateCommonException(e.getMessage());
		}
		return organizationList;

	}

}
