package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.MoneyFilterDao;
import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.dto.MdmProductResponseDto;
import com.mfs.curbi.support.mapper.DestinationTypeMapper;
import com.mfs.curbi.support.mapper.ProductMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("MoneyFilterDao")
public class MoneyFilterDaoImpl implements MoneyFilterDao {

	@Autowired
	private DataSource dataSource;

	/*
	 * Method-getProductList method return all product list
	 * using string builder and prepared statement
	 */
	public List<MdmProductResponseDto> getProductList() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder productQuery = new StringBuilder();
		productQuery.append(QueryConstant.COMMON_QUERY10);
		List<MdmProductResponseDto> productList = jdbcTemplate.query(productQuery.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new ProductMapper());

		return productList;
	}

	/*
	 * Method-getDestinationType method return all destination type list
	 * using string builder and prepared statement
	 */
	public List<MdmDestinationTypeResponseDto> getDestinationType() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder destinationTypeList = new StringBuilder();
		destinationTypeList.append(QueryConstant.COMMON_QUERY8);
		List<MdmDestinationTypeResponseDto> destinationTypeLists = jdbcTemplate.query(destinationTypeList.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new DestinationTypeMapper());

		return destinationTypeLists;
	}

}
