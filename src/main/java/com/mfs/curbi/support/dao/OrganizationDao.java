package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.GetOrganizationResponseDto;

public interface OrganizationDao {

	public List<GetOrganizationResponseDto> getOrganization() throws Exception;

}
