package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.MoneyFlowDao;
import com.mfs.curbi.support.dto.MoneyFlowDto;
import com.mfs.curbi.support.dto.RecieveCountry;
import com.mfs.curbi.support.dto.SendFromCountry;
import com.mfs.curbi.support.dto.SendTotalTransaction;
import com.mfs.curbi.support.dto.TransAmountAsPerPartner;
import com.mfs.curbi.support.dto.TrasnactionId;
import com.mfs.curbi.support.mapper.RecieveCountryMapper;
import com.mfs.curbi.support.mapper.SendFromCountryMapper;
import com.mfs.curbi.support.mapper.SendTotalTransactionMapper;
import com.mfs.curbi.support.mapper.TransAmountAsPerMapper;
import com.mfs.curbi.support.mapper.TransactionIdMapper;
import com.mfs.curbi.support.util.Format;
import com.mfs.curbi.support.util.QueryConstant;

@Repository
public class MoneyFlowDaoImpl implements MoneyFlowDao {

	@Autowired
	private DataSource dataSource;

	/*
	 * Method-getRecieveFromPartner method return receive from partner list using
	 * string builder and prepared statement
	 */
	public List<RecieveCountry> getRecieveFromPartner(String transaction) throws Exception {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder recievePartner = new StringBuilder();
		recievePartner.append(QueryConstant.MONEYFLOW_QUERY1);
		recievePartner.append(transaction);
		recievePartner.append(")");
		recievePartner.append(QueryConstant.MONEYFLOW_QUERY2);

		List<RecieveCountry> recieveCountries = jdbcTemplate.query(recievePartner.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new RecieveCountryMapper());

		return recieveCountries;
	}

	/*
	 * Method-transAmountAsPerPartners method return transaction amount as per
	 * partner list using string builder and prepared statement
	 */
	public List<TransAmountAsPerPartner> transAmountAsPerPartners(MoneyFlowDto moneyFlowDto, Timestamp startDate,
			Timestamp endDate, String senderPartner) throws Exception {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder transaAmountAsPerPartner = new StringBuilder();
		transaAmountAsPerPartner.append(QueryConstant.MONEYFLOW_QUERY3);

		// call common query for checking filter join
		transaAmountAsPerPartner = commonQuerys(transaAmountAsPerPartner, moneyFlowDto);

		transaAmountAsPerPartner.append(QueryConstant.COMMON_QUERY11);
		transaAmountAsPerPartner.append(moneyFlowDto.getCountryNames());
		transaAmountAsPerPartner.append(QueryConstant.COMMON_QUERY14);
		transaAmountAsPerPartner.append(startDate);
		transaAmountAsPerPartner.append(QueryConstant.COMMON_QUERY3);
		transaAmountAsPerPartner.append(endDate);
		transaAmountAsPerPartner.append(QueryConstant.COMMON_QUERY15);
		transaAmountAsPerPartner.append(moneyFlowDto.getType());
		transaAmountAsPerPartner.append("'");

		// call common query for checking selected filter
		transaAmountAsPerPartner = commonQuery(transaAmountAsPerPartner, moneyFlowDto);

		transaAmountAsPerPartner.append(QueryConstant.MONEYFLOW_QUERY4);

		List<TransAmountAsPerPartner> transAmountAsPerPartners = jdbcTemplate.query(transaAmountAsPerPartner.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new TransAmountAsPerMapper());

		return transAmountAsPerPartners;
	}

	/*
	 * Method-transAmountAsPerPartners method return recievePartner using string
	 * builder and prepared statement
	 */

	public List<RecieveCountry> recievePartner(MoneyFlowDto moneyFlowDto, Timestamp startDate, Timestamp endDate)
			throws Exception {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder recievePartner = new StringBuilder();
		recievePartner.append(QueryConstant.MONEYFLOW_QUERY5);
		recievePartner = commonQuerys(recievePartner, moneyFlowDto);

		recievePartner.append(QueryConstant.COMMON_QUERY11);
		recievePartner.append(moneyFlowDto.getCountryNames());
		recievePartner.append(QueryConstant.COMMON_QUERY12);
		recievePartner.append(moneyFlowDto.getType());
		recievePartner.append(QueryConstant.COMMON_QUERY13);
		recievePartner.append(moneyFlowDto.getStartDate());
		recievePartner.append(QueryConstant.COMMON_QUERY3);
		recievePartner.append(moneyFlowDto.getEndDate());
		recievePartner.append("'");
		recievePartner = commonQuery(recievePartner, moneyFlowDto);

		recievePartner.append(QueryConstant.MONEYFLOW_QUERY2);

		List<RecieveCountry> recieveCountries = jdbcTemplate.query(recievePartner.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new RecieveCountryMapper());

		return recieveCountries;
	}

	/*
	 * Method-sendFromCountrie method return send from country using string builder
	 * and prepared statement
	 */
	public List<SendFromCountry> sendFromCountrie(String transaction) throws Exception {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder sendFromCountrie = new StringBuilder();
		sendFromCountrie.append(QueryConstant.MONEYFLOW_QUERY6);
		sendFromCountrie.append(transaction);
		sendFromCountrie.append(" )");
		sendFromCountrie.append(QueryConstant.MONEYFLOW_QUERY2);
		List<SendFromCountry> sendFromCountries = jdbcTemplate.query(sendFromCountrie.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new SendFromCountryMapper());

		return sendFromCountries;
	}

	/*
	 * Method-getTotalSenderTransaction method return total of send transaction list
	 * using string builder and prepared statement
	 */
	public List<SendTotalTransaction> getTotalRecieveTransaction(MoneyFlowDto moneyFlowDto, Timestamp startDate,
			Timestamp endDate, String transaction) throws Exception {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder totalRecieveTransaction = new StringBuilder();
		totalRecieveTransaction.append(QueryConstant.MONEYFLOW_QUERY);

		totalRecieveTransaction.append(QueryConstant.MONEYFLOW_QUERY7);
		totalRecieveTransaction.append(transaction);
		totalRecieveTransaction.append(" ) ");

		// check partner direction
		if (Format.isStringNotEmptyAndNotNull(moneyFlowDto.getType())) {
			if (moneyFlowDto.getType().equalsIgnoreCase("Send")) {
				totalRecieveTransaction.append(QueryConstant.MONEYFLOW_QUERY8);
			} else {
				totalRecieveTransaction.append(QueryConstant.MONEYFLOW_QUERY9);
			}

		}
		totalRecieveTransaction.append(";");

		List<SendTotalTransaction> sendTransaction = jdbcTemplate.query(totalRecieveTransaction.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new SendTotalTransactionMapper());

		return sendTransaction;
	}

	/*
	 * Method-getTransaction method return all transaction using string builder and
	 * prepared statement
	 */
	public List<TrasnactionId> getTransactions(MoneyFlowDto moneyFlowDto, Timestamp startDate, Timestamp endDate)
			throws Exception {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder transaction = new StringBuilder();
		transaction.append(QueryConstant.COMMON_QUERY16);

		// call common query for checking filter join

		transaction = commonQuerys(transaction, moneyFlowDto);
		transaction.append(QueryConstant.COMMON_QUERY11);
		transaction.append(moneyFlowDto.getCountryNames());
		transaction.append(QueryConstant.COMMON_QUERY14);
		transaction.append(moneyFlowDto.getStartDate());
		transaction.append(QueryConstant.COMMON_QUERY3);
		transaction.append(moneyFlowDto.getEndDate());
		transaction.append("'");
		if (Format.isStringNotEmptyAndNotNull(moneyFlowDto.getType())) {
			if (moneyFlowDto.getType().equalsIgnoreCase("Send")) {
				transaction.append(QueryConstant.MONEYFLOW_QUERY9);
			} else {
				transaction.append(QueryConstant.MONEYFLOW_QUERY8);

			}

		}

		// call common query for checking selected filter
		transaction = commonQuery(transaction, moneyFlowDto);

		transaction.append(";");
		List<TrasnactionId> trasnactionId = jdbcTemplate.query(transaction.toString(), new PreparedStatementSetter() {

			public void setValues(PreparedStatement arg0) throws SQLException {

			}
		}, new TransactionIdMapper());

		return trasnactionId;
	}

	// check for selected filter
	public StringBuilder commonQuery(StringBuilder querys, MoneyFlowDto moneyFlowDto) {
		if (Format.isStringNotEmptyAndNotNull(moneyFlowDto.getProduct())) {
			querys.append(QueryConstant.COMMON_QUERY);
			querys.append(moneyFlowDto.getProduct());
			querys.append("'");
		}
		if (Format.isStringNotEmptyAndNotNull(moneyFlowDto.getDestinationType())) {
			querys.append(QueryConstant.COMMON_QUERY1);
			querys.append(moneyFlowDto.getDestinationType());
			querys.append("'");

		}
		return querys;
	}

	// check filter joins
	public StringBuilder commonQuerys(StringBuilder query, MoneyFlowDto moneyFlowDto) {

		if (Format.isStringNotEmptyAndNotNull(moneyFlowDto.getDestinationType())
				|| Format.isStringNotEmptyAndNotNull(moneyFlowDto.getProduct())) {

			query.append(QueryConstant.COMMON_QUERY5);
		}
		if (Format.isStringNotEmptyAndNotNull(moneyFlowDto.getProduct())) {
			query.append(QueryConstant.COMMON_QUERY6);
		}
		return query;

	}
}