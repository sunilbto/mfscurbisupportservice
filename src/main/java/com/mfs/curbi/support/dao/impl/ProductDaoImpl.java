package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.exception.CurbiHibernateCommonException;
import com.mfs.curbi.support.dao.ProductDao;
import com.mfs.curbi.support.dto.MdmProductResponseDto;
import com.mfs.curbi.support.mapper.ProductMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	private DataSource dataSource;
	private static final Logger log = LoggerFactory.getLogger(ProductDaoImpl.class);

	/*
	 * Method-getAllProduct method return all product
	 * using string builder and prepared statement
	 */
	public List<MdmProductResponseDto> getAllProduct() {
		List<MdmProductResponseDto> productList = null;
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			StringBuilder productQuery = new StringBuilder();
			productQuery.append(QueryConstant.COMMON_QUERY9);

			productList = jdbcTemplate.query(productQuery.toString(), new PreparedStatementSetter() {

				public void setValues(PreparedStatement arg0) throws SQLException {

				}
			}, new ProductMapper());

		} catch (HibernateException e) {
			log.error("In Exception ProductDaoImpl : " + e);
			throw new CurbiHibernateCommonException(e.getMessage());
		}
		return productList;
	}

}
