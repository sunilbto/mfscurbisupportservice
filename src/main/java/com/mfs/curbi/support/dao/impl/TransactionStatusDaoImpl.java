package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.exception.CurbiHibernateCommonException;
import com.mfs.curbi.support.dao.TransactionStatusDao;
import com.mfs.curbi.support.dto.TransactionStatusRsponseDto;
import com.mfs.curbi.support.mapper.TransactionStatusMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("TransactionStatusDaoImpl")
public class TransactionStatusDaoImpl implements TransactionStatusDao {

	@Autowired
	private DataSource dataSource;
	private static final Logger log = LoggerFactory.getLogger(TransactionStatusDaoImpl.class);

	/*
	 * Method-getTransactionStatus method return all transaction Status
	 * using string builder and prepared statement
	 */
	public List<TransactionStatusRsponseDto> getTransactionStatus() {
		List<TransactionStatusRsponseDto> transStatusList = null;
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			StringBuilder transStatus = new StringBuilder();
			transStatus.append(QueryConstant.TRANSACTION_STATUS_QUERY);
			transStatusList = jdbcTemplate.query(transStatus.toString(), new PreparedStatementSetter() {

				public void setValues(PreparedStatement arg0) throws SQLException {

				}
			}, new TransactionStatusMapper());

		} catch (HibernateException e) {
			log.error("In Exception TransactionStatusDaoImpl : " + e);
			throw new CurbiHibernateCommonException(e.getMessage());
		}
		return transStatusList;
	}

}
