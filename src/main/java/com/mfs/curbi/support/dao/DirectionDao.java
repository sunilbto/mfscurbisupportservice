package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.MdmDirectionResonseDto;

public interface DirectionDao {
	public List<MdmDirectionResonseDto> getDirectionList()throws Exception;
}
