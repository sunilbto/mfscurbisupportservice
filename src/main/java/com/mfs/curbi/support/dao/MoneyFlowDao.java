package com.mfs.curbi.support.dao;

import java.sql.Timestamp;
import java.util.List;

import com.mfs.curbi.support.dto.MoneyFlowDto;
import com.mfs.curbi.support.dto.RecieveCountry;
import com.mfs.curbi.support.dto.SendFromCountry;
import com.mfs.curbi.support.dto.SendTotalTransaction;
import com.mfs.curbi.support.dto.TransAmountAsPerPartner;
import com.mfs.curbi.support.dto.TrasnactionId;

public interface MoneyFlowDao {

	public List<RecieveCountry> getRecieveFromPartner(String transacton) throws Exception;

	public List<TransAmountAsPerPartner> transAmountAsPerPartners(MoneyFlowDto moneyFlowDto, Timestamp startDate,
			Timestamp endDate, String senderPartner) throws Exception;

	public List<RecieveCountry> recievePartner(MoneyFlowDto moneyFlowDto, Timestamp startDate, Timestamp endDate)
			throws Exception;

	public List<SendFromCountry> sendFromCountrie(String transaction) throws Exception;


	public List<SendTotalTransaction> getTotalRecieveTransaction(MoneyFlowDto moneyFlowDto, Timestamp startDate,
			Timestamp endDate, String transaction) throws Exception;

	public List<TrasnactionId> getTransactions(MoneyFlowDto moneyFlowDto, Timestamp startDate, Timestamp endDate)
			throws Exception;

}
