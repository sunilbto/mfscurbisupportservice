package com.mfs.curbi.support.dao;

import java.sql.Timestamp;

import com.mfs.curbi.support.dto.AllDashBoardResponse;

public interface DashboardDao {

	public AllDashBoardResponse getDashBoardInfo(long partnerCode,Timestamp startDate,Timestamp endDate,String status,String product,String direction,String destinationType,String receiveCountry)throws Exception;
}
