package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.PricingDetailDao;
import com.mfs.curbi.support.dto.PricingRequestDto;
import com.mfs.curbi.support.dto.PricingResponseRowDto;
import com.mfs.curbi.support.mapper.PricingRowMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("PricingDetailDaoImpl")
public class PricingDetailDaoImpl implements PricingDetailDao {

	@Autowired
	private DataSource dataSource;

	/*
	 * Method-getPricingDetailsDao method return all pricing details
	 * using string builder and prepared statement
	 */
	public List<PricingResponseRowDto> getPricingDetailsDao(PricingRequestDto request) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		StringBuilder pricingDetail = new StringBuilder();

		pricingDetail.append(QueryConstant.PRICING_QUERY);
		pricingDetail.append(request.getForexMargin());
		pricingDetail.append(QueryConstant.PRICING_QUERY1);
		pricingDetail.append(request.getForexMargin());
		pricingDetail.append(QueryConstant.PRICING_QUERY2);
		pricingDetail.append(request.getCountryName());
		pricingDetail.append(QueryConstant.PRICING_QUERY3);

		List<PricingResponseRowDto> pricingDetailList = jdbcTemplate.query(pricingDetail.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new PricingRowMapper());

		return pricingDetailList;
	}

}
