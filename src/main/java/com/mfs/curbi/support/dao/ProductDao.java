package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.MdmProductResponseDto;

public interface ProductDao {
	public List<MdmProductResponseDto> getAllProduct() throws Exception;
}
