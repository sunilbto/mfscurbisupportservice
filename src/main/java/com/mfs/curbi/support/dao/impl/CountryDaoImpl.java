package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.CountryDao;
import com.mfs.curbi.support.dto.MdmCountryResponseDto;
import com.mfs.curbi.support.mapper.CountryMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("CountryDaoImpl")
public class CountryDaoImpl implements CountryDao {

	@Autowired
	private DataSource dataSource;

	/*
	 * Method-getTransaction method return all countries
	 * using string builder and prepared statement
	 */
	public List<MdmCountryResponseDto> getAllCountries() {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder countrys = new StringBuilder();
		countrys.append(QueryConstant.COUNTRY_QUERY);

		List<MdmCountryResponseDto> countryLists = jdbcTemplate.query(countrys.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new CountryMapper());

		return countryLists;
	}

}
