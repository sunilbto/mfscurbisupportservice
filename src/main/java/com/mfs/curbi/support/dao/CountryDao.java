package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.MdmCountryResponseDto;

public interface CountryDao {
	public List<MdmCountryResponseDto> getAllCountries() throws Exception;
}
