package com.mfs.curbi.support.dao;

import java.sql.Timestamp;
import java.util.List;

import com.mfs.curbi.support.dto.ComplianceResponseDto;
import com.mfs.curbi.support.dto.TrasnactionId;



public interface ComplianceDetailsDao {
	public List<ComplianceResponseDto> sendingFromCountryList(String countryName,Timestamp startDate,Timestamp endDate) throws Exception;
	public List<ComplianceResponseDto> sendingToCountryList(String transaction) throws Exception;
	public List<ComplianceResponseDto> receiveFromCountryList(String transaction)throws Exception;
	public List<TrasnactionId> getTransaction(String countryName,Timestamp startDate,Timestamp endDate)
			throws Exception;
	public List<TrasnactionId> getTransactions(String countryName,Timestamp startDate,Timestamp endDate)
			throws Exception;
}
