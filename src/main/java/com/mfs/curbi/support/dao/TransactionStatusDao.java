package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.TransactionStatusRsponseDto;

public interface TransactionStatusDao {
	public List<TransactionStatusRsponseDto> getTransactionStatus() throws Exception;
}
