package com.mfs.curbi.support.dao;

import java.sql.Timestamp;
import java.util.List;

import com.mfs.curbi.support.dto.StatisticCounsumerMapperResponseDto;
import com.mfs.curbi.support.dto.StatisticTransactionSendFromResponseDto;
import com.mfs.curbi.support.dto.StatisticTransactionSendToResponseDto;
import com.mfs.curbi.support.dto.TotalAverage;
import com.mfs.curbi.support.dto.TotalAverageSendTo;
import com.mfs.curbi.support.dto.TrasnactionId;

public interface StatisticDetailDao {
	
	public List<StatisticCounsumerMapperResponseDto> getStatisticCounsmerDetailsDao(String countryName,Timestamp startDate,Timestamp endDate,String product,String destinationType);

	public List<StatisticTransactionSendToResponseDto> getStatisticTransactionSendToDetailsDao(String countryName,Timestamp startDates,Timestamp endDates,String product,String destinationType,String transaction );
	
	public List<StatisticTransactionSendFromResponseDto> getStatisticTransactionSendFromDetailsDao(String countryName,Timestamp startDate,Timestamp endDate,String product,String destinationType);

	public List<TrasnactionId> getTransaction(String countryName,
			Timestamp startDate, Timestamp endDate, String product, String destinationType);
	
	public List<TotalAverage> getTotalAverageSendFrom(String countryName, Timestamp startDate, Timestamp endDate, String product, String destinationType);

	public List<TotalAverageSendTo> getTotalAverageSendTo(String countryName, Timestamp startDate, Timestamp endDate, String product, String destinationType);

	
}


