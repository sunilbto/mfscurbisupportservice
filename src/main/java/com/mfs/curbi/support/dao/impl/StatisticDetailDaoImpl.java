package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.StatisticDetailDao;
import com.mfs.curbi.support.dto.StatisticCounsumerMapperResponseDto;
import com.mfs.curbi.support.dto.StatisticTransactionSendFromResponseDto;
import com.mfs.curbi.support.dto.StatisticTransactionSendToResponseDto;
import com.mfs.curbi.support.dto.TotalAverage;
import com.mfs.curbi.support.dto.TotalAverageSendTo;
import com.mfs.curbi.support.dto.TrasnactionId;
import com.mfs.curbi.support.mapper.StatisticCounsumerMapper;
import com.mfs.curbi.support.mapper.StatisticTransactionMapper;
import com.mfs.curbi.support.mapper.StatisticTransactionSendFromMapper;
import com.mfs.curbi.support.mapper.TotalAverageMapper;
import com.mfs.curbi.support.mapper.TotalAverageSendToMapper;
import com.mfs.curbi.support.mapper.TransactionIdMapper;
import com.mfs.curbi.support.util.Format;
import com.mfs.curbi.support.util.QueryConstant;

@Service("StatisticDetailDao")
public class StatisticDetailDaoImpl implements StatisticDetailDao {

	@Autowired
	private DataSource dataSource;

	/*
	 * Method-getStatisticCounsmerDetailsDao method return all
	 * StatisticCounsmerDetails
	 * using string builder and prepared statement
	 */
	public List<StatisticCounsumerMapperResponseDto> getStatisticCounsmerDetailsDao(String countryName,
			Timestamp startDate, Timestamp endDate, String product, String destinationType) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder staticCounsumerQuery = new StringBuilder();
		staticCounsumerQuery.append(QueryConstant.STATISTIC_CONSUME_QUERY);
		staticCounsumerQuery.append(QueryConstant.STATISTIC_CONSUME_QUERY1);
		staticCounsumerQuery.append(QueryConstant.STATISTIC_CONSUME_QUERY2);
		staticCounsumerQuery.append(countryName);
		staticCounsumerQuery.append(QueryConstant.COMMON_QUERY14);
		staticCounsumerQuery.append(startDate);
		staticCounsumerQuery.append(QueryConstant.COMMON_QUERY3);
		staticCounsumerQuery.append(endDate);
		staticCounsumerQuery.append("'");

		//check selected filter
		if (Format.isStringNotEmptyAndNotNull(product)) {
			staticCounsumerQuery.append(QueryConstant.COMMON_QUERY);
			staticCounsumerQuery.append(product);
			staticCounsumerQuery.append("'");
		}
		if (Format.isStringNotEmptyAndNotNull(destinationType)) {
			staticCounsumerQuery.append(QueryConstant.COMMON_QUERY1);
			staticCounsumerQuery.append(destinationType);
			staticCounsumerQuery.append("'");

		}
		staticCounsumerQuery.append(QueryConstant.STATISTIC_CONSUME_QUERY4);

		List<StatisticCounsumerMapperResponseDto> statisticMapperResponseList = jdbcTemplate
				.query(staticCounsumerQuery.toString(), new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new StatisticCounsumerMapper());

		return statisticMapperResponseList;
	}

	/*
	 * Method-getStatisticTransactionSendToDetailsDao method return all Statistic
	 * transaction
	 * using string builder and prepared statement
	 */

	public List<StatisticTransactionSendToResponseDto> getStatisticTransactionSendToDetailsDao(String countryName,
			Timestamp startDates, Timestamp endDates, String product, String destinationType, String transaction) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder StatisticTransactionqQuery = new StringBuilder();

		long diff = endDates.getTime() - startDates.getTime();
		long diffDays = (diff / (24 * 60 * 60 * 1000));

		StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY);
		
       // checking date difference and prepare query as per selected date
		
		if (diffDays <= 7) {
			StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY1);

		} else if (diffDays > 7 && diffDays <= 30) {
			StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY2);

		} else if (diffDays > 30) {
			StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY3);

		}
		StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY4);
		StatisticTransactionqQuery = commonQuerys(StatisticTransactionqQuery, product, destinationType);

		StatisticTransactionqQuery.append(QueryConstant.COMMON_QUERY11);
		StatisticTransactionqQuery.append(countryName);
		StatisticTransactionqQuery.append(QueryConstant.COMMON_QUERY2);
		StatisticTransactionqQuery.append(startDates);
		StatisticTransactionqQuery.append(QueryConstant.COMMON_QUERY3);
		StatisticTransactionqQuery.append(endDates);
		StatisticTransactionqQuery.append(QueryConstant.COMMON_QUERY4);
		StatisticTransactionqQuery.append(transaction);
		StatisticTransactionqQuery.append(")");
		
		//check selected filter
		StatisticTransactionqQuery = commonQuery(StatisticTransactionqQuery, product, destinationType);

		StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY8);
		
		List<StatisticTransactionSendToResponseDto> statisticTransactionList = jdbcTemplate
				.query(StatisticTransactionqQuery.toString(), new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new StatisticTransactionMapper());

		return statisticTransactionList;
	}

	/*
	 * Method-getStatisticTransactionSendFromDetailsDao method return all Statistic
	 * transaction
	 * using string builder and prepared statement
	 */
	public List<StatisticTransactionSendFromResponseDto> getStatisticTransactionSendFromDetailsDao(String countryName,
			Timestamp startDate, Timestamp endDate, String product, String destinationType) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder StatisticTransactionqQuery = new StringBuilder();
		long diff = endDate.getTime() - startDate.getTime();
		long diffDays = (diff / (24 * 60 * 60 * 1000));
		StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY);

		//check date difference and prepare query as per selected date
		if (diffDays < 7) {
			StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY1);

		} else if (diffDays > 7 && diffDays < 30) {
			StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY2);

		} else if (diffDays >= 30) {
			StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY3);

		}
		StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY4);
		
		//check filter join
		StatisticTransactionqQuery = commonQuerys(StatisticTransactionqQuery, product, destinationType);

		StatisticTransactionqQuery.append(QueryConstant.COMMON_QUERY11);
		StatisticTransactionqQuery.append(countryName);

		StatisticTransactionqQuery.append(QueryConstant.COMMON_QUERY2);
		StatisticTransactionqQuery.append(startDate);
		StatisticTransactionqQuery.append(QueryConstant.COMMON_QUERY3);
		StatisticTransactionqQuery.append(endDate);
		StatisticTransactionqQuery.append("'");
		
		//check selected filter
		StatisticTransactionqQuery = commonQuery(StatisticTransactionqQuery, product, destinationType);

		StatisticTransactionqQuery.append(QueryConstant.STATISTIC_TRANSACTION_QUERY8);

		List<StatisticTransactionSendFromResponseDto> statisticTransactionList = jdbcTemplate
				.query(StatisticTransactionqQuery.toString(), new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new StatisticTransactionSendFromMapper());

		return statisticTransactionList;

	}

	/*
	 * Method-getTransaction method return all Transaction
	 * using string builder and prepared statement
	 */
	public List<TrasnactionId> getTransaction(String countryName, Timestamp startDate, Timestamp endDate,
			String product, String destinationType) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder transaction = new StringBuilder();
		transaction.append(QueryConstant.COMMON_QUERY16);
		
		//check filter join
		transaction = commonQuerys(transaction, product, destinationType);

		transaction.append(QueryConstant.STATISTIC_TRANSACTION_QUERY5);
		transaction.append(countryName);
		transaction.append(QueryConstant.COMMON_QUERY7);
		transaction.append(startDate);
		transaction.append("'AND'");
		transaction.append(endDate);
		transaction.append("'");
		
		//check selected filter
		transaction = commonQuery(transaction, product, destinationType);

		transaction.append(";");

		List<TrasnactionId> trasnactionId = jdbcTemplate.query(transaction.toString(), new PreparedStatementSetter() {

			public void setValues(PreparedStatement arg0) throws SQLException {

			}
		}, new TransactionIdMapper());

		return trasnactionId;
	}

	// check selected filter 
	public StringBuilder commonQuery(StringBuilder querys, String product, String destinationType) {
		if (Format.isStringNotEmptyAndNotNull(product)) {
			querys.append(QueryConstant.COMMON_QUERY);
			querys.append(product);
			querys.append("'");
		}
		if (Format.isStringNotEmptyAndNotNull(destinationType)) {
			querys.append(QueryConstant.COMMON_QUERY1);
			querys.append(destinationType);
			querys.append("'");

		}
		return querys;
	}

	// check filter joins
	public StringBuilder commonQuerys(StringBuilder query, String product, String destinationType) {

		if (Format.isStringNotEmptyAndNotNull(destinationType) || Format.isStringNotEmptyAndNotNull(product)) {

			query.append(QueryConstant.COMMON_QUERY5);
		}
		if (Format.isStringNotEmptyAndNotNull(product)) {
			query.append(QueryConstant.COMMON_QUERY6);
		}
		return query;

	}

	public List<TotalAverage> getTotalAverageSendFrom(String countryName, Timestamp startDate, Timestamp endDate, String product,
			String destinationType) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder totalAverage = new StringBuilder();
		totalAverage.append(QueryConstant.STATISTIC_TRANSACTION_QUERY6);
		totalAverage.append(QueryConstant.STATISTIC_TRANSACTION_QUERY4);
//		totalAverage.append(QueryConstant.STATISTIC_TRANSACTION_QUERY5);
		totalAverage=commonQuerys(totalAverage, product, destinationType);
		totalAverage.append(QueryConstant.COMMON_QUERY11);
		totalAverage.append(countryName);
		totalAverage.append(QueryConstant.COMMON_QUERY2);
		totalAverage.append(startDate);
		totalAverage.append(QueryConstant.COMMON_QUERY3);
		totalAverage.append(endDate);
		totalAverage.append("'");
		totalAverage = commonQuery(totalAverage, product, destinationType);
		
		List<TotalAverage> totalAvg = jdbcTemplate.query(totalAverage.toString(), new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new TotalAverageMapper());

		return totalAvg;
		
		
	}

	public List<TotalAverageSendTo> getTotalAverageSendTo(String countryName, Timestamp startDate, Timestamp endDate,
			String product, String destinationType) {
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder totalAverage = new StringBuilder();
		totalAverage.append(QueryConstant.STATISTIC_TRANSACTION_QUERY6);
		totalAverage.append(QueryConstant.STATISTIC_TRANSACTION_QUERY4);
//		totalAverage.append(QueryConstant.STATISTIC_TRANSACTION_QUERY5);
		totalAverage=commonQuerys(totalAverage, product, destinationType);
		totalAverage.append(QueryConstant.COMMON_QUERY11);
		totalAverage.append(countryName);
		totalAverage.append(QueryConstant.COMMON_QUERY7);
		totalAverage.append(startDate);
		totalAverage.append(QueryConstant.COMMON_QUERY3);
		totalAverage.append(endDate);
		totalAverage.append("'");
		totalAverage = commonQuery(totalAverage, product, destinationType);
		
		List<TotalAverageSendTo> totalAvg = jdbcTemplate.query(totalAverage.toString(), new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new TotalAverageSendToMapper());

		return totalAvg;
	}
}
