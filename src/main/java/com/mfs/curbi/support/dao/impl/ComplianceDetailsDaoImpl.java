package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.ComplianceDetailsDao;
import com.mfs.curbi.support.dto.ComplianceResponseDto;
import com.mfs.curbi.support.dto.TrasnactionId;
import com.mfs.curbi.support.mapper.ReceivingFromCountryRowMapper;
import com.mfs.curbi.support.mapper.SendingFromCountryRowMapper;
import com.mfs.curbi.support.mapper.SendingToCountryRowMapper;
import com.mfs.curbi.support.mapper.TransactionIdMapper;
import com.mfs.curbi.support.util.QueryConstant;

@Repository("ComplianceDetailsDao")
public class ComplianceDetailsDaoImpl implements ComplianceDetailsDao {

	@Autowired
	private DataSource dataSource;

	/*
	 * Method-sendingFromCountryList method return sending from country list
	 * using string builder and prepared statement
	 */
	public List<ComplianceResponseDto> sendingFromCountryList(String countryName, Timestamp startDate,
			Timestamp endDate) throws Exception {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder sendingFromCountry = new StringBuilder();
		sendingFromCountry.append(QueryConstant.COMPLIANCE_QUERY);
		sendingFromCountry.append(countryName);
		sendingFromCountry.append(QueryConstant.COMPLIANCE_QUERY1);
		sendingFromCountry.append(startDate);
		sendingFromCountry.append(QueryConstant.COMMON_QUERY3);
		sendingFromCountry.append(endDate);
		sendingFromCountry.append(QueryConstant.COMPLIANCE_QUERY3);

		List<ComplianceResponseDto> sendingFroCountryList = jdbcTemplate.query(sendingFromCountry.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new SendingFromCountryRowMapper());

		return sendingFroCountryList;
	}

	/*
	 * Method-sendingToCountryList method return sending to country list
	 * using string builder and prepared statement
	 */
	public List<ComplianceResponseDto> sendingToCountryList(String transaction) throws Exception {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder sendingToCountry = new StringBuilder();
		sendingToCountry.append(QueryConstant.COMPLIANCE_QUERY4);
		sendingToCountry.append(transaction);
		sendingToCountry.append(QueryConstant.COMPLIANCE_QUERY5);

		List<ComplianceResponseDto> sendingToCountryList = jdbcTemplate.query(sendingToCountry.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new SendingToCountryRowMapper());

		return sendingToCountryList;
	}

	/*
	 * Method-receiveFromCountryList method return receive from country list
	 * using string builder and prepared statement
	 */
	public List<ComplianceResponseDto> receiveFromCountryList(String transaction) throws Exception {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder receiveFromCountry = new StringBuilder();
		receiveFromCountry.append(QueryConstant.COMPLIANCE_QUERY6);
		receiveFromCountry.append(transaction);
		receiveFromCountry.append(QueryConstant.COMPLIANCE_QUERY5);

		List<ComplianceResponseDto> receiveFroCountryList = jdbcTemplate.query(receiveFromCountry.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new ReceivingFromCountryRowMapper());

		return receiveFroCountryList;

	}

	/*
	 * Method-getTransaction method return all transactionId
	 * using string builder and prepared statement
	 */
	public List<TrasnactionId> getTransaction(String countryName, Timestamp startDate, Timestamp endDate)
			throws Exception {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder transactions = new StringBuilder();
		transactions.append(QueryConstant.COMPLIANCE_QUERY7);
		transactions.append(countryName);
		transactions.append(QueryConstant.COMMON_QUERY7);
		transactions.append(startDate);
		transactions.append(QueryConstant.COMMON_QUERY3);
		transactions.append(endDate);
		transactions.append(QueryConstant.COMPLIANCE_QUERY3);

		List<TrasnactionId> trasnactionId = jdbcTemplate.query(transactions.toString(), new PreparedStatementSetter() {

			public void setValues(PreparedStatement arg0) throws SQLException {

			}
		}, new TransactionIdMapper());

		return trasnactionId;
	}

	/*
	 * Method-getTransactions method return all transactionId
	 * using string builder and prepared statement
	 */
	public List<TrasnactionId> getTransactions(String countryName, Timestamp startDate, Timestamp endDate)
			throws Exception {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder transactions = new StringBuilder();
		transactions.append(QueryConstant.COMPLIANCE_QUERY7);
		transactions.append(countryName);
		transactions.append(QueryConstant.COMMON_QUERY2);
		transactions.append(startDate);
		transactions.append(QueryConstant.COMMON_QUERY3);
		transactions.append(endDate);
		transactions.append(QueryConstant.COMPLIANCE_QUERY3);

		List<TrasnactionId> trasnactionId = jdbcTemplate.query(transactions.toString(), new PreparedStatementSetter() {

			public void setValues(PreparedStatement arg0) throws SQLException {

			}
		}, new TransactionIdMapper());

		return trasnactionId;
	}

}
