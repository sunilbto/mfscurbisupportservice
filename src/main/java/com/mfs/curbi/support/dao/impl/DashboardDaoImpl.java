package com.mfs.curbi.support.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.mfs.curbi.support.dao.DashboardDao;
import com.mfs.curbi.support.dto.AllDashBoardResponse;
import com.mfs.curbi.support.dto.AmountResponseDto;
import com.mfs.curbi.support.dto.DashResponseDto;
import com.mfs.curbi.support.mapper.AmountMapper;
import com.mfs.curbi.support.mapper.DashMapper;
import com.mfs.curbi.support.util.Format;
import com.mfs.curbi.support.util.QueryConstant;

@Repository
public class DashboardDaoImpl implements DashboardDao {

	@Autowired
	DataSource dataSource;

	/*
	 * Method-getDashBoardInfo method return all dashboard info
	 * using string builder and prepared statement
	 */
	@SuppressWarnings("unused")
	public AllDashBoardResponse getDashBoardInfo(long partnerCode, Timestamp startDate, Timestamp endDate,
			String status, String product, String direction, String destinationType, String receiveCountry)
			throws Exception {
		// log.info("Inside getDashBoardInfo() method of DashboardDaoImpl");

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		StringBuilder getDashBoardQuery = new StringBuilder();
		

		long tempCount = 0l;
		double amount = 0.0;
		double fxshare = 0.0;
		double commission = 0.0;
		double feeShare = 0.0;

		AllDashBoardResponse allRow = new AllDashBoardResponse();

		List<Date> processedDateList = new ArrayList<Date>();
		List<Double> amountList = new ArrayList<Double>();
		List<Double> countList = new ArrayList<Double>();

		Date tempDateList = null;
		double tempamountList = 0.0;
		double tempCountList = 0.0;
		String ccy = null;

		double current = 0.0;

		getDashBoardQuery.append(QueryConstant.DASHBORAD_QUERY);
		getDashBoardQuery.append(partnerCode);
		getDashBoardQuery.append(QueryConstant.DASHBORAD_QUERY4);
		getDashBoardQuery.append(startDate);
		getDashBoardQuery.append(QueryConstant.COMMON_QUERY3);
		getDashBoardQuery.append(endDate);
		getDashBoardQuery.append("'");

		//check filter of dashborad
		if (Format.isStringNotEmptyAndNotNull(status)) {
			getDashBoardQuery.append(QueryConstant.COMMON_QUERY17);
			getDashBoardQuery.append(status);
			getDashBoardQuery.append("'");
		}
		if (Format.isStringNotEmptyAndNotNull(product)) {
			getDashBoardQuery.append(QueryConstant.COMMON_QUERY18);
			getDashBoardQuery.append(product);
			getDashBoardQuery.append("'");
		}
		if (Format.isStringNotEmptyAndNotNull(direction)) {
			getDashBoardQuery.append(QueryConstant.COMMON_QUERY22);
			getDashBoardQuery.append(direction);
			getDashBoardQuery.append("'");

		}
		if (Format.isStringNotEmptyAndNotNull(destinationType)) {
			getDashBoardQuery.append(QueryConstant.COMMON_QUERY19);
			getDashBoardQuery.append(destinationType);
			getDashBoardQuery.append("'");

		}
		if (Format.isStringNotEmptyAndNotNull(receiveCountry)) {
			getDashBoardQuery.append(QueryConstant.COMMON_QUERY23);
			getDashBoardQuery.append(receiveCountry);
			getDashBoardQuery.append("'");


		}
		getDashBoardQuery.append(QueryConstant.DASHBORAD_QUERY1);

		List<DashResponseDto> dashList = jdbcTemplate.query(getDashBoardQuery.toString(), new PreparedStatementSetter() {

			public void setValues(PreparedStatement arg0) throws SQLException {

			}
		}, new DashMapper());

		StringBuilder amountQuery = new StringBuilder();
		amountQuery.append(QueryConstant.DASHBORAD_QUERY2);
		amountQuery.append(partnerCode);
		amountQuery.append("'");
		amountQuery.append(QueryConstant.DASHBORAD_QUERY3);

		List<AmountResponseDto> currentBalList = jdbcTemplate.query(amountQuery.toString(),
				new PreparedStatementSetter() {

					public void setValues(PreparedStatement arg0) throws SQLException {

					}
				}, new AmountMapper());

		//check current balance list
		if (!(currentBalList.isEmpty())) {

			for (AmountResponseDto currentBal : currentBalList) {
				current = currentBal.getAfterBalance();
				ccy = currentBal.getPartnerSettlCcy();
			}
		}

		for (int i = 0; i < dashList.size(); i++) {
			amount = amount + dashList.get(i).getFinalAmount();

			fxshare = fxshare + dashList.get(i).getForexRevShare();

			commission = commission + dashList.get(i).getCommission();

			feeShare = feeShare + dashList.get(i).getFeeRevShare();

			tempDateList = dashList.get(i).getProcessedDate();
			processedDateList.add(tempDateList);

			tempCount = tempCount + dashList.get(i).getCount();

			tempamountList = dashList.get(i).getFinalAmount();
			amountList.add(tempamountList);

			tempCountList = dashList.get(i).getCount();
			countList.add(tempCountList);

		}

		allRow.setFxShare(fxshare);
		allRow.setCommission(commission);
		allRow.setFeeshare(feeShare);
		allRow.setFinalAmount(amount);
		allRow.setFinalTransactionCount(tempCount);
		allRow.setProcessedDateList(processedDateList);
		allRow.setTransactionCountList(countList);
		allRow.setAmountList(amountList);
		allRow.setCurrentBalance(current);
		allRow.setCcy(ccy);

		return allRow;
	}

}
