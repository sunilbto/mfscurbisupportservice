package com.mfs.curbi.support.dao;

import java.util.List;

import com.mfs.curbi.support.dto.PricingRequestDto;
import com.mfs.curbi.support.dto.PricingResponseRowDto;

public interface PricingDetailDao {
	
	public List<PricingResponseRowDto> getPricingDetailsDao(PricingRequestDto request);

}
