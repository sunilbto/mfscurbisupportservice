package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.AmountResponseDto;

public class AmountMapper implements RowMapper<AmountResponseDto> {

	
	public AmountResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
	
		AmountResponseDto response = new AmountResponseDto();
		response.setAfterBalance(rs.getDouble("afterBalance"));
		response.setPartnerSettlCcy(rs.getString("partnerSettlCcy"));

		return response;
	}

}
