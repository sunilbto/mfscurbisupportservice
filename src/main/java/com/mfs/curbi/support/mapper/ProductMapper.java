package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmProductResponseDto;

public class ProductMapper implements RowMapper<MdmProductResponseDto> {

	
	public MdmProductResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		MdmProductResponseDto response = new MdmProductResponseDto();
		response.setMdmProductId(rs.getInt("id"));
		response.setProductName(rs.getString("product_name"));
		response.setRateProfile(rs.getInt("rate_profile"));
		response.setProductDescription(rs.getString("product_description"));
		return response;
	}

}
