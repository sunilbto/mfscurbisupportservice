package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.RecieveCountry;

public class RecieveCountryMapper implements RowMapper<RecieveCountry>{

	public RecieveCountry mapRow(ResultSet rs, int rowNum) throws SQLException {
		RecieveCountry recieveCountry=new RecieveCountry();
		recieveCountry.setAmount(rs.getDouble("value_currency"));
		recieveCountry.setPartnerCountry(rs.getString("Partner_country"));
		recieveCountry.setSpcAmount(rs.getDouble("value_spc"));
		recieveCountry.setTotalAmount(rs.getDouble("totalAmount"));
		recieveCountry.setTxnCount(rs.getInt("trans_count"));
		return recieveCountry;
	}

}
