package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmPartnerProductResponseDto;

public class MdmPartnerProductMapper implements RowMapper<MdmPartnerProductResponseDto> {

	
	public MdmPartnerProductResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		MdmPartnerProductResponseDto response = new MdmPartnerProductResponseDto();

		response.setPartnerProductId(rs.getInt("id"));
		response.setPartnerId(rs.getInt("partner"));
		response.setProductId(rs.getInt("product"));
		response.setPivotCaseId(rs.getInt("pivot_case_id"));
		response.setEnabled(rs.getBoolean("is_enabled"));

		return response;
	}

}
