package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TransactionSpcAmount;

public class SpcAmountMapper implements RowMapper<TransactionSpcAmount> {

	public TransactionSpcAmount mapRow(ResultSet rs, int rowNum) throws SQLException {
		TransactionSpcAmount transactionSpcAmount=new TransactionSpcAmount();
		transactionSpcAmount.setSpcAmount(rs.getDouble("Spc_Amount"));
		return transactionSpcAmount;
	}

}
