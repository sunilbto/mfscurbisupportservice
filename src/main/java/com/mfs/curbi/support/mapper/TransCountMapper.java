package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TransactionCountDto;

public class TransCountMapper implements RowMapper<TransactionCountDto>{

	public TransactionCountDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		TransactionCountDto transactionCountDto=new TransactionCountDto();
		transactionCountDto.setTotalTransactionCount(rs.getInt("Trans_count"));
		return transactionCountDto;
	}

}
