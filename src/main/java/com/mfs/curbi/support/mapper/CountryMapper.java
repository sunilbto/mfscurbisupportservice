package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmCountryResponseDto;

public class CountryMapper implements RowMapper<MdmCountryResponseDto> {

	
	public MdmCountryResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		MdmCountryResponseDto response = new MdmCountryResponseDto();
		response.setMdmCountryId(rs.getInt("id"));
		response.setCountryName(rs.getString("country_name"));
		response.setCountryCode(rs.getString("country_code"));
		response.setNumericCode(rs.getString("numeric_code"));
		response.setPhoneCode(rs.getInt("phone_code"));
		return response;
	}

}
