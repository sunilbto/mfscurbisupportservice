package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.SendFromReceieve;

public class SendFromReceieveMapper implements RowMapper<SendFromReceieve> {

	public SendFromReceieve mapRow(ResultSet rs, int rowNum) throws SQLException {
		SendFromReceieve response=new SendFromReceieve();
		response.setPartnerName(rs.getString("Partner_name"));
		response.setPartnerCode(rs.getString("partner_code"));
		
		
		return response;
	}

}
