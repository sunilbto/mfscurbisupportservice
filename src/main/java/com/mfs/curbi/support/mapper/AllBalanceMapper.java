package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.AllBalanceResponseDto;

public class AllBalanceMapper implements RowMapper<AllBalanceResponseDto> {
	
	
	
	public AllBalanceResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AllBalanceResponseDto response = new AllBalanceResponseDto();

		response.setAfterBalance(rs.getDouble("afterBalance"));
		response.setBeforeBalance(rs.getDouble("beforeBalance"));
		response.setCcy(rs.getString("Ccy"));
		response.setCommission(rs.getDouble("Commission"));
		response.setFeeShare(rs.getDouble("FeeShare"));
		response.setFxShare(rs.getDouble("fxShare"));
		response.setMfsId(rs.getString("mfsId"));
		response.setPrinciple(rs.getDouble("Principal"));
		response.setStatus(rs.getString("Status"));
		response.setTransactionDetailId(rs.getInt("transaction_detail_id"));
		response.setType(rs.getString("Type"));
		response.setValueDate(rs.getDate("ValueDate"));
		response.setPartnerDirection(rs.getString("partner_direction"));
		response.setPartnerCountry(rs.getString("partner_country"));
		response.setDestinationType(rs.getString("destination_type"));
		response.setProductName(rs.getString("product_name"));

		return response;
	}

}
