package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.SendFromCountry;

public class SendFromCountryMapper implements RowMapper<SendFromCountry>{

	public SendFromCountry mapRow(ResultSet rs, int rowNum) throws SQLException {
		SendFromCountry SendFromCountry=new SendFromCountry();
		SendFromCountry.setAmount(rs.getDouble("value_currency"));
		SendFromCountry.setPartnerCountry(rs.getString("Partner_country"));
		SendFromCountry.setSpcAmount(rs.getDouble("value_spc"));
		SendFromCountry.setTotalAmount(rs.getDouble("totalamount"));
		SendFromCountry.setTxnCount(rs.getInt("trans_count"));
		return SendFromCountry;
	}

}
