package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TmpNspLedBalanceResponseDto;

public class BalanceMapper implements RowMapper<TmpNspLedBalanceResponseDto> {

	
	public TmpNspLedBalanceResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		TmpNspLedBalanceResponseDto response = new TmpNspLedBalanceResponseDto();
		response.setTmpNspLedBalanceId(rs.getInt("id"));
		response.setPartnerId(rs.getInt("partner_id"));
		response.setPartnerSettlCcy(rs.getString("partner_settl_ccy"));
		response.setBeforeBalance(rs.getDouble("before_balance"));
		response.setAfterBalance(rs.getDouble("after_balance"));
		response.setValueDate(rs.getDate("value_date"));
		return response;
	}

}
