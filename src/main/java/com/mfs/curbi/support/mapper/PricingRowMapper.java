package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.PricingResponseRowDto;

public class PricingRowMapper implements RowMapper<PricingResponseRowDto> {

	public PricingResponseRowDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PricingResponseRowDto response=new PricingResponseRowDto();
		response.setPartnerName(rs.getString("partner_name"));
		response.setAverageSendFee(rs.getDouble("AverageSendFee"));
		response.setAverageTotalCost(rs.getDouble("AverageTotalCost"));
		response.setPartnerCurrency(rs.getString("partnerCurrency"));
		response.setAverageTotalCostPer(rs.getDouble("AverageTotalCostPercentage"));
		response.setTotalCount(rs.getDouble("count"));
		return response;
	}

}
