package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TotalAverageSendTo;

public class TotalAverageSendToMapper implements RowMapper<TotalAverageSendTo>{

	public TotalAverageSendTo mapRow(ResultSet rs, int rowNum) throws SQLException {
		TotalAverageSendTo totalAverage=new TotalAverageSendTo();
		totalAverage.setTotalAvgSendTo(rs.getDouble("totalAvg"));
		return totalAverage;
	}
	

}
