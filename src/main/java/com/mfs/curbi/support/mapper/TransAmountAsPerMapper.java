package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TransAmountAsPerPartner;

public class TransAmountAsPerMapper implements RowMapper<TransAmountAsPerPartner> {

	public TransAmountAsPerPartner mapRow(ResultSet rs, int rowNum) throws SQLException {
		TransAmountAsPerPartner transAmountAsPerPartner=new TransAmountAsPerPartner();
		transAmountAsPerPartner.setTransAmount(rs.getDouble("Trans_amount"));
		transAmountAsPerPartner.setTransDate(rs.getString("transactionDate"));
		transAmountAsPerPartner.setTransCount(rs.getInt("Trans_count"));
		transAmountAsPerPartner.setSpcAmount(rs.getDouble("value_spc"));
		transAmountAsPerPartner.setCurrencyCode(rs.getString("currency"));
		
		return transAmountAsPerPartner;
	}

}
