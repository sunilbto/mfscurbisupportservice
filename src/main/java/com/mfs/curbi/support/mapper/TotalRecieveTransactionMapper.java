package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TotalRecieve;

public class TotalRecieveTransactionMapper implements RowMapper<TotalRecieve> {
	public TotalRecieve mapRow(ResultSet rs, int rowNum) throws SQLException {
		TotalRecieve recieveCountry=new TotalRecieve();
		recieveCountry.setAmount(rs.getDouble("value_currency"));
		recieveCountry.setSpcAmount(rs.getDouble("value_spc"));
		recieveCountry.setTotalAmount(rs.getDouble("totalAmount"));
		recieveCountry.setTxnCount(rs.getInt("trans_count"));
		return recieveCountry;
	}
}
