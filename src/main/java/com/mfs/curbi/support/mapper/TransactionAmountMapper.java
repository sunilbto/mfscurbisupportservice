package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TransactionAmount;

public class TransactionAmountMapper implements RowMapper<TransactionAmount>{

	public TransactionAmount mapRow(ResultSet rs, int rowNum) throws SQLException {
		TransactionAmount transactionAmount=new TransactionAmount();
		transactionAmount.setTransactionAmount(rs.getDouble("Trans_amount"));
		return transactionAmount;
	}

}
