package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.StatisticCounsumerMapperResponseDto;

public class StatisticCounsumerMapper implements RowMapper<StatisticCounsumerMapperResponseDto> {

	public StatisticCounsumerMapperResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StatisticCounsumerMapperResponseDto response=new StatisticCounsumerMapperResponseDto();
		response.setTransactionProcessedDate(rs.getTimestamp("transaction_processed_date"));
		response.setSendCount(rs.getDouble("send_count"));
		response.setReceiveCount(rs.getDouble("receive_count"));
		return response;
	}

}
