package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TrasnactionId;

public class TransactionIdMapper implements RowMapper<TrasnactionId>{

	public TrasnactionId mapRow(ResultSet rs, int rowNum) throws SQLException {
		TrasnactionId id=new TrasnactionId();
		id.setTransactionId(rs.getString("transaction_detail_id"));	
		return id;
	}

}
