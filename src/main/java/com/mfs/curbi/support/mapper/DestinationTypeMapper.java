package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;

public class DestinationTypeMapper implements RowMapper<MdmDestinationTypeResponseDto>{

	public MdmDestinationTypeResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		
		MdmDestinationTypeResponseDto response=new MdmDestinationTypeResponseDto();
		response.setDestinationType(rs.getString("destination_type"));
		
		return response;
	}

}
