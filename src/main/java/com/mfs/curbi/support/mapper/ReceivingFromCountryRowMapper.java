package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.ComplianceResponseDto;

public class ReceivingFromCountryRowMapper implements RowMapper<ComplianceResponseDto> {

	public ComplianceResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		ComplianceResponseDto response=new ComplianceResponseDto();
		response.setPartnerName(rs.getString("partner_name"));
		response.setCountryName(rs.getString("partner_country"));
		return response;
		
	}

}
