package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmSendCountryDto;

public class SendCountryMapper implements RowMapper<MdmSendCountryDto> {

	public MdmSendCountryDto mapRow(ResultSet rs, int arg1) throws SQLException {
		
		MdmSendCountryDto response=new MdmSendCountryDto();
		response.setCountryId(rs.getInt("id"));
		response.setCountryName(rs.getString("country_name"));
		return response;
	}

}
