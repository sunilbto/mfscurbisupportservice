package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmSendPartnerResponseDto;



public class SendPartnerMapper implements RowMapper<MdmSendPartnerResponseDto> {

	public MdmSendPartnerResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		MdmSendPartnerResponseDto response =new MdmSendPartnerResponseDto();
		response.setPartnerName(rs.getString("partner_name"));
		response.setPartnerCode(rs.getString("partner_code"));
		return response;
	}

}
