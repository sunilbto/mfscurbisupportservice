package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.CurrencyResponseDto;

public class CurrencyMapper implements RowMapper<CurrencyResponseDto> {

	
	public CurrencyResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		CurrencyResponseDto response = new CurrencyResponseDto();
		response.setPartnerCurrency(rs.getString("partner_currency"));
		return response;
	}

}
