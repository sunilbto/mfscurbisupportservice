package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.GetPartnerCode;

public class GetPartnerCodeMapper implements RowMapper<GetPartnerCode>{

	public GetPartnerCode mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		GetPartnerCode response=new GetPartnerCode();
		response.setPartnerCode(rs.getString("partner_code"));
		
		return response;
	}

}
