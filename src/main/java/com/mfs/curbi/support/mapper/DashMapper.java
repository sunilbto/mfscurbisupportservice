package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.DashResponseDto;

public class DashMapper implements RowMapper<DashResponseDto> {

	
	public DashResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		
		DashResponseDto response = new DashResponseDto();

		response.setPartnerId(rs.getString("partner_id"));
		response.setProcessedDate(rs.getDate("date"));
		response.setCount(rs.getInt("count"));
		response.setFinalAmount(rs.getDouble("amount"));
		response.setFeeRevShare(rs.getDouble("fee_rev_share"));
		response.setForexRevShare(rs.getDouble("forex_rev_share"));
		response.setCommission(rs.getDouble("commission"));

		return response;
	}

}
