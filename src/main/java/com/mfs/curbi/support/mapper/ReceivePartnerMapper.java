package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmReceivePartnerResponseDto;

public class ReceivePartnerMapper implements RowMapper<MdmReceivePartnerResponseDto> {

	public MdmReceivePartnerResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		MdmReceivePartnerResponseDto response=new MdmReceivePartnerResponseDto();
		response.setPartnerName(rs.getString("partner_name"));
		response.setPartnerCode(rs.getString("partner_code"));
		return response;
	}

}
