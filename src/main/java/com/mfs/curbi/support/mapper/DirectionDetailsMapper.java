package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmDirectionResonseDto;

public class DirectionDetailsMapper implements RowMapper<MdmDirectionResonseDto> {
	
	
	public MdmDirectionResonseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		MdmDirectionResonseDto response = new MdmDirectionResonseDto();
		response.setMdmDirctionId(rs.getInt("id"));
		response.setDirectionName(rs.getString("direction_name"));
		response.setDescription(rs.getString("description"));
		return response;
	}

}
