package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.ReceieveFromSend;

public class ReceieveFromSendMapper implements RowMapper<ReceieveFromSend> {

	public ReceieveFromSend mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ReceieveFromSend response=new ReceieveFromSend();
		response.setPartnerName(rs.getString("Partner_name"));
		response.setPartnerCode(rs.getString("partner_code"));
		
		return response;
	}

}
