package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.SendTotalTransaction;

public class SendTotalTransactionMapper implements RowMapper<SendTotalTransaction> {

	public SendTotalTransaction mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		SendTotalTransaction sendFromCountry=new SendTotalTransaction();
		sendFromCountry.setAmount(rs.getDouble("value_currency"));
		sendFromCountry.setSpcAmount(rs.getDouble("value_spc"));
		sendFromCountry.setCurrency(rs.getString("currency"));
		return sendFromCountry;
	}

}
