package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.StatisticTransactionSendToResponseDto;

public class StatisticTransactionMapper implements RowMapper<StatisticTransactionSendToResponseDto> {

	public StatisticTransactionSendToResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StatisticTransactionSendToResponseDto response=new StatisticTransactionSendToResponseDto();
		
	//	response.setCount(rs.getInt("count"));
		response.setAvgAmount(rs.getDouble("avg_amount"));
		response.setProcessedDate(rs.getString("transactionDate"));
		response.setDate(rs.getString("processedDate"));
		
		return response;
	}

}
