package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.CurrentBalanceDto;

public class CurrentBalanceMapper implements RowMapper<CurrentBalanceDto> {

	
	public CurrentBalanceDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		CurrentBalanceDto response = new CurrentBalanceDto();
		response.setAfterBalance(rs.getDouble("afterBalance"));
		response.setPartnerSettlCcy(rs.getString("partnerSettlCcy"));
		

		return response;
	}

}
