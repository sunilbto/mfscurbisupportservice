package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.AllTransactionResponseDto;

public class AllTransactionMapper implements RowMapper<AllTransactionResponseDto> {

	
	public AllTransactionResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
	
		AllTransactionResponseDto response = new AllTransactionResponseDto();
		response.setMfsId(rs.getString("mfsId"));
		response.setDirection(rs.getString("partnerDirection"));
		response.setProductId(rs.getInt("producId"));
		response.setProduct(rs.getString("productName"));
		response.setStatus(rs.getString("status"));
		response.setTransaction_detail_id(rs.getInt("transaction_detail_id"));
		response.setValueDate(rs.getDate("valueDate"));
		response.setDestinationType(rs.getString("destinationType"));
		response.setReceiveCountry(rs.getString("receiveCountry"));
		response.setThirdPartyTransactionId(rs.getString("thirdPartyTransactionId"));

		return response;
	}

}
