package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmTransactionDetailsResponseDTO;

public class TransactionDetailsMapper implements RowMapper<MdmTransactionDetailsResponseDTO> {

	
	public MdmTransactionDetailsResponseDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		MdmTransactionDetailsResponseDTO response = new MdmTransactionDetailsResponseDTO();
		response.setMdmTansactionId(rs.getInt("id"));
		response.setRpTransactionId(rs.getString("rpTransactionId"));
		response.setSpTransactionId(rs.getString("spTransactionId"));
		response.setStatusCode(rs.getString("statusCode"));
		response.setStatusMessage(rs.getString("statusMessage"));
		response.setTransactionCreatedDate(rs.getTimestamp("transaction_created_date"));
		response.setTransactionProcessedDate(rs.getTimestamp("transaction_processed_date"));
		response.setTransactionRef(rs.getString("transaction_ref"));
		response.setTransactionStatus(rs.getString("transaction_status"));
		response.setTransMfsId(rs.getString("trans_mfs_id"));

		return response;
	}

}
