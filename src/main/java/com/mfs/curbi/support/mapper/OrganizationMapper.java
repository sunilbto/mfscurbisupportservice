package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.GetOrganizationResponseDto;

public class OrganizationMapper implements RowMapper<GetOrganizationResponseDto> {

	
	public GetOrganizationResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		GetOrganizationResponseDto response = new GetOrganizationResponseDto();
		response.setOrgId(rs.getInt("id"));
		response.setOrganizatioName(rs.getString("partner_name"));
		response.setPartnerCode(rs.getString("partner_code"));

		return response;
	}

}
