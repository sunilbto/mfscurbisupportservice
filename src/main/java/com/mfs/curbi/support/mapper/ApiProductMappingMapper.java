package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.ApiProductMappingResponseDto;

public class ApiProductMappingMapper implements RowMapper<ApiProductMappingResponseDto> {

	
	public ApiProductMappingResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ApiProductMappingResponseDto response = new ApiProductMappingResponseDto();
		
		response.setAmountType(rs.getString("amount_type"));
		response.setApiId(rs.getInt("api_id"));
		response.setApiProductMappingId(rs.getInt("id"));
		response.setDestionationType(rs.getString("destination_type"));
		response.setProductId(rs.getInt("product_id"));

		return response;
	}

}
