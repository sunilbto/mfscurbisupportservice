package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.AllTransactionByIdResponseDto;

public class AllTransactionByIdMapper implements RowMapper<AllTransactionByIdResponseDto> {

	
	public AllTransactionByIdResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AllTransactionByIdResponseDto response = new AllTransactionByIdResponseDto();

		response.setProductId(rs.getInt("producId"));
		response.setProductName(rs.getString("productName"));
		response.setCreationDate(rs.getDate("creationDate"));
		response.setProcessedDate(rs.getDate("processedDate"));
		response.setTransactionStatus(rs.getString("transactionStatus"));
		response.setMfsId(rs.getString("trans_mfs_id"));
		response.setPartnerDirection(rs.getString("partner_direction"));
		response.setPartnerDirectionR(rs.getString("partner_direction"));
		response.setSendPartner(rs.getString("sendPartner"));
		response.setReceiverPartner(rs.getString("receiveParner"));
		response.setSendCurrency(rs.getString("sendCurrency"));
		response.setSendCountry(rs.getString("sendCountry"));
		response.setSendAmount(rs.getDouble("sendAmount"));
		response.setSendFee(rs.getDouble("sendFee"));
		response.setReceiveCurrency(rs.getString("receiveCurrency"));
		response.setReceiveCountry(rs.getString("recieveCountry"));
		response.setReceiveAmount(rs.getDouble("receiveAmount"));
		response.setReceiveFee(rs.getDouble("receiveFee"));

		response.setValueDate(rs.getDate("valueDate"));
		response.setBalanceBefore(rs.getDouble("balanceBefore"));
		response.setPrincipal(rs.getDouble("principal"));
		response.setCommission(rs.getDouble("commission"));
		response.setFeeRevShare(rs.getDouble("feeRevShare"));
		response.setForexRevShare(rs.getDouble("forexRevShare"));
		response.setBalanceAfter(rs.getDouble("balanceAfter"));
		response.setMovementType(rs.getString("movementType"));
		response.setSettlmentCurrency(rs.getString("settlmentCurrency"));
		response.setTdid(rs.getInt("tdid"));
		response.setTpsdid(rs.getInt("tpsdid"));
		response.setTprdid(rs.getInt("tprdid"));
		response.setTnlid(rs.getInt("tnlid"));
		response.setsPartner_id(rs.getInt("sPartnerId"));
		response.setrPartner_id(rs.getInt("rPsrtnerId"));
		response.setFxRate(rs.getDouble("fxRate"));
		response.setDestinationType(rs.getString("destinationType"));
		return response;
	}

}
