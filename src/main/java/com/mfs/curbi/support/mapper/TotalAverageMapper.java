package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TotalAverage;

public class TotalAverageMapper  implements   RowMapper<TotalAverage>   {

	public TotalAverage mapRow(ResultSet rs, int rowNum) throws SQLException {
		TotalAverage totalAverage=new TotalAverage();
		totalAverage.setTotalAvgSendFrom(rs.getDouble("totalAvg"));
		return totalAverage;
	}

}
