package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TransactionStatusRsponseDto;

public class TransactionStatusMapper implements RowMapper<TransactionStatusRsponseDto> {

	
	public TransactionStatusRsponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		TransactionStatusRsponseDto response = new TransactionStatusRsponseDto();
		response.setTransactionStatus(rs.getString("transaction_status"));
		return response;
	}

}
