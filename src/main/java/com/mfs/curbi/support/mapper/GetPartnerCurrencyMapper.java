package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.GetPartnerCurrency;

public class GetPartnerCurrencyMapper implements RowMapper<GetPartnerCurrency> {

	public GetPartnerCurrency mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		GetPartnerCurrency response=new GetPartnerCurrency(); 
		response.setPartnerCurrency(rs.getString("currecny"));
		
		return response;
	}

}
