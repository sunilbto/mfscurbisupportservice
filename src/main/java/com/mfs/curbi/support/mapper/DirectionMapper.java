package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.MdmDirectionResponseDto;

public class DirectionMapper implements RowMapper<MdmDirectionResponseDto> {

	public MdmDirectionResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		
		MdmDirectionResponseDto response=new MdmDirectionResponseDto();
		response.setMdmDirctionId(rs.getInt("id"));
		response.setDirectionName(rs.getString("direction_name"));
		return response;
	}

}
