package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.TransactionCountPerPartnerDto;

public class TransactionCountPerPartnerMapper implements RowMapper<TransactionCountPerPartnerDto> {

	public TransactionCountPerPartnerDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		TransactionCountPerPartnerDto TransactionCountPerPartnerDto=new TransactionCountPerPartnerDto();
		TransactionCountPerPartnerDto.setParterName(rs.getString("partnerName"));
		TransactionCountPerPartnerDto.setTransCount(rs.getInt("Trans_count"));
		TransactionCountPerPartnerDto.setTransDate(rs.getString("transactionDate"));
		return TransactionCountPerPartnerDto;
	}

}
