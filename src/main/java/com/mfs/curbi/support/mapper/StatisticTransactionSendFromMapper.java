package com.mfs.curbi.support.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mfs.curbi.support.dto.StatisticTransactionSendFromResponseDto;

public class StatisticTransactionSendFromMapper implements RowMapper<StatisticTransactionSendFromResponseDto> {

	public StatisticTransactionSendFromResponseDto mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StatisticTransactionSendFromResponseDto response=new StatisticTransactionSendFromResponseDto();
	//	response.setCount(rs.getInt("count"));
		response.setAvgAmount(rs.getDouble("avg_amount"));
		response.setProcessedDate(rs.getString("transactionDate"));
		response.setDate(rs.getString("processedDate"));
		
		return response;
	}

}
