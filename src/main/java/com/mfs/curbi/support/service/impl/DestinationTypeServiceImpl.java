package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.DestinationTypeDao;
import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.service.DestinationTypeService;

@Service
public class DestinationTypeServiceImpl implements DestinationTypeService {

	@Autowired
	DestinationTypeDao destinationTypeDao;

	/*
     * Method-getDestinationTypeList returns destination type list  
     */
	public List<MdmDestinationTypeResponseDto> getDestinationTypeList()  {
		List<MdmDestinationTypeResponseDto> response= destinationTypeDao.getDestinationTypeList();
		return response;
	}

}
