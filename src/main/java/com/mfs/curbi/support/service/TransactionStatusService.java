package com.mfs.curbi.support.service;

import java.util.List;

import com.mfs.curbi.support.dto.TransactionStatusRsponseDto;

public interface TransactionStatusService {
	public List<TransactionStatusRsponseDto> getTransactionStatus() throws Exception;

}
