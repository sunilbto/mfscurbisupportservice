package com.mfs.curbi.support.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dto.AllBalanceRequestDto;
import com.mfs.curbi.support.dto.AllDashBoardRequestDto;
import com.mfs.curbi.support.dto.AllDashBoardResponse;
import com.mfs.curbi.support.dto.AllTransactionDetailsResponseDto;
import com.mfs.curbi.support.dto.AllTransactionRequestDto;
import com.mfs.curbi.support.dto.BalanceDetailResponseDto;
import com.mfs.curbi.support.dto.BalanceExcelDto;
import com.mfs.curbi.support.dto.ReportExcelDto;
import com.mfs.curbi.support.dto.TransactionExcelDto;
import com.mfs.curbi.support.service.BalanceService;
import com.mfs.curbi.support.service.DashboardService;
import com.mfs.curbi.support.service.ExportService;
import com.mfs.curbi.support.service.TransactionDetailService;

@Service("ExportServiceImpl")
public class ExportServiceImpl implements ExportService {

	@Autowired
	TransactionDetailService transService;

	@Autowired
	BalanceService balanceService;

	@Autowired
	DashboardService dashboardService;

	private static String[] columns = { "Value Date", "MFS ID", "Third Party Transaction Id", "Status", "Product",
			"Direction" };

	//export transaction in excel
	public TransactionExcelDto exportTransactionExcelService(AllTransactionRequestDto request) throws Exception {

		TransactionExcelDto res = new TransactionExcelDto();
		String pattern = "EE hh:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);
		Date today = Calendar.getInstance().getTime();

		String fileName = "Trasnactionfile " + df.format(today) + ".xlsx";
		// TransactionAllResponseDto response; // =
		// transService.getAllTransaction(request.getPartnerCode(),request.getStartDate(),request.getEndDate(),request.getStatus(),request.getProduct(),request.getDirection(),request.getDestinationType(),request.getReceiveCountry());

		List<AllTransactionDetailsResponseDto> allTransactionResponse = new ArrayList<AllTransactionDetailsResponseDto>(); // =response.getAllTransactionResponse();

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Transaction");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setBorderBottom(BorderStyle.DOUBLE);
		headerCellStyle.setBorderLeft(BorderStyle.DOUBLE);
		headerCellStyle.setBorderRight(BorderStyle.DOUBLE);
		headerCellStyle.setBorderTop(BorderStyle.DOUBLE);

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 1;
		for (AllTransactionDetailsResponseDto AllTransaction : allTransactionResponse) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(AllTransaction.getValueDate());
			row.createCell(1).setCellValue(AllTransaction.getMfsId());
			row.createCell(2).setCellValue(AllTransaction.getThirdPartyTransactionId());
			row.createCell(3).setCellValue(AllTransaction.getStatus());
			row.createCell(4).setCellValue(AllTransaction.getProduct());
			row.createCell(5).setCellValue(AllTransaction.getDirection());
		}
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}
		FileOutputStream fileOut = new FileOutputStream("GetAlltarsnactionfile.xlsx");
		workbook.write(fileOut);
		fileOut.close();
		String home = System.getProperty("user.home");
		String mExportPath = home + "/Downloads/";
		File file = new File(mExportPath, fileName);
		FileOutputStream fos = new FileOutputStream(file);
		workbook.write(fos);
		fos.flush();
		fos.close();
		workbook.close();

		res.setResponseMessage("Excel Save Succesfully");

		return res;
	}

	
	// define string array for transaction and balance table columns
	private static String[] column = { "Value Date", "Before Bal", "Principal", "Commision", "Fee Share", "Fx Share",
			"After Bal", "Ccy", "Type", "Mfs ID" };
	

	public BalanceExcelDto exportBalanceExcelService(AllBalanceRequestDto request) throws Exception {

		BalanceExcelDto res = new BalanceExcelDto();
		String pattern = "EE hh:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);
		Date today = Calendar.getInstance().getTime();
		// String todayAsString = df.format(today);

		String fileName = "Balancefile " + df.format(today) + ".xlsx";

		// BalanceResponseDto response =
		// balanceService.getAllBalance(request.getPartnerCode(),request.getStartDate(),request.getEndDate(),request.getStatus(),request.getProduct(),request.getDirection(),request.getDestinationType(),request.getReceiveCountry());
		List<BalanceDetailResponseDto> balanceDetailList = new ArrayList<BalanceDetailResponseDto>();// =response.getBalanceDetail();

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Balance");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setBorderBottom(BorderStyle.DOUBLE);
		headerCellStyle.setBorderLeft(BorderStyle.DOUBLE);
		headerCellStyle.setBorderRight(BorderStyle.DOUBLE);
		headerCellStyle.setBorderTop(BorderStyle.DOUBLE);

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < column.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(column[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 1;
		for (BalanceDetailResponseDto balanceDetail : balanceDetailList) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(balanceDetail.getValueDate());
			row.createCell(1).setCellValue(balanceDetail.getBeforeBalance());
			row.createCell(2).setCellValue(balanceDetail.getPrinciple());
			row.createCell(3).setCellValue(balanceDetail.getCommission());
			row.createCell(4).setCellValue(balanceDetail.getFeeshare());
			row.createCell(5).setCellValue(balanceDetail.getFxShare());
			row.createCell(6).setCellValue(balanceDetail.getAfterBalance());
			row.createCell(7).setCellValue(balanceDetail.getCcy());
			row.createCell(8).setCellValue(balanceDetail.getType());
			row.createCell(9).setCellValue(balanceDetail.getMfsId());
		}

		for (int i = 0; i < column.length; i++) {
			sheet.autoSizeColumn(i);
		}

		String home = System.getProperty("user.home");
		String mExportPath = home + "/Downloads/";
		File file = new File(mExportPath, fileName);
		FileOutputStream fos = new FileOutputStream(file);
		workbook.write(fos);
		fos.flush();
		fos.close();
		workbook.close();

		res.setResponseMessage("saved succefully");		
		return res;
	}

	// define string array for dashborad table columns
	private static String[] reportColumn = { "From Date", "To Date", "Ccy", "Value", "Volume", "Commission",
			"Fee share", "Fx share" };

	public ReportExcelDto exportReportExcelService(AllDashBoardRequestDto request) throws Exception {

		ReportExcelDto res = new ReportExcelDto();
		String pattern = "EE hh:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);
		Date today = Calendar.getInstance().getTime();
		// String todayAsString = df.format(today);

		String fileName = "Reportfile " + df.format(today) + ".xlsx";

		AllDashBoardResponse response = dashboardService.getDashBoardInfo(request.getPartnerCode(),
				request.getStartDate(), request.getEndDate(), request.getStatus(), request.getProduct(),
				request.getDirection(), request.getDestinationType(), request.getReceiveCountry());

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Report");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setBorderBottom(BorderStyle.DOUBLE);
		headerCellStyle.setBorderLeft(BorderStyle.DOUBLE);
		headerCellStyle.setBorderRight(BorderStyle.DOUBLE);
		headerCellStyle.setBorderTop(BorderStyle.DOUBLE);

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < reportColumn.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(reportColumn[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 1;
		Row row = sheet.createRow(rowNum++);
		row.createCell(0).setCellValue(request.getStartDate());
		row.createCell(1).setCellValue(request.getEndDate());
		row.createCell(2).setCellValue(response.getCcy());
		row.createCell(3).setCellValue(response.getFinalAmount());
		row.createCell(4).setCellValue(response.getFinalTransactionCount());
		row.createCell(5).setCellValue(response.getCommission());
		row.createCell(6).setCellValue(response.getFeeshare());
		row.createCell(7).setCellValue(response.getFxShare());

		for (int i = 0; i < reportColumn.length; i++) {
			sheet.autoSizeColumn(i);
		}

		String home = System.getProperty("user.home");
		String mExportPath = home + "/Downloads/";
		File file = new File(mExportPath, fileName);
		FileOutputStream fos = new FileOutputStream(file);
		workbook.write(fos);
		fos.flush();
		fos.close();
		workbook.close();

		res.setResponseMessage("saved succefully");

		return res;
	}

}
