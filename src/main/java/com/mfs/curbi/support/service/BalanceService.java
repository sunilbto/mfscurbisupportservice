package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.BalanceResponseDto;
import com.mfs.curbi.support.dto.TransactionByIdResponseDto;

public interface BalanceService {
	
	
	public BalanceResponseDto getAllBalance(long partnerCode,String startDate, String endDate,String status,String product,String direction,String destinationType,String receiveCountry,String flag)throws Exception;
	
	public TransactionByIdResponseDto getAllBalanceById(int transactionId)throws Exception;
	
	

}
