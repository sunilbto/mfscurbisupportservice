package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.MoneyFlowDto;
import com.mfs.curbi.support.dto.MoneyFlowResponseDto;

public interface MoneyFlowService {

	public MoneyFlowResponseDto getMoneyFlows(MoneyFlowDto moneyFlowDto)throws Exception;
}
