package com.mfs.curbi.support.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.ProductDao;
import com.mfs.curbi.support.dao.TransactionDetailsDao;
import com.mfs.curbi.support.dto.AllTransactionByIdResponseDto;
import com.mfs.curbi.support.dto.AllTransactionResponseDto;
import com.mfs.curbi.support.dto.TransactionAllResponseDto;
import com.mfs.curbi.support.dto.TransactionByIdResponseDto;
import com.mfs.curbi.support.dto.TransactionDetailByIdResponseDto;
import com.mfs.curbi.support.dto.TransactionDetailResponseDto;
import com.mfs.curbi.support.service.TransactionDetailService;
import com.mfs.curbi.support.util.Constant;
import com.mfs.curbi.support.util.ConvertDateToString;

@Service
public class TransactionDetailServiceImpl implements TransactionDetailService {

	@Autowired
	TransactionDetailsDao transactionDetailsDao;

	@Autowired
	ProductDao productDao;

	private static final Logger log = LoggerFactory.getLogger(TransactionDetailServiceImpl.class);

	/*
	 * Method-getAllTransaction method returns transaction details
	 */
	public TransactionAllResponseDto getAllTransaction(long partnerCode, String startDate, String endDate,
			String status, String product, String direction, String destinationType, String receiveCountry, String flag)
			throws Exception {

		Date sd = null;
		Date ed = null;
		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT).parse(startDate);
			ed = new SimpleDateFormat(Constant.DATE_FORMAT).parse(endDate);
		} catch (ParseException e) {
			log.error("Inside getAllTransaction() method of TransactionDetailServiceImpl----" + e);
			throw new HibernateException(
					"Incorrect Date Format//	log.info(\"Inside getCurrentBalanace() method of TransactionDetailsDaoImpl\");e Error during parsing-----"
							+ e);
		}

		// convert date into timeStamp
		Timestamp startdate = new Timestamp(sd.getTime());
		Timestamp enddate = new Timestamp(ed.getTime());

		TransactionAllResponseDto response = null;

		// returns transaction details list
		List<AllTransactionResponseDto> transactionList = transactionDetailsDao.getAllTransactionDao(partnerCode,
				startdate, enddate, status, product, direction, destinationType, receiveCountry);

		// check transaction list is empty or not
		if (!(transactionList.isEmpty())) {

			if (flag != null ? flag.equals(Constant.DOWNLOAD_CODE) : false) {
				response = new TransactionAllResponseDto();
				response.setAllTransactionResponse(transactionList);

			} else if (transactionList.size() <= 10000) {
				response = new TransactionAllResponseDto();
				response.setAllTransactionResponse(transactionList);
			} else if (transactionList.size() <= 50000) {
				response = new TransactionAllResponseDto();
				response.setResponseMessage("Transaction is more limit");
				response.setResponseCode(Constant.DOWNLOAD_CODE);

			} else if (transactionList.size() > 50000) {
				response = new TransactionAllResponseDto();
				response.setResponseMessage("Change date range");
				response.setResponseCode(Constant.CHANGE_DATE_RANGE_CODE);
			}
		}

		return response;

	}

	/*
	 * Method-getTransactionById method returns transaction details by transaction
	 * id
	 */
	public TransactionByIdResponseDto getTransactionById(int transactionId) throws Exception {

		TransactionByIdResponseDto response = null;

		// Returns transaction details list by transaction id
		List<AllTransactionByIdResponseDto> transactionByIdList = transactionDetailsDao
				.getTransactionDetailsById(transactionId);

		// checking transactionByIdList is not empty
		if (!(transactionByIdList.isEmpty())) {
			response = new TransactionByIdResponseDto();
			response.setTransactionDetail(transactionByIdList);

		}

		return response;
	}

	public TransactionDetailResponseDto getTransactionPerDatePerPartner(long partnerCode, String dateStr)
			throws Exception {

		Date sd = null;
		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT_YYYY_MM_DD).parse(dateStr);
		} catch (ParseException e) {
			log.error("Inside getAllTransaction() method of TransactionDetailServiceImpl----" + e);
			throw new HibernateException("Incorrect Date Formate Error during parsing-----" + e);
		}

		// convert date into timeStamp
		Timestamp startdate = new Timestamp(sd.getTime());

		TransactionDetailResponseDto response = new TransactionDetailResponseDto();

		List<AllTransactionByIdResponseDto> transactionList = transactionDetailsDao.getTransactionPerDate(partnerCode,
				startdate);

		List<TransactionDetailByIdResponseDto> transactionDetail = new ArrayList<TransactionDetailByIdResponseDto>();

		// check transaction list is empty or not
		if (transactionList.isEmpty()) {
			// log.info("No reccord found");
			response.setResponseCode(Constant.INFO_CODE);
			response.setResponseMessage("No record found");

		} else {
			response.setResponseCode(Constant.SUCCESS_CODE);
			response.setResponseMessage("All Transactions for Given Date");

			for (AllTransactionByIdResponseDto transactionById : transactionList) {
				TransactionDetailByIdResponseDto row = new TransactionDetailByIdResponseDto();
				row.setMfsId(transactionById.getMfsId());
				row.setThirdPartyTransactionId(transactionById.getTprdid());
				row.setStatus(transactionById.getTransactionStatus());
				row.setCreationDate(ConvertDateToString.convertDateToString(transactionById.getCreationDate()));
				row.setProccessDate(ConvertDateToString.convertDateToString(transactionById.getProcessedDate()));
				row.setValueDate(ConvertDateToString.convertDateToString(transactionById.getValueDate()));
				row.setProductName(transactionById.getProductName());
				row.setDestinationAccountType(transactionById.getDestinationType());
				row.setDirection(transactionById.getPartnerDirection());
				row.setReceiveCountry(transactionById.getReceiveCountry());
				row.setReceivePartner(transactionById.getReceiverPartner());
				row.setSendAmount(transactionById.getSendAmount());
				row.setSendFee(transactionById.getSendFee());
				row.setSendCurrency(transactionById.getSendCurrency());
				row.setReceiveAmount(transactionById.getReceiveAmount());
				row.setReceiveCurrency(transactionById.getReceiveCurrency());
				row.setFxRate(transactionById.getFxRate());
				row.setReceiveFee(transactionById.getReceiveFee());
				row.setBalanceBefore(transactionById.getBalanceBefore());
				row.setPrincipal(transactionById.getPrincipal());
				row.setCommission(transactionById.getCommission());
				row.setFeeRevShare(transactionById.getFeeRevShare());
				row.setForexRevShare(transactionById.getForexRevShare());
				row.setBalanceAfter(transactionById.getBalanceAfter());
				row.setSettlementCurrency(transactionById.getSettlmentCurrency());
				transactionDetail.add(row);
			}
		}
		response.setData(transactionDetail);
		return response;
	}

}
