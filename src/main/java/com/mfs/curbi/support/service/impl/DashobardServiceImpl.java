package com.mfs.curbi.support.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.DashboardDao;
import com.mfs.curbi.support.dto.AllDashBoardResponse;
import com.mfs.curbi.support.service.DashboardService;
import com.mfs.curbi.support.util.Constant;

@Service
public class DashobardServiceImpl implements DashboardService {

	@Autowired
	DashboardDao dao;

	private static final Logger log = LoggerFactory.getLogger(DashobardServiceImpl.class);

	/*
     * Method-getDashBoardInfo returns dashboard details  
     */
	public AllDashBoardResponse getDashBoardInfo(long partnerCode, String startDate, String endDate,String status,String product,String direction,String destinationType,String receiveCountry) throws Exception  {


		AllDashBoardResponse response=null;
		Date sd=null;
		Date ed=null;
		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT).parse(startDate);
		    ed = new SimpleDateFormat(Constant.DATE_FORMAT).parse(endDate);
		} catch (ParseException e) {
			log.error("Inside getDashBoardInfo() method of DashobardServiceImpl----"+e);
			throw new HibernateException("Incorrect Date Formate Error during parsing-----"+e);
		}
		
		 //convert date into timeStamp
		Timestamp startdate = new Timestamp(sd.getTime());
		Timestamp enddate = new Timestamp(ed.getTime());

		//Returns dashboard info
		 response = dao.getDashBoardInfo(partnerCode, startdate, enddate,status,product,direction,destinationType,receiveCountry);

		return response;
	}

}
