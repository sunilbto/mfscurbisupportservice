package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.ProductDao;
import com.mfs.curbi.support.dto.MdmProductResponseDto;
import com.mfs.curbi.support.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao dao;

	/*
	 * Method-getAllProduct returns product
	 */
	public List<MdmProductResponseDto> getAllProduct() throws Exception {
		List<MdmProductResponseDto> response = dao.getAllProduct();
		return response;
	}

}
