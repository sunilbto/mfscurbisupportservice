package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.CountryDao;
import com.mfs.curbi.support.dto.MdmCountryResponseDto;
import com.mfs.curbi.support.service.Countryservice;

@Service
public class CountryServiceImpl implements Countryservice {

	@Autowired
	CountryDao dao;

    /*
     * Method-getAllCountries returns all countries  
     */
	public List<MdmCountryResponseDto> getAllCountries() throws Exception {
		List<MdmCountryResponseDto> countryList = dao.getAllCountries();
		return countryList;
	}

}
