package com.mfs.curbi.support.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.mfs.curbi.support.service.CommonServices;

@Service
public class CommonServicesImpl implements CommonServices {

	
	// convert into comma separated string
	public String convertString(String countryName) {

		Set<String> set =new HashSet<String>( Arrays.asList(countryName));
		String citiesCommaSeparated = String.join("' , '", set);
		
			return citiesCommaSeparated;
	}

}
