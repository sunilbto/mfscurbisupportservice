package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.ComplianceFinalResponseDto;

public interface ComplianceDetailsService {

	public ComplianceFinalResponseDto getComplianceDetailsService(String countryName,String startDate,String endDate)throws Exception;
}
