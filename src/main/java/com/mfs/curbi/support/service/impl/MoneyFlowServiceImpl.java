package com.mfs.curbi.support.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.MoneyFlowDao;
import com.mfs.curbi.support.dto.MoneyFlowDto;
import com.mfs.curbi.support.dto.MoneyFlowResponseDto;
import com.mfs.curbi.support.dto.RecieveCountry;
import com.mfs.curbi.support.dto.RecieveTransactionResponse;
import com.mfs.curbi.support.dto.SendFromCountry;
import com.mfs.curbi.support.dto.SendTotalTransaction;
import com.mfs.curbi.support.dto.SendTransactionResponse;
import com.mfs.curbi.support.dto.TotalRecieveTransaction;
import com.mfs.curbi.support.dto.TotalSendTransaction;
import com.mfs.curbi.support.dto.TransAmountAsPerPartner;
import com.mfs.curbi.support.dto.TrasnactionId;
import com.mfs.curbi.support.service.CommonServices;
import com.mfs.curbi.support.service.MoneyFlowService;
import com.mfs.curbi.support.util.Constant;

@Service
public class MoneyFlowServiceImpl implements MoneyFlowService {

	private static final Logger log = LoggerFactory.getLogger(MoneyFlowServiceImpl.class);

	@Autowired
	MoneyFlowDao moneyFlowDao;
	@Autowired
	CommonServices commonService;

	/*
	 * Method-getMoneyFlows returns money flow Details
	 */
	public MoneyFlowResponseDto getMoneyFlows(MoneyFlowDto moneyFlowDto) throws Exception {

		Date sd = null;
		Date ed = null;

		TotalSendTransaction totalSendTransaction = null;
		String senderPartner = null;
		String partnerName = "Total";
		List<SendTransactionResponse> sendFromCountrieList = new ArrayList<SendTransactionResponse>();
		SendTransactionResponse sendTransactionResponse = null;
		List<TotalSendTransaction> totalTransactionList = new ArrayList<TotalSendTransaction>();
		MoneyFlowResponseDto moneyFlowResponseDto = new MoneyFlowResponseDto();
		TotalRecieveTransaction totalRecieveTransaction = null;
		List<TotalRecieveTransaction> totalRecieveTransactionList = new ArrayList<TotalRecieveTransaction>();
		RecieveTransactionResponse recieveTransactionResponse = null;
		List<RecieveTransactionResponse> recieveTransactionResponseList = new ArrayList<RecieveTransactionResponse>();

		List<TransAmountAsPerPartner> transAmountAsPerPartnersList = new ArrayList<TransAmountAsPerPartner>();
		TransAmountAsPerPartner transAmountAsPerPartner = null;

		// double amountValue = 0;

		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT).parse(moneyFlowDto.getStartDate());
			ed = new SimpleDateFormat(Constant.DATE_FORMAT).parse(moneyFlowDto.getEndDate());
		} catch (ParseException e) {
			log.error("Inside getMoneyFlows() method of MoneyFlowServiceImpl----" + e);
			throw new HibernateException("Incorrect Date Formate Error during parsing-----" + e.getMessage());
		}

		// define string array for transaction table columns
		Timestamp startDate = new Timestamp(sd.getTime());
		Timestamp endDate = new Timestamp(ed.getTime());

		// convert string into comma separated string for country names
		String countryName = commonService.convertString(moneyFlowDto.getCountryNames());

		String commaSeparatedCountries[] = countryName.split(",");
		String cntryNames = "";

		for (int i = 0; i < commaSeparatedCountries.length; i++) {
			cntryNames += "'" + commaSeparatedCountries[i] + "',";
		}
		cntryNames = cntryNames.substring(0, cntryNames.length() - 1);

		moneyFlowDto.setCountryNames(cntryNames);

		// Checking partner direction
		if (moneyFlowDto.getType().equalsIgnoreCase("Send")) {

			// Returns transaction id
			List<TrasnactionId> transactionId = moneyFlowDao.getTransactions(moneyFlowDto, startDate, endDate);
			String commSeparatedTransaction = null;

			// Checking transaction id empty or not empty
			if (!(transactionId.isEmpty())) {
				commSeparatedTransaction = transactionId.toString().replace('[', ' ').replace(']', ' ').trim();
			} else {
				commSeparatedTransaction = "' '";
			}

			List<SendTotalTransaction> recieveTransactions = moneyFlowDao.getTotalRecieveTransaction(moneyFlowDto,
					startDate, endDate, commSeparatedTransaction);

			// Checking recieveTransactions empty or not empty
			if (!(recieveTransactions.isEmpty())) {
				double amount = 0;
				double amountSpac = 0;
				int transCount = recieveTransactions.size();

				for (SendTotalTransaction recieveTransaction : recieveTransactions) {
					totalRecieveTransaction = new TotalRecieveTransaction();
					amount = amount + recieveTransaction.getAmount();
					amountSpac = amountSpac + recieveTransaction.getSpcAmount();

				}

				totalRecieveTransaction.setPartnerName(partnerName);
				totalRecieveTransaction.setAvgTxnValue(amount / transCount);
				totalRecieveTransaction.setPartnerCurrency(amount);
				totalRecieveTransaction.setSettlementCurrency(amountSpac);
				totalRecieveTransaction.setTxnValue(transCount);
				totalRecieveTransactionList.add(totalRecieveTransaction);
				moneyFlowResponseDto.setTotalRecieveTransactionList(totalRecieveTransactionList);
			}

			
			// Returns receive countries
			List<RecieveCountry> recieveCountries = moneyFlowDao.getRecieveFromPartner(commSeparatedTransaction);

			// Checking receive countries empty or not empty
			if (!recieveCountries.isEmpty()) {

				for (RecieveCountry recieveCountry : recieveCountries) {
					recieveTransactionResponse = new RecieveTransactionResponse();

					recieveTransactionResponse.setAvgTxnValue(recieveCountry.getTotalAmount());
					recieveTransactionResponse.setCountry(recieveCountry.getPartnerCountry());

					recieveTransactionResponse.setPartnerCurrency(recieveCountry.getAmount());
					recieveTransactionResponse.setSettlementCurrency(recieveCountry.getSpcAmount());
					recieveTransactionResponse.setTxnValue(recieveCountry.getTxnCount());
					recieveTransactionResponseList.add(recieveTransactionResponse);
				}
				moneyFlowResponseDto.setRecieveTransactionResponse(recieveTransactionResponseList);

			}

			// Returns list of amount as per partner
			List<TransAmountAsPerPartner> transAmountAsPerPartners = moneyFlowDao.transAmountAsPerPartners(moneyFlowDto,
					startDate, endDate, senderPartner);

			double amounts = 0;
			int transCounts = 0;
			double amountSpac = 0;

			// Checking transAmountAsPerPartners empty or not empty
			if (!(transAmountAsPerPartners.isEmpty())) {
				moneyFlowResponseDto.setCurrecny(transAmountAsPerPartners.get(0).getCurrencyCode());
				for (TransAmountAsPerPartner transAmountAsPerPartner2 : transAmountAsPerPartners) {
					transAmountAsPerPartner = new TransAmountAsPerPartner();

					transCounts = transCounts + transAmountAsPerPartner2.getTransCount();
					amounts = amounts + transAmountAsPerPartner2.getTransAmount();
					amountSpac = amountSpac + transAmountAsPerPartner2.getSpcAmount();
					transAmountAsPerPartner.setTransAmount(transAmountAsPerPartner2.getTransAmount());
					transAmountAsPerPartner.setTransCount(transAmountAsPerPartner2.getTransCount());
					transAmountAsPerPartner.setTransDate(transAmountAsPerPartner2.getTransDate());
					transAmountAsPerPartnersList.add(transAmountAsPerPartner);

				}
				totalSendTransaction = new TotalSendTransaction();
				totalSendTransaction.setPartnerName(partnerName);
				totalSendTransaction.setPartnerCurrency(amounts);
				totalSendTransaction.setSettlementCurrency(amountSpac);
				totalSendTransaction.setTxnValue(transCounts);
				moneyFlowResponseDto.setTotalTransactionAmount(amounts);
				totalSendTransaction.setAvgTxnValue(amounts / transCounts);
				totalTransactionList.add(totalSendTransaction);
				moneyFlowResponseDto.setTotalSendTransactionList(totalTransactionList);
				moneyFlowResponseDto.setTotalTransactionCount(transCounts);

				moneyFlowResponseDto.setTransAmountAsPerPartner(transAmountAsPerPartnersList);

			}
		}

		// Checking partner direction
		else if (moneyFlowDto.getType().equalsIgnoreCase("Receive")) {

			// Returns transaction id
			List<TrasnactionId> transactionId = moneyFlowDao.getTransactions(moneyFlowDto, startDate, endDate);
			String commSeparatedTransaction = null;

			// Checking transaction id empty or not empty
			if (!(transactionId.isEmpty())) {
				commSeparatedTransaction = transactionId.toString().replace('[', ' ').replace(']', ' ').trim();
			} else {
				commSeparatedTransaction = "' '";
			}

			List<SendTotalTransaction> recieveTransactions = moneyFlowDao.getTotalRecieveTransaction(moneyFlowDto,
					startDate, endDate, commSeparatedTransaction);

			// check recieveTransactions is empty or not
			if (!(recieveTransactions.isEmpty())) {
				double amount = 0;
				double amountSpac = 0;
				int transCount = recieveTransactions.size();

				for (SendTotalTransaction recieveTransaction : recieveTransactions) {
					totalRecieveTransaction = new TotalRecieveTransaction();
					amount = amount + recieveTransaction.getAmount();
					amountSpac = amountSpac + recieveTransaction.getSpcAmount();

				}

				totalRecieveTransaction.setPartnerName(partnerName);
				totalRecieveTransaction.setAvgTxnValue(amount / transCount);
				totalRecieveTransaction.setPartnerCurrency(amount);
				totalRecieveTransaction.setSettlementCurrency(amountSpac);
				totalRecieveTransaction.setTxnValue(transCount);
				totalRecieveTransactionList.add(totalRecieveTransaction);
				moneyFlowResponseDto.setTotalRecieveTransactionList(totalRecieveTransactionList);
			}
			
			// Returns send from country list
			List<SendFromCountry> sendFromCountries = moneyFlowDao.sendFromCountrie(commSeparatedTransaction);

			// checking send from country list recieveTransactions
			if (!(sendFromCountries.isEmpty())) {

				for (SendFromCountry sendFromCountrys : sendFromCountries) {

					sendTransactionResponse = new SendTransactionResponse();
					sendTransactionResponse.setPartnerCurrency(sendFromCountrys.getAmount());
					sendTransactionResponse.setPartnerCountry(sendFromCountrys.getPartnerCountry());
					sendTransactionResponse.setSettlementCurrency(sendFromCountrys.getSpcAmount());
					sendTransactionResponse.setAvgTxnValue(sendFromCountrys.getTotalAmount());
					sendTransactionResponse.setTxnValue(sendFromCountrys.getTxnCount());
					sendFromCountrieList.add(sendTransactionResponse);
				}

				moneyFlowResponseDto.setSendTransactionResponse(sendFromCountrieList);

			}

			// Returns list of amount as per partner
			List<TransAmountAsPerPartner> transAmountAsPerPartners = moneyFlowDao.transAmountAsPerPartners(moneyFlowDto,
					startDate, endDate, senderPartner);

			double amounts = 0;
			int transCounts = 0;
			double amountSpac = 0;

			// Checking transAmountAsPerPartners list is empty or not
			if (!(transAmountAsPerPartners.isEmpty())) {
				moneyFlowResponseDto.setCurrecny(transAmountAsPerPartners.get(0).getCurrencyCode());
				for (TransAmountAsPerPartner transAmountAsPerPartner2 : transAmountAsPerPartners) {
					transAmountAsPerPartner = new TransAmountAsPerPartner();

					transCounts = transCounts + transAmountAsPerPartner2.getTransCount();
					amounts = amounts + transAmountAsPerPartner2.getTransAmount();
					amountSpac = amountSpac + transAmountAsPerPartner2.getSpcAmount();
					transAmountAsPerPartner.setTransAmount(transAmountAsPerPartner2.getTransAmount());
					transAmountAsPerPartner.setTransCount(transAmountAsPerPartner2.getTransCount());
					transAmountAsPerPartner.setTransDate(transAmountAsPerPartner2.getTransDate());
					transAmountAsPerPartnersList.add(transAmountAsPerPartner);

				}
				totalSendTransaction = new TotalSendTransaction();
				totalSendTransaction.setPartnerName(partnerName);
				totalSendTransaction.setPartnerCurrency(amounts);
				totalSendTransaction.setSettlementCurrency(amountSpac);
				totalSendTransaction.setTxnValue(transCounts);
				moneyFlowResponseDto.setTotalTransactionAmount(amounts);
				totalSendTransaction.setAvgTxnValue(amounts / transCounts);
				totalTransactionList.add(totalSendTransaction);
				moneyFlowResponseDto.setTotalSendTransactionList(totalTransactionList);
				moneyFlowResponseDto.setTotalTransactionCount(transCounts);

				moneyFlowResponseDto.setTransAmountAsPerPartner(transAmountAsPerPartnersList);

			}
		}

		return moneyFlowResponseDto;
	}

}
