package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.TransactionAllResponseDto;
import com.mfs.curbi.support.dto.TransactionByIdResponseDto;
import com.mfs.curbi.support.dto.TransactionDetailResponseDto;

public interface TransactionDetailService {

	public TransactionAllResponseDto getAllTransaction(long partnerCode,String startDate, String endDate,String status,String product,String direction,String destinationType,String receiveCountry,String flag) throws Exception;

	public TransactionByIdResponseDto getTransactionById(int transactionId) throws Exception;

	TransactionDetailResponseDto getTransactionPerDatePerPartner(long partnerCode, String dateStr) throws Exception;

}
