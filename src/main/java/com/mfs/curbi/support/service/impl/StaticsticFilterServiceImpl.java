package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.StaticsticFilterDao;
import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.dto.MdmProductResponseDto;
import com.mfs.curbi.support.dto.StaticsticFilterResponseDto;
import com.mfs.curbi.support.service.StaticsticFilterService;


@Service("StaticsticFilterService")
public class StaticsticFilterServiceImpl implements StaticsticFilterService
{
	@Autowired
	StaticsticFilterDao staticsticFilterDao;

	/*
	 *Method-getStaticsticFilterService returns staticstic filters 
	 */
	public StaticsticFilterResponseDto getStaticsticFilterService() {
		
		StaticsticFilterResponseDto staticsticFilterResponse=new StaticsticFilterResponseDto();
		
		//Returns product list
		List<MdmProductResponseDto> productList=staticsticFilterDao.getProductList();
		
		//Returns destinationtype list
		List<MdmDestinationTypeResponseDto> destinationTypeList=staticsticFilterDao.getDestinationTypeList();
		
		staticsticFilterResponse.setProductList(productList);
		staticsticFilterResponse.setDestinationTypeList(destinationTypeList);
		
		return staticsticFilterResponse;
	}

}
