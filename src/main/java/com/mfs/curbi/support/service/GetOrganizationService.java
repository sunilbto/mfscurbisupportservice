package com.mfs.curbi.support.service;

import java.util.List;

import com.mfs.curbi.support.dto.GetOrganizationResponseDto;

public interface GetOrganizationService {
	
	public List<GetOrganizationResponseDto> getOrganization()throws Exception; 

}
