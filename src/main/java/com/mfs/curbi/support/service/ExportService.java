package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.AllBalanceRequestDto;
import com.mfs.curbi.support.dto.AllDashBoardRequestDto;
import com.mfs.curbi.support.dto.AllTransactionRequestDto;
import com.mfs.curbi.support.dto.BalanceExcelDto;
import com.mfs.curbi.support.dto.ReportExcelDto;
import com.mfs.curbi.support.dto.TransactionExcelDto;

public interface ExportService {
	
	public TransactionExcelDto exportTransactionExcelService(AllTransactionRequestDto request)throws Exception;
	
	public BalanceExcelDto exportBalanceExcelService(AllBalanceRequestDto request)throws Exception;
	
	public ReportExcelDto exportReportExcelService(AllDashBoardRequestDto request)throws Exception;

}
