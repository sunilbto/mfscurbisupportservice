package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.AllDashBoardResponse;

public interface DashboardService {

	public AllDashBoardResponse getDashBoardInfo(long partnerCode, String startDate, String endDate,String status,String product,String direction,String destinationType,String receiveCountry) throws Exception;
}