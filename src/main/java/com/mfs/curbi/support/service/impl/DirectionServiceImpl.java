package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.DirectionDao;
import com.mfs.curbi.support.dto.MdmDirectionResonseDto;
import com.mfs.curbi.support.service.DirectionService;

@Service
public class DirectionServiceImpl implements DirectionService {

	@Autowired
	DirectionDao dao;

	/*
     * Method-getDirectionList returns direction list  
     */
	public List<MdmDirectionResonseDto> getDirectionList() throws Exception {
		List<MdmDirectionResonseDto> response = dao.getDirectionList();
		return response;
	}

}
