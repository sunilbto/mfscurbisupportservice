package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.TransactionStatusDao;
import com.mfs.curbi.support.dto.TransactionStatusRsponseDto;
import com.mfs.curbi.support.service.TransactionStatusService;

@Service
public class TransactionStatusServiceImpl implements TransactionStatusService {
	@Autowired
	TransactionStatusDao dao;

	/*
	 * Method-getTransactionStatus method returns transaction status
	 */
	public List<TransactionStatusRsponseDto> getTransactionStatus() throws Exception {
	
		List<TransactionStatusRsponseDto> response = dao.getTransactionStatus();
		return response;
	}

}
