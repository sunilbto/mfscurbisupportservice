package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.OrganizationDao;
import com.mfs.curbi.support.dto.GetOrganizationResponseDto;
import com.mfs.curbi.support.service.GetOrganizationService;

@Service
public class GetOrganizationServiceImpl implements GetOrganizationService {

	@Autowired
	OrganizationDao organizationDao;

	/*
	 * Method-getOrganization returns organization
	 */
	public List<GetOrganizationResponseDto> getOrganization() throws Exception {
		List<GetOrganizationResponseDto> response = organizationDao.getOrganization();
        return response;
	}

}
