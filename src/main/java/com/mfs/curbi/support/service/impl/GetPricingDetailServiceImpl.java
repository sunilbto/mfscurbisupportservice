package com.mfs.curbi.support.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.PricingDetailDao;
import com.mfs.curbi.support.dto.PricingFinalResponseDto;
import com.mfs.curbi.support.dto.PricingRequestDto;
import com.mfs.curbi.support.dto.PricingResponseDto;
import com.mfs.curbi.support.dto.PricingResponseRowDto;
import com.mfs.curbi.support.service.CommonServices;
import com.mfs.curbi.support.service.PricingDetailService;

@Service("PricingDetailService")
public class GetPricingDetailServiceImpl implements PricingDetailService {

	@Autowired
	PricingDetailDao pricingDetailDao;
	@Autowired
	CommonServices commonService;

	/*
	 * Method-getPricingDetails returns pricing details by country name
	 */
	public PricingFinalResponseDto getPricingDetails(PricingRequestDto request) {

		PricingFinalResponseDto response = new PricingFinalResponseDto();

		List<PricingResponseDto> pricingResponseList = new ArrayList<PricingResponseDto>();

		List<PricingResponseRowDto> pricingList = new ArrayList<PricingResponseRowDto>();

		double finalTotalAverageCost = 0.0;
		double finalTotalAverageCostPer = 0.0;
		double totalCount = 0.0;

		// convert string into comma separated string
		String countryName = commonService.convertString(request.getCountryName());

		String commaSeparatedCountries[] = countryName.split(",");
		String cntryNames = "";

		for (int i = 0; i < commaSeparatedCountries.length; i++) {
			cntryNames += "'" + commaSeparatedCountries[i] + "',";
		}
		cntryNames = cntryNames.substring(0, cntryNames.length() - 1);
		request.setCountryName(cntryNames);

		// Returns pricing detail list
		pricingList = pricingDetailDao.getPricingDetailsDao(request);

		double totalAverageTotalCost = 0.0;
		double totalAverageTotalCostPer = 0.0;

		// Checking pricing list is not empty
		if (!(pricingList.isEmpty())) {
			PricingResponseDto pricingResponse = null;
			for (PricingResponseRowDto pricing : pricingList) {
				pricingResponse = new PricingResponseDto();
				pricingResponse.setAverageSendFee(pricing.getAverageSendFee());
				pricingResponse.setAverageTotalCost(pricing.getAverageTotalCost());
				pricingResponse.setAverageTotalCostPercent(pricing.getAverageTotalCostPer());
				pricingResponse.setPartnerName(pricing.getPartnerName());
				pricingResponse.setPartnerCurrency(pricing.getPartnerCurrency());
				pricingResponseList.add(pricingResponse);

				totalCount = totalCount + pricing.getTotalCount();

				totalAverageTotalCost = totalAverageTotalCost + pricing.getTotalCount() * pricing.getAverageTotalCost();
				totalAverageTotalCostPer = totalAverageTotalCostPer
						+ pricing.getTotalCount() * pricing.getAverageTotalCostPer();
			}
		}

		finalTotalAverageCost = totalAverageTotalCost / totalCount;
		finalTotalAverageCostPer = totalAverageTotalCostPer / totalCount;

		response.setPricingResponse(pricingResponseList);
		response.setTotalAverageCost(finalTotalAverageCost);
		response.setTotalAverageCostPer(finalTotalAverageCostPer);

		return response;
	}

}
