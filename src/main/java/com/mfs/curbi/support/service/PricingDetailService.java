package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.PricingFinalResponseDto;
import com.mfs.curbi.support.dto.PricingRequestDto;

public interface PricingDetailService {
	
	public PricingFinalResponseDto getPricingDetails(PricingRequestDto request);

}
