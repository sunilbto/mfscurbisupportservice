package com.mfs.curbi.support.service;

import com.mfs.curbi.support.dto.FinalStatisticTransactionResponseDto;
import com.mfs.curbi.support.dto.StatisticCounsumeResponseDto;

public interface StatisticDetailService {

	public StatisticCounsumeResponseDto getStatisticCounsmerDetailsService(String countryName,String startDate,String endDate,String product,String destinationType); 

    public FinalStatisticTransactionResponseDto getStatisticTransactionDetailsService(String countryName,String startDate,String endDate,String product,String destinationType);
}
