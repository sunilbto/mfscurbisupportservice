package com.mfs.curbi.support.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.ComplianceDetailsDao;
import com.mfs.curbi.support.dto.ComplianceFinalResponseDto;
import com.mfs.curbi.support.dto.ComplianceResponseDto;
import com.mfs.curbi.support.dto.TrasnactionId;
import com.mfs.curbi.support.service.CommonServices;
import com.mfs.curbi.support.service.ComplianceDetailsService;
import com.mfs.curbi.support.util.Constant;


@Service("ComplianceDetailsService")
public class ComplianceDetailsServiceImpl implements ComplianceDetailsService{
	
	@Autowired
	ComplianceDetailsDao complianceDetailDao;
	@Autowired
	CommonServices commonService;
	
	/*
	 * Method-getComplianceDetailsService method returns compliance details
	 */
	public ComplianceFinalResponseDto getComplianceDetailsService(String countryName,String startDate,String endDate) throws Exception {
		 
		Date sd=null;
		Date ed=null;
		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT).parse(startDate);
		    ed = new SimpleDateFormat(Constant.DATE_FORMAT).parse(endDate);
		} catch (ParseException e) {
			throw new HibernateException("Incorrect Date Formate Error during parsing-----"+e.getMessage());
		}
		
		 //convert date into timeStamp
		Timestamp startDates = new Timestamp(sd.getTime());
		Timestamp endDates = new Timestamp(ed.getTime());
		
		ComplianceFinalResponseDto finalResponse=new ComplianceFinalResponseDto();
		
		// string converted into comma separated
		String country=commonService.convertString(countryName);
		
		String commaSeparatedCountries[]=country.split(",");			
		String cntryNames="";
		
		for(int i=0;i<commaSeparatedCountries.length;i++) {
			cntryNames+="'"+commaSeparatedCountries[i]+"',";
		}
		cntryNames=cntryNames.substring(0,cntryNames.length()-1);
		
		//Returns send from country list
		List<ComplianceResponseDto> sendingFromCountryResponseRowList=complianceDetailDao.sendingFromCountryList(cntryNames,startDates,endDates);

		//Returns transaction id
		List<TrasnactionId> transactionId = complianceDetailDao.getTransaction(cntryNames, startDates, endDates);
		String commSeparatedTransaction=null;
		//check transactionId list is empty or not
		if(!(transactionId.isEmpty()))
		{
		 commSeparatedTransaction = transactionId.toString().replace('[', ' ').replace(']', ' ').trim();
		}
		else
		{
			commSeparatedTransaction="' '";
		}

		//Returns sending to country list
		List<ComplianceResponseDto> sendingToCountryResponseRowList=complianceDetailDao.sendingToCountryList(commSeparatedTransaction);
		
		//Returns transaction id
		List<TrasnactionId> transactionIds = complianceDetailDao.getTransactions(cntryNames, startDates, endDates);
		
		String commSeparatedTransactions=null;
		
		//checking transaction id is not empty
		if(!(transactionId.isEmpty()))
		{
			commSeparatedTransactions = transactionIds.toString().replace('[', ' ').replace(']', ' ').trim();
		}
		else
		{
			commSeparatedTransactions="' '";
		}

		//Returns receiving from country list
		List<ComplianceResponseDto> receivingFromCountryResponseRowList=complianceDetailDao.receiveFromCountryList(commSeparatedTransactions);
		
		//checking sendingFromCountryResponseRowList is not empty
		if(!(sendingFromCountryResponseRowList.isEmpty()))
		{
			finalResponse.setSendingFromCountryRespone(sendingFromCountryResponseRowList);
		
		}
		
		//checking sendingToCountryResponseRowList is not empty
		if(!(sendingToCountryResponseRowList.isEmpty()))
		{
			finalResponse.setSendingToCountryRespone(sendingToCountryResponseRowList);
		
		}
		
		//Checking receivingFromCountryResponseRowList is not empty
		if(!(receivingFromCountryResponseRowList.isEmpty()))
		{
			finalResponse.setReceivingFromCountryResponse(receivingFromCountryResponseRowList);
		
		}
		
		return finalResponse;
	}

	
	
}
