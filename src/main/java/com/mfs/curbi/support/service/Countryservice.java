package com.mfs.curbi.support.service;

import java.util.List;

import com.mfs.curbi.support.dto.MdmCountryResponseDto;

public interface Countryservice {
	public List<MdmCountryResponseDto> getAllCountries()throws Exception;

}
