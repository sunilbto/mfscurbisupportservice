package com.mfs.curbi.support.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.MoneyFilterDao;
import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;
import com.mfs.curbi.support.dto.MdmProductResponseDto;
import com.mfs.curbi.support.dto.MoneyFilterResponseDto;
import com.mfs.curbi.support.service.CommonServices;
import com.mfs.curbi.support.service.MoneyFilterService;

@Service("MoneyFilterService")
public class MoneyFilterServiceImpl implements MoneyFilterService {

	@Autowired
	MoneyFilterDao moneyFilterDao;
	@Autowired
	CommonServices commonService;

	/*
	 * Method-moneyFilterService returns money flow filters
	 */
	public MoneyFilterResponseDto moneyFilterService() {

		MoneyFilterResponseDto filterResponse = new MoneyFilterResponseDto();

		// Returns product list
		List<MdmProductResponseDto> productList = moneyFilterDao.getProductList();

		// Returns destinationtype list
		List<MdmDestinationTypeResponseDto> destinationTypeList = moneyFilterDao.getDestinationType();

		filterResponse.setProductList(productList);
		filterResponse.setDestinationTypeList(destinationTypeList);

		return filterResponse;

	}

}
