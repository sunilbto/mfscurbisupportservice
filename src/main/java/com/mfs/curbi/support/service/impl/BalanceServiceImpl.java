package com.mfs.curbi.support.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.TransactionDetailsDao;
import com.mfs.curbi.support.dto.AllBalanceResponseDto;
import com.mfs.curbi.support.dto.AllTransactionByIdResponseDto;
import com.mfs.curbi.support.dto.BalanceResponseDto;
import com.mfs.curbi.support.dto.CurrentBalanceResponseDto;
import com.mfs.curbi.support.dto.TransactionByIdResponseDto;
import com.mfs.curbi.support.service.BalanceService;
import com.mfs.curbi.support.util.Constant;
import com.mfs.curbi.support.util.DoubleToString;

@Service
public class BalanceServiceImpl implements BalanceService {

	@Autowired
	TransactionDetailsDao transactionDetailsDao;

	private static final Logger log = LoggerFactory.getLogger(BalanceServiceImpl.class);

	/*
	 * Method-getAllBalance method return balance details of partner
	 */
	public BalanceResponseDto getAllBalance(long partnerCode, String startDate, String endDate, String status,
			String product, String direction, String destinationType, String receiveCountry, String flag)
			throws Exception {

		Date sd = null;
		Date ed = null;
		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT).parse(startDate);
			ed = new SimpleDateFormat(Constant.DATE_FORMAT).parse(endDate);
		} catch (ParseException e) {
			log.error("Inside getAllBalance() method of BalanceServiceImpl----" + e);
			throw new HibernateException("Incorrect Date Formate Error during parsing-----" + e);
		}
        //convert date into timeStamp
		Timestamp startdate = new Timestamp(sd.getTime());
		Timestamp enddate = new Timestamp(ed.getTime());

		BalanceResponseDto response = new BalanceResponseDto();

	   //Returns balance List against that partner code
		List<AllBalanceResponseDto> balanceList = transactionDetailsDao.getAllBalanceDao(partnerCode, startdate,
				enddate, status, product, direction, destinationType, receiveCountry);

	  // Checking balance list is not empty	
		if (!(balanceList.isEmpty())) {

			if (flag != null ? flag.equals(Constant.DOWNLOAD_CODE) : false) {
				response = new BalanceResponseDto();
				response.setBalanceDetail(balanceList);

			} else if (balanceList.size() <= 10000) {
				response = new BalanceResponseDto();
				response.setBalanceDetail(balanceList);
			} else if (balanceList.size() <= 50000) {
				response = new BalanceResponseDto();
				response.setResponseMessage("Balance Record is more than limit");
				response.setResponseCode(Constant.DOWNLOAD_CODE);

			} else if (balanceList.size() > 50000) {
				response = new BalanceResponseDto();
				response.setResponseMessage("Change date range");
				response.setResponseCode(Constant.CHANGE_DATE_RANGE_CODE);

			}
		}

		CurrentBalanceResponseDto currentBalance = null;
      
	  // Returns current balance 
		currentBalance = transactionDetailsDao.getCurrentBalanace(partnerCode);

	  // Checking current balance is not empty	
		if (currentBalance != null) {
			DoubleToString.takeToDigit(currentBalance.getCurrentBalance());
			response.setCurrentBalance(currentBalance.getCurrentBalance());
			response.setCurrency(currentBalance.getCurrency());
			}
			return response;
		}

	/*
	 * Method-getAllBalanceById method return balance details by transaction id
	 */	
	 public TransactionByIdResponseDto getAllBalanceById(int transactionId) throws Exception {

		TransactionByIdResponseDto response = null;
		
		//Returns list of transaction 
		List<AllTransactionByIdResponseDto> transactionByIdList = transactionDetailsDao
				.getTransactionDetailsById(transactionId);

		//Checking transaction list is not empty
		if (!(transactionByIdList.isEmpty())) {
			response = new TransactionByIdResponseDto();
			response.setTransactionDetail(transactionByIdList);

		}

		return response;

	}

}
