package com.mfs.curbi.support.service;

import java.util.List;

import com.mfs.curbi.support.dto.MdmDirectionResonseDto;

public interface DirectionService {
	public List<MdmDirectionResonseDto> getDirectionList() throws Exception;

}
