package com.mfs.curbi.support.service;

import java.util.List;

import com.mfs.curbi.support.dto.MdmProductResponseDto;

public interface ProductService {

	public List<MdmProductResponseDto> getAllProduct()throws Exception;
}
