package com.mfs.curbi.support.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.curbi.support.dao.StatisticDetailDao;
import com.mfs.curbi.support.dto.FinalStatisticTransactionResponseDto;
import com.mfs.curbi.support.dto.StatisticCounsumeResponseDto;
import com.mfs.curbi.support.dto.StatisticCounsumerMapperResponseDto;
import com.mfs.curbi.support.dto.StatisticTransactionSendFromResponseDto;
import com.mfs.curbi.support.dto.StatisticTransactionSendToResponseDto;
import com.mfs.curbi.support.dto.TotalAverage;
import com.mfs.curbi.support.dto.TotalAverageSendTo;
import com.mfs.curbi.support.dto.TrasnactionId;
import com.mfs.curbi.support.service.CommonServices;
import com.mfs.curbi.support.service.StatisticDetailService;
import com.mfs.curbi.support.util.Constant;

@Service("StatisticDetailService")
public class StatisticDetailServiceImpl implements StatisticDetailService {

	@Autowired
	StatisticDetailDao statisticDetailDao;
	@Autowired
	CommonServices commonService;

	/*
	 *Method-getStatisticCounsmerDetailsService returns statistic counsumer details
	 */
	public StatisticCounsumeResponseDto getStatisticCounsmerDetailsService(String countryName, String startDate,
			String endDate, String product, String destinationType) {

		Date sd = null;
		Date ed = null;
		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT).parse(startDate);
			ed = new SimpleDateFormat(Constant.DATE_FORMAT).parse(endDate);
		} catch (ParseException e) {
			throw new HibernateException("Incorrect Date Formate Error during parsing-----" + e.getMessage());
		}

		// convert date into timeStamp
		Timestamp startdate = new Timestamp(sd.getTime());
		Timestamp enddate = new Timestamp(ed.getTime());
		
		//convert string into comma separated string for country names
		String country = commonService.convertString(countryName);

		String commaSeparatedCountries[] = country.split(",");
		String cntryNames = "";

		for (int i = 0; i < commaSeparatedCountries.length; i++) {
			cntryNames += "'" + commaSeparatedCountries[i] + "',";
		}
		cntryNames = cntryNames.substring(0, cntryNames.length() - 1);

		//Returns statistic counsumer list
		List<StatisticCounsumerMapperResponseDto> StatisticCounsumerMapperList = statisticDetailDao
				.getStatisticCounsmerDetailsDao(cntryNames, startdate, enddate, product, destinationType);

		StatisticCounsumeResponseDto statisticCounsumeResponse = null;

		//checking StatisticCounsumerMapperList is not empty
		if (!(StatisticCounsumerMapperList.isEmpty())) {
			statisticCounsumeResponse = new StatisticCounsumeResponseDto();
			statisticCounsumeResponse.setStatisticConsumeResponse(StatisticCounsumerMapperList);
		}

		return statisticCounsumeResponse;
	}
  
	/*
	 *Method-FinalStatisticTransactionResponseDto returns statistic transaction details
	 */
	public FinalStatisticTransactionResponseDto getStatisticTransactionDetailsService(String countryName,
			String startDate, String endDate, String product, String destinationType) {

		FinalStatisticTransactionResponseDto finalResponse = new FinalStatisticTransactionResponseDto();
		Date sd = null;
		Date ed = null;
		try {
			sd = new SimpleDateFormat(Constant.DATE_FORMAT).parse(startDate);
			ed = new SimpleDateFormat(Constant.DATE_FORMAT).parse(endDate);
			

			
		} catch (ParseException e) {
			throw new HibernateException("Incorrect Date Formate Error during parsing-----" + e.getMessage());
		}

		Timestamp startDates = new Timestamp(sd.getTime());
		Timestamp endDates = new Timestamp(ed.getTime());
		String country = commonService.convertString(countryName);

		String commaSeparatedCountries[] = country.split(",");
		String cntryNames = "";

		for (int i = 0; i < commaSeparatedCountries.length; i++) {
			cntryNames += "'" + commaSeparatedCountries[i] + "',";
		}
		cntryNames = cntryNames.substring(0, cntryNames.length() - 1);

		//Return transaction ids
		List<TrasnactionId> transactionId = statisticDetailDao.getTransaction(cntryNames, startDates, endDates, product,destinationType);
		List<TotalAverage> totalAvgSendFrom=statisticDetailDao.getTotalAverageSendFrom(cntryNames, startDates, endDates, product,destinationType);
		List<TotalAverageSendTo> totalAvgSendTo=statisticDetailDao.getTotalAverageSendTo(cntryNames, startDates, endDates, product, destinationType);
		String commSeparatedTransaction=null;
		
		//cheking transactionid list is not empty
		if(!(transactionId.isEmpty()))
		{
		 commSeparatedTransaction = transactionId.toString().replace('[', ' ').replace(']', ' ').trim();
		}
		else
		{
			commSeparatedTransaction="' '";
		}

		//Returns statistic transaction sendTo List
		List<StatisticTransactionSendToResponseDto> statisticTransactionSendToList = statisticDetailDao
				.getStatisticTransactionSendToDetailsDao(cntryNames, startDates, endDates, product,
						destinationType,commSeparatedTransaction);
		
		//Returns statistic transaction sendFrom List
		List<StatisticTransactionSendFromResponseDto> statisticTransactionSendFromList = statisticDetailDao
				.getStatisticTransactionSendFromDetailsDao(cntryNames, startDates, endDates, product, destinationType);

		//Checking statisticTransactionSendToList is not emtpy
		if (!(statisticTransactionSendToList.isEmpty())) {
			finalResponse.setSendToCountry(statisticTransactionSendToList);
		}
		//Checking statisticTransactionSendFromList is not empty
		if (!(statisticTransactionSendFromList.isEmpty())) {
			finalResponse.setSendFromCountry(statisticTransactionSendFromList);
		}
		if(!(totalAvgSendFrom.isEmpty())) {
			finalResponse.setTotalAverageSendFrom(totalAvgSendFrom.get(0).getTotalAvgSendFrom());
		}
		if(!(totalAvgSendTo.isEmpty())) {
			finalResponse.setTotalAverageSendTo(totalAvgSendTo.get(0).getTotalAvgSendTo());
		}

		return finalResponse;
	}

}
