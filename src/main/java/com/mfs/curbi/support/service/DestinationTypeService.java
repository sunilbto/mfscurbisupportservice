
package com.mfs.curbi.support.service;

import java.util.List;

import com.mfs.curbi.support.dto.MdmDestinationTypeResponseDto;

public interface DestinationTypeService {
	public List<MdmDestinationTypeResponseDto> getDestinationTypeList()throws Exception;
}
