package com.mfs.curbi.exception;

public class CustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CustomException(String exception) {
		super(exception);
	}

}
