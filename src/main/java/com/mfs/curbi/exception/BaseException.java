package com.mfs.curbi.exception;

public class BaseException extends RuntimeException {

	public BaseException() {
		super();
	}

	public BaseException(Throwable thwx) {
		super(thwx);
	}

	public BaseException(String msg) {
		super(msg);
	}

	public BaseException(String msg, Throwable thx) {
		super(msg, thx);
	}
}
