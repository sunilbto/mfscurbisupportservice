package com.mfs.curbi.exception;

public class CurbiHibernateCommonException extends BaseException {

	public CurbiHibernateCommonException(Throwable e) {
		super(e);
	}

	public CurbiHibernateCommonException(String msg) {
		super(msg);
	}

	public CurbiHibernateCommonException(String msg, Throwable e) {
		super(msg, e);
	}

}
